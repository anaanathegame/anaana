﻿/// ---------------------------------------------
/// Ultimate Character Controller
/// Copyright (c) Opsive. All Rights Reserved.
/// https://www.opsive.com
/// ---------------------------------------------

using UnityEngine;
using Opsive.UltimateCharacterController.Events;
using Opsive.UltimateCharacterController.Character.Abilities;
using Opsive.UltimateCharacterController.Utility;

namespace Opsive.UltimateCharacterController.Addons.Agility
{
    /// <summary>
    /// The Balance ability will keep the character oriented in the forward/backward direction of a narrow platform. The ability will restrict the character's
    /// rotation so they can only move forward or backwards along the object.
    /// </summary>
    [DefaultAbilityIndex(107)]
    [DefaultUseRootMotionPosition(AbilityBoolOverride.True)]
    public class Balance : DetectGroundAbilityBase
    {
        [Tooltip("The horizontal force required to stop the ability.")]
        [SerializeField] protected float m_StopForceThreshold = 0.75f;
        [Tooltip("The amount of force to apply when the ability stops because of the stop force.")]
        [SerializeField] protected float m_StopForce = 0.1f;
        [Tooltip("The number of frames to apply the force in when the ability stops because of the stop force.")]
        [SerializeField] protected int m_StopForceFrames = 10;

        public float StopForceThreshold { get { return m_StopForceThreshold; } set { m_StopForceThreshold = value; } }
        public float StopForce { get { return m_StopForce; } set { m_StopForce = value; } }
        public int StopForceFrames { get { return m_StopForceFrames; } set { m_StopForceFrames = value; } }

        public override bool ImmediateStartItemVerifier { get { return true; } }

        /// <summary>
        /// The ability has started.
        /// </summary>
        protected override void AbilityStarted()
        {
            base.AbilityStarted();

            // Force independent look so the movement type won't try to rotate the character.
            EventHandler.ExecuteEvent(m_GameObject, "OnCharacterForceIndependentLook", true);
        }

        /// <summary>
        /// Called when another ability is attempting to start and the current ability is active.
        /// Returns true or false depending on if the new ability should be blocked from starting.
        /// </summary>
        /// <param name="startingAbility">The ability that is starting.</param>
        /// <returns>True if the ability should be blocked.</returns>
        public override bool ShouldBlockAbilityStart(Ability startingAbility)
        {
            return startingAbility is SpeedChange || startingAbility is HeightChange;
        }

        /// <summary>
        /// Called when the current ability is attempting to start and another ability is active.
        /// Returns true or false depending on if the active ability should be stopped.
        /// </summary>
        /// <param name="activeAbility">The ability that is currently active.</param>
        /// <returns>True if the ability should be stopped.</returns>
        public override bool ShouldStopActiveAbility(Ability activeAbility)
        {
            return activeAbility is SpeedChange || activeAbility is HeightChange;
        }

        /// <summary>
        /// Updates the ability.
        /// </summary>
        public override void Update()
        {
            base.Update();

            if (Mathf.Abs(m_CharacterLocomotion.RawInputVector.x) > m_StopForceThreshold) {
                m_CharacterLocomotion.AddForce(m_Transform.right * m_StopForce * (m_CharacterLocomotion.RawInputVector.x > 0 ? 1 : -1), m_StopForceFrames, false, true);
                StopAbility();
            }
        }

        /// <summary>
        /// Update the ability's Animator parameters.
        /// </summary>
        public override void UpdateAnimator()
        {
            base.UpdateAnimator();

            // No matter the movement type the x input should be set to the Horizontal Movement parameter, and the y input should be the Forward Movement parameter.
            m_CharacterLocomotion.InputVector = m_CharacterLocomotion.RawInputVector;
        }

        /// <summary>
        /// Update the controller's rotation values.
        /// </summary>
        public override void UpdateRotation()
        {
            // The character should orient towards the direction of the balance object.
            var angle = Vector3.SignedAngle(m_Transform.forward, m_GroundTransform.forward, m_CharacterLocomotion.Up);
            if (Mathf.Abs(angle) > 90) {
                angle = 180 + angle;
            }
            angle = Mathf.Lerp(0, MathUtility.ClampInnerAngle(angle), m_CharacterLocomotion.MotorRotationSpeed * Time.fixedDeltaTime);
            m_CharacterLocomotion.DeltaYawRotation = angle;
        }

        /// <summary>
        /// The ability has stopped running.
        /// </summary>
        /// <param name="force">Was the ability force stopped?</param>
        protected override void AbilityStopped(bool force)
        {
            base.AbilityStopped(force);

            EventHandler.ExecuteEvent(m_GameObject, "OnCharacterForceIndependentLook", false);
        }
    }
}