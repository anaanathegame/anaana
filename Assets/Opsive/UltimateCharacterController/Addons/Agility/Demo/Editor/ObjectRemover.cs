﻿#if !ULTIMATE_CHARACTER_CONTROLLER_ADDON_DEBUG
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
#if !FIRST_PERSON_CONTROLLER || !THIRD_PERSON_CONTROLLER
using Opsive.UltimateCharacterController.Camera.ViewTypes;
using Opsive.UltimateCharacterController.Character.MovementTypes;
using Opsive.UltimateCharacterController.Character.Abilities;
using Opsive.UltimateCharacterController.Character.Abilities.Items;
using Opsive.UltimateCharacterController.StateSystem;
using Opsive.UltimateCharacterController.Utility;
using Opsive.UltimateCharacterController.Editor.Inspectors.Utility;
using Opsive.UltimateCharacterController.Editor.Managers;
using System.Collections.Generic;
#endif

namespace Opsive.UltimateCharacterController.Addons.Agility.Demo.Editor
{
    [InitializeOnLoad]
    public class ObjectRemover
    {
        private static Scene s_ActiveScene;

        /// <summary>
        /// Registers for the scene change callback.
        /// </summary>
        static ObjectRemover()
        {
            EditorApplication.update += Update;
        }

        /// <summary>
        /// The scene has been changed.
        /// </summary>
        private static void Update()
        {
            var scene = SceneManager.GetActiveScene();

            if (scene == s_ActiveScene) {
                return;
            }
            s_ActiveScene = scene;

            // Only the Agility Pack demo scene should be affected.
            if (!s_ActiveScene.path.Replace("\\", "/").Contains("Agility/Demo/Demo.unity")) {
                return;
            }

            // Find the object which contains the objects that should be removed.
            var sceneReferences = GameObject.FindObjectOfType<SceneReferences>();
            RemoveSceneObjects(sceneReferences.RemoveObjects);
            sceneReferences.RemoveObjects = null;
#if !FIRST_PERSON_CONTROLLER
            RemoveSceneObjects(sceneReferences.FirstPersonObjects);
            sceneReferences.FirstPersonObjects = null;
#endif
#if !THIRD_PERSON_CONTROLLER
            RemoveSceneObjects(sceneReferences.ThirdPersonObjects);
            sceneReferences.ThirdPersonObjects = null;
#endif
#if !ULTIMATE_CHARACTER_CONTROLLER_SHOOTER
            RemoveSceneObjects(sceneReferences.ShooterObjects);
            sceneReferences.ShooterObjects = null;
#endif
#if !ULTIMATE_CHARACTER_CONTROLLER_MELEE
            RemoveSceneObjects(sceneReferences.MeleeObjects);
            sceneReferences.MeleeObjects = null;
#endif

#if !FIRST_PERSON_CONTROLLER || !THIRD_PERSON_CONTROLLER
            // Remove any view types and states that are no longer valid.
            var cameraController = GameObject.FindObjectOfType<Camera.CameraController>();
            cameraController.DeserializeViewTypes();
            var viewTypes = new List<ViewType>(cameraController.ViewTypes);
            for (int i = viewTypes.Count - 1; i > -1; --i) {
                if (viewTypes[i] == null) {
                    viewTypes.RemoveAt(i);
                    continue;
                }
                viewTypes[i].States = RemoveUnusedStates(viewTypes[i].States);
            }
            cameraController.ViewTypeData = Serialization.Serialize<ViewType>(viewTypes);
            cameraController.ViewTypes = viewTypes.ToArray();
            InspectorUtility.SetDirty(cameraController);

            // Remove any movement types and states that are no longer valid.
            var characterLocomotion = GameObject.FindObjectOfType<Character.UltimateCharacterLocomotion>();
            characterLocomotion.DeserializeMovementTypes();
            var movementTypes = new List<MovementType>(characterLocomotion.MovementTypes);
            for (int i = movementTypes.Count - 1; i > -1; --i) {
                if (movementTypes[i] == null) {
                    movementTypes.RemoveAt(i);
                    continue;
                }
                movementTypes[i].States = RemoveUnusedStates(movementTypes[i].States);
            }
            characterLocomotion.MovementTypeData = Serialization.Serialize<MovementType>(movementTypes);
            characterLocomotion.MovementTypes = movementTypes.ToArray();
            characterLocomotion.SetMovementType(characterLocomotion.MovementTypes[0].GetType());
            
            // Check for unused ability states.
            var abilities = new List<Ability>(characterLocomotion.GetSerializedAbilities());
            for (int i = abilities.Count - 1; i > -1; --i) {
                if (abilities[i] == null) {
                    abilities.RemoveAt(i);
                    continue;
                }
                abilities[i].States = RemoveUnusedStates(abilities[i].States);
            }
            characterLocomotion.AbilityData = Serialization.Serialize<Ability>(abilities);
            characterLocomotion.Abilities = abilities.ToArray();

            // Check for unused item ability states.
            var itemAbilities = new List<ItemAbility>(characterLocomotion.GetSerializedItemAbilities());
            for (int i = itemAbilities.Count - 1; i > -1; --i) {
                if (itemAbilities[i] == null) {
                    itemAbilities.RemoveAt(i);
                    continue;
                }
                itemAbilities[i].States = RemoveUnusedStates(itemAbilities[i].States);
            }
            characterLocomotion.ItemAbilityData = Serialization.Serialize<ItemAbility>(itemAbilities);
            characterLocomotion.ItemAbilities = itemAbilities.ToArray();
            InspectorUtility.SetDirty(characterLocomotion);

            // Update the inventory.
            var inventory = characterLocomotion.GetComponent<Inventory.InventoryBase>();
            var loadout = new List<Inventory.ItemTypeCount>(inventory.DefaultLoadout);
            for (int i = loadout.Count - 1; i > -1; --i) {
                if (loadout[i] == null) {
                    loadout.RemoveAt(i);
                }
            }
            inventory.DefaultLoadout = loadout.ToArray();
            InspectorUtility.SetDirty(inventory);

            var itemSetManager = characterLocomotion.GetComponent<Inventory.ItemSetManager>();
            var categoryItemSets = itemSetManager.CategoryItemSets;
            for (int i = 0; i < categoryItemSets.Length; ++i) {
                for (int j = categoryItemSets[i].ItemSetList.Count - 1; j > -1; --j) {
                    var nullItemType = true;
                    for (int k = 0; k < categoryItemSets[i].ItemSetList[j].Slots.Length; ++k) {
                        if (categoryItemSets[i].ItemSetList[j].Slots[k] != null) {
                            nullItemType = false;
                            break;
                        }
                    }
                    if (nullItemType) {
                        categoryItemSets[i].ItemSetList.RemoveAt(j);
                    }
                }
            };
            InspectorUtility.SetDirty(itemSetManager);

#if !THIRD_PERSON_CONTROLLER
            // Set the shadow caster for the first person only objects.
            var shadowCaster = ManagerUtility.FindInvisibleShadowCaster(null);
            if (shadowCaster != null) {
                for (int i = 0; i < sceneReferences.ShadowCasterObjects.Length; ++i) {
                    if (sceneReferences.ShadowCasterObjects[i] == null) {
                        continue;
                    }

                    var renderers = sceneReferences.ShadowCasterObjects[i].GetComponentsInChildren<Renderer>();
                    for (int j = 0; j < renderers.Length; ++j) {
                        var materials = renderers[j].sharedMaterials;
                        for (int k = 0; k < materials.Length; ++k) {
                            materials[k] = shadowCaster;
                        }
                        renderers[j].sharedMaterials = materials;
                        InspectorUtility.SetDirty(renderers[j]);
                    }
                }
            }
#endif

            // Ensure all of the states point to a preset
            var stateBehaviors = GameObject.FindObjectsOfType<StateBehavior>();
            for (int i = 0; i < stateBehaviors.Length; ++i) {
                stateBehaviors[i].States = RemoveUnusedStates(stateBehaviors[i].States);
                InspectorUtility.SetDirty(stateBehaviors[i]);
            }
#endif
        }

        /// <summary>
        /// Removes the specified scene objects.
        /// </summary>
        private static void RemoveSceneObjects(Object[] objects)
        {
            if (objects == null) {
                return;
            }

            for (int i = objects.Length - 1; i > -1; --i) {
                if (objects[i] == null) {
                    continue;
                }

                GameObject.DestroyImmediate(objects[i], true);
            }
        }

#if !FIRST_PERSON_CONTROLLER || !THIRD_PERSON_CONTROLLER
        /// <summary>
        /// Removes any states whose preset will be exlcluded.
        /// </summary>
        private static State[] RemoveUnusedStates(State[] stateArray)
        {
            var states = new List<State>(stateArray);
            var stateRemovals = new HashSet<string>();
            for (int i = states.Count - 2; i > -1; --i) {
                var preset = states[i].Preset;
                if (preset == null) {
                    stateRemovals.Add(states[i].Name);
                    states.RemoveAt(i);
                }
            }
            for (int i = 0; i < states.Count; ++i) {
                var blockList = new List<string>(states[i].BlockList);
                for (int j = blockList.Count - 1; j > -1; --j) {
                    if (stateRemovals.Contains(blockList[j])) {
                        blockList.RemoveAt(j);
                    }
                }
                states[i].BlockList = blockList.ToArray();
            }
            return states.ToArray();
        }
#endif
    }
}
#endif