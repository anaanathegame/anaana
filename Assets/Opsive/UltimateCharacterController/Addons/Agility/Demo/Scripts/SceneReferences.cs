﻿using UnityEngine;

namespace Opsive.UltimateCharacterController.Addons.Agility.Demo
{
    /// <summary>
    /// Helper class which references the scene objects.
    /// </summary>
    public class SceneReferences : MonoBehaviour
    {
        [Tooltip("A reference to the first person objects.")]
        [SerializeField] protected Object[] m_FirstPersonObjects;
        [Tooltip("A reference to the third person objects.")]
        [SerializeField] protected Object[] m_ThirdPersonObjects;
        [Tooltip("A reference to the shooter objects.")]
        [SerializeField] protected Object[] m_ShooterObjects;
        [Tooltip("A reference to the melee objects.")]
        [SerializeField] protected Object[] m_MeleeObjects;
        [Tooltip("Any object that should always be removed.")]
        [SerializeField] protected Object[] m_RemoveObjects;
        [Tooltip("Objects that should use the shadow caster while in a first person only perspective.")]
        [SerializeField] protected GameObject[] m_ShadowCasterObjects;

        public Object[] FirstPersonObjects { get { return m_FirstPersonObjects; } set { m_FirstPersonObjects = value; } }
        public Object[] ThirdPersonObjects { get { return m_ThirdPersonObjects; } set { m_ThirdPersonObjects = value; } }
        public Object[] ShooterObjects { get { return m_ShooterObjects; } set { m_ShooterObjects = value; } }
        public Object[] MeleeObjects { get { return m_MeleeObjects; } set { m_MeleeObjects = value; } }
        public Object[] RemoveObjects { get { return m_RemoveObjects; } set { m_RemoveObjects = value; } }
        public GameObject[] ShadowCasterObjects { get { return m_ShadowCasterObjects; } set { m_ShadowCasterObjects = value; } }
    }
}