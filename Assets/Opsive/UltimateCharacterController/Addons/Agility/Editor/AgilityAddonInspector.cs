﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using Opsive.UltimateCharacterController.Editor.Managers;
using Opsive.UltimateCharacterController.Editor.Inspectors.Character;
using Opsive.UltimateCharacterController.Editor.Inspectors.Utility;

namespace Opsive.UltimateCharacterController.Addons.Agility.Editor
{
    /// <summary>
    /// Draws the inspector for the agility addon.
    /// </summary>
    [OrderedEditorItem("Agility Pack", 0)]
    public class AgilityAddonInspector : AddonInspector
    {
        private GameObject m_Character;

        private UnityEditor.Animations.AnimatorController m_AnimatorController;

        /// <summary>
        /// Draws the addon inspector.
        /// </summary>
        public override void DrawInspector()
        {
            ManagerUtility.DrawControlBox("Agility Animations", DrawCharacterSelection, "This option will add the agility animations to your animator controller.", m_Character != null, "Add Animations",
                            AddAnimations, "The animations were successfully added.");
        }

        /// <summary>
        /// Draws the additional controls for the animator.
        /// </summary>
        private void DrawCharacterSelection()
        {
            EditorGUI.BeginChangeCheck();
            m_Character = EditorGUILayout.ObjectField("Character", m_Character, typeof(GameObject), true) as GameObject;
            if (EditorGUI.EndChangeCheck()) {
                var animator = m_Character.GetComponent<Animator>();
                if (animator != null) {
                    m_AnimatorController = (AnimatorController)animator.runtimeAnimatorController;
                }
            }

            if (m_Character != null) {
                EditorGUI.indentLevel++;
                m_AnimatorController = ClampAnimatorControllerField("Animator Controller", m_AnimatorController, 33);
                EditorGUI.indentLevel--;
            }
            GUILayout.Space(5);
        }

        /// <summary>
        /// Adds the animations to the animator controllers.
        /// </summary>
        private void AddAnimations()
        {
            var agilityTypes = InspectorDrawerUtility.GetAllTypesWithinNamespace("Opsive.UltimateCharacterController.Addons.Agility");
            if (agilityTypes == null) {
                return;
            }

            // Call BuildAnimator on all of the inspector drawers for the agility abilities.
            for (int i = 0; i < agilityTypes.Count; ++i) {
                var inspectorDrawer = InspectorDrawerUtility.InspectorDrawerForType(agilityTypes[i]) as AbilityInspectorDrawer;
                if (inspectorDrawer == null || !inspectorDrawer.CanBuildAnimator) {
                    continue;
                }

                inspectorDrawer.BuildAnimator(m_AnimatorController, null);
            }
        }

        /// <summary>
        /// Prevents the label from being too far away from the object field.
        /// </summary>
        /// <param name="label">The animator controller label.</param>
        /// <param name="animatorController">The animator controller value.</param>
        /// <param name="widthAddition">Any additional width to separate the label and the control.</param>
        /// <returns>The new animator controller.</returns>
        private static AnimatorController ClampAnimatorControllerField(string label, AnimatorController animatorController, int widthAddition)
        {
            var textDimensions = GUI.skin.label.CalcSize(new GUIContent(label));
            var prevLabelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = textDimensions.x + widthAddition;
            animatorController = EditorGUILayout.ObjectField(label, animatorController, typeof(AnimatorController), true) as AnimatorController;
            EditorGUIUtility.labelWidth = prevLabelWidth;
            return animatorController;
        }
    }
}