using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using Opsive.UltimateCharacterController.Editor.Utility;

namespace Opsive.UltimateCharacterController.Editor.Inspectors.Character.Abilities
{
	/// <summary>
	/// Draws a custom inspector for the Roll Ability.
	/// </summary>
	[InspectorDrawer(typeof(Opsive.UltimateCharacterController.Addons.Agility.Roll))]
	public class RollInspectorDrawer : AbilityInspectorDrawer
	{
		// ------------------------------------------- Start Generated Code -------------------------------------------
		// ------- Do NOT make any changes below. Changes will be removed when the animator is generated again. -------
		// ------------------------------------------------------------------------------------------------------------

		/// <summary>
		/// Returns true if the ability can build to the animator.
		/// </summary>
		public override bool CanBuildAnimator { get { return true; } }

		/// <summary>
		/// An editor only method which can add the abilities states/transitions to the animator.
		/// </summary>
		/// <param name="animatorController">The Animator Controller to add the states to.</param>
		/// <param name="firstPersonAnimatorController">The first person Animator Controller to add the states to.</param>
		public override void BuildAnimator(AnimatorController animatorController, AnimatorController firstPersonAnimatorController)
		{
			var baseStateMachine0 = animatorController.layers[0].stateMachine;

			// The state machine should start fresh.
			for (int i = 0; i < animatorController.layers.Length; ++i) {
				for (int j = 0; j < baseStateMachine0.stateMachines.Length; ++j) {
					if (baseStateMachine0.stateMachines[j].stateMachine.name == "Roll") {
						baseStateMachine0.RemoveStateMachine(baseStateMachine0.stateMachines[j].stateMachine);
						break;
					}
				}
			}

			// AnimationClip references.
			var m_AimRollStrafeLeftAnimationClip19980Path = AssetDatabase.GUIDToAssetPath("ebc53bbaf0a0c1a4cb304d6fcf6d59e0"); 
			var m_AimRollStrafeLeftAnimationClip19980 = AnimatorBuilder.GetAnimationClip(m_AimRollStrafeLeftAnimationClip19980Path, "AimRollStrafeLeft");
			var m_RollStrafeLeftAnimationClip13936Path = AssetDatabase.GUIDToAssetPath("b8637c33bea0507438d6ac9899a42eba"); 
			var m_RollStrafeLeftAnimationClip13936 = AnimatorBuilder.GetAnimationClip(m_RollStrafeLeftAnimationClip13936Path, "RollStrafeLeft");
			var m_RollStrafeRightAnimationClip13938Path = AssetDatabase.GUIDToAssetPath("b8637c33bea0507438d6ac9899a42eba"); 
			var m_RollStrafeRightAnimationClip13938 = AnimatorBuilder.GetAnimationClip(m_RollStrafeRightAnimationClip13938Path, "RollStrafeRight");
			var m_RollAnimationClip14134Path = AssetDatabase.GUIDToAssetPath("80a3c683dd5529f45a234b67ef06e9f9"); 
			var m_RollAnimationClip14134 = AnimatorBuilder.GetAnimationClip(m_RollAnimationClip14134Path, "Roll");
			var m_RollWalkAnimationClip13940Path = AssetDatabase.GUIDToAssetPath("b8637c33bea0507438d6ac9899a42eba"); 
			var m_RollWalkAnimationClip13940 = AnimatorBuilder.GetAnimationClip(m_RollWalkAnimationClip13940Path, "RollWalk");
			var m_RollRunAnimationClip16768Path = AssetDatabase.GUIDToAssetPath("a6ca56273daad044499776330617bbe0"); 
			var m_RollRunAnimationClip16768 = AnimatorBuilder.GetAnimationClip(m_RollRunAnimationClip16768Path, "RollRun");
			var m_AimRollStrafeRightAnimationClip13268Path = AssetDatabase.GUIDToAssetPath("2086f17275bfe1a428abc9e4fd79b9d0"); 
			var m_AimRollStrafeRightAnimationClip13268 = AnimatorBuilder.GetAnimationClip(m_AimRollStrafeRightAnimationClip13268Path, "AimRollStrafeRight");
			var m_RollFallingAnimationClip12036Path = AssetDatabase.GUIDToAssetPath("f58548f03b2a12d4784d3d02009eac46"); 
			var m_RollFallingAnimationClip12036 = AnimatorBuilder.GetAnimationClip(m_RollFallingAnimationClip12036Path, "RollFalling");

			// State Machine.
			var m_RollAnimatorStateMachine420336 = baseStateMachine0.AddStateMachine("Roll", new Vector3(624f, 156f, 0f));

			// States.
			var m_AimRollLeftAnimatorState420042 = m_RollAnimatorStateMachine420336.AddState("Aim Roll Left", new Vector3(384f, -132f, 0f));
			m_AimRollLeftAnimatorState420042.motion = m_AimRollStrafeLeftAnimationClip19980;
			m_AimRollLeftAnimatorState420042.cycleOffset = 0f;
			m_AimRollLeftAnimatorState420042.cycleOffsetParameterActive = false;
			m_AimRollLeftAnimatorState420042.iKOnFeet = false;
			m_AimRollLeftAnimatorState420042.mirror = false;
			m_AimRollLeftAnimatorState420042.mirrorParameterActive = false;
			m_AimRollLeftAnimatorState420042.speed = 2.75f;
			m_AimRollLeftAnimatorState420042.speedParameterActive = false;
			m_AimRollLeftAnimatorState420042.writeDefaultValues = true;

			var m_RollLeftAnimatorState420224 = m_RollAnimatorStateMachine420336.AddState("Roll Left", new Vector3(384f, -192f, 0f));
			m_RollLeftAnimatorState420224.motion = m_RollStrafeLeftAnimationClip13936;
			m_RollLeftAnimatorState420224.cycleOffset = 0f;
			m_RollLeftAnimatorState420224.cycleOffsetParameterActive = false;
			m_RollLeftAnimatorState420224.iKOnFeet = false;
			m_RollLeftAnimatorState420224.mirror = false;
			m_RollLeftAnimatorState420224.mirrorParameterActive = false;
			m_RollLeftAnimatorState420224.speed = 2.75f;
			m_RollLeftAnimatorState420224.speedParameterActive = false;
			m_RollLeftAnimatorState420224.writeDefaultValues = true;

			var m_RollRightAnimatorState420054 = m_RollAnimatorStateMachine420336.AddState("Roll Right", new Vector3(384f, -72f, 0f));
			m_RollRightAnimatorState420054.motion = m_RollStrafeRightAnimationClip13938;
			m_RollRightAnimatorState420054.cycleOffset = 0f;
			m_RollRightAnimatorState420054.cycleOffsetParameterActive = false;
			m_RollRightAnimatorState420054.iKOnFeet = false;
			m_RollRightAnimatorState420054.mirror = false;
			m_RollRightAnimatorState420054.mirrorParameterActive = false;
			m_RollRightAnimatorState420054.speed = 2.75f;
			m_RollRightAnimatorState420054.speedParameterActive = false;
			m_RollRightAnimatorState420054.writeDefaultValues = true;

			var m_RollAnimatorState420040 = m_RollAnimatorStateMachine420336.AddState("Roll", new Vector3(384f, 48f, 0f));
			m_RollAnimatorState420040.motion = m_RollAnimationClip14134;
			m_RollAnimatorState420040.cycleOffset = 0f;
			m_RollAnimatorState420040.cycleOffsetParameterActive = false;
			m_RollAnimatorState420040.iKOnFeet = false;
			m_RollAnimatorState420040.mirror = false;
			m_RollAnimatorState420040.mirrorParameterActive = false;
			m_RollAnimatorState420040.speed = 2.75f;
			m_RollAnimatorState420040.speedParameterActive = false;
			m_RollAnimatorState420040.writeDefaultValues = true;

			var m_RollWalkAnimatorState420096 = m_RollAnimatorStateMachine420336.AddState("Roll Walk", new Vector3(384f, 108f, 0f));
			m_RollWalkAnimatorState420096.motion = m_RollWalkAnimationClip13940;
			m_RollWalkAnimatorState420096.cycleOffset = 0f;
			m_RollWalkAnimatorState420096.cycleOffsetParameterActive = false;
			m_RollWalkAnimatorState420096.iKOnFeet = false;
			m_RollWalkAnimatorState420096.mirror = false;
			m_RollWalkAnimatorState420096.mirrorParameterActive = false;
			m_RollWalkAnimatorState420096.speed = 2.75f;
			m_RollWalkAnimatorState420096.speedParameterActive = false;
			m_RollWalkAnimatorState420096.writeDefaultValues = true;

			var m_RollRunAnimatorState420158 = m_RollAnimatorStateMachine420336.AddState("Roll Run", new Vector3(384f, 168f, 0f));
			m_RollRunAnimatorState420158.motion = m_RollRunAnimationClip16768;
			m_RollRunAnimatorState420158.cycleOffset = 0f;
			m_RollRunAnimatorState420158.cycleOffsetParameterActive = false;
			m_RollRunAnimatorState420158.iKOnFeet = false;
			m_RollRunAnimatorState420158.mirror = false;
			m_RollRunAnimatorState420158.mirrorParameterActive = false;
			m_RollRunAnimatorState420158.speed = 2.75f;
			m_RollRunAnimatorState420158.speedParameterActive = false;
			m_RollRunAnimatorState420158.writeDefaultValues = true;

			var m_AimRollRightAnimatorState420240 = m_RollAnimatorStateMachine420336.AddState("Aim Roll Right", new Vector3(384f, -12f, 0f));
			m_AimRollRightAnimatorState420240.motion = m_AimRollStrafeRightAnimationClip13268;
			m_AimRollRightAnimatorState420240.cycleOffset = 0f;
			m_AimRollRightAnimatorState420240.cycleOffsetParameterActive = false;
			m_AimRollRightAnimatorState420240.iKOnFeet = false;
			m_AimRollRightAnimatorState420240.mirror = false;
			m_AimRollRightAnimatorState420240.mirrorParameterActive = false;
			m_AimRollRightAnimatorState420240.speed = 2.75f;
			m_AimRollRightAnimatorState420240.speedParameterActive = false;
			m_AimRollRightAnimatorState420240.writeDefaultValues = true;

			var m_FallingRollAnimatorState420108 = m_RollAnimatorStateMachine420336.AddState("Falling Roll", new Vector3(384f, 228f, 0f));
			m_FallingRollAnimatorState420108.motion = m_RollFallingAnimationClip12036;
			m_FallingRollAnimatorState420108.cycleOffset = 0f;
			m_FallingRollAnimatorState420108.cycleOffsetParameterActive = false;
			m_FallingRollAnimatorState420108.iKOnFeet = false;
			m_FallingRollAnimatorState420108.mirror = false;
			m_FallingRollAnimatorState420108.mirrorParameterActive = false;
			m_FallingRollAnimatorState420108.speed = 2.75f;
			m_FallingRollAnimatorState420108.speedParameterActive = false;
			m_FallingRollAnimatorState420108.writeDefaultValues = true;

			// State Transitions.
			var m_AnimatorStateTransition419390 = m_AimRollLeftAnimatorState420042.AddExitTransition();
			m_AnimatorStateTransition419390.canTransitionToSelf = true;
			m_AnimatorStateTransition419390.duration = 0.15f;
			m_AnimatorStateTransition419390.exitTime = 0.9158775f;
			m_AnimatorStateTransition419390.hasExitTime = false;
			m_AnimatorStateTransition419390.hasFixedDuration = true;
			m_AnimatorStateTransition419390.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419390.offset = 0f;
			m_AnimatorStateTransition419390.orderedInterruption = true;
			m_AnimatorStateTransition419390.isExit = true;
			m_AnimatorStateTransition419390.mute = false;
			m_AnimatorStateTransition419390.solo = false;
			m_AnimatorStateTransition419390.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419536 = m_RollLeftAnimatorState420224.AddExitTransition();
			m_AnimatorStateTransition419536.canTransitionToSelf = true;
			m_AnimatorStateTransition419536.duration = 0.15f;
			m_AnimatorStateTransition419536.exitTime = 0.9158775f;
			m_AnimatorStateTransition419536.hasExitTime = false;
			m_AnimatorStateTransition419536.hasFixedDuration = true;
			m_AnimatorStateTransition419536.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419536.offset = 0f;
			m_AnimatorStateTransition419536.orderedInterruption = true;
			m_AnimatorStateTransition419536.isExit = true;
			m_AnimatorStateTransition419536.mute = false;
			m_AnimatorStateTransition419536.solo = false;
			m_AnimatorStateTransition419536.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418560 = m_RollRightAnimatorState420054.AddExitTransition();
			m_AnimatorStateTransition418560.canTransitionToSelf = true;
			m_AnimatorStateTransition418560.duration = 0.15f;
			m_AnimatorStateTransition418560.exitTime = 0.9158775f;
			m_AnimatorStateTransition418560.hasExitTime = false;
			m_AnimatorStateTransition418560.hasFixedDuration = true;
			m_AnimatorStateTransition418560.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418560.offset = 0f;
			m_AnimatorStateTransition418560.orderedInterruption = true;
			m_AnimatorStateTransition418560.isExit = true;
			m_AnimatorStateTransition418560.mute = false;
			m_AnimatorStateTransition418560.solo = false;
			m_AnimatorStateTransition418560.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419518 = m_RollAnimatorState420040.AddExitTransition();
			m_AnimatorStateTransition419518.canTransitionToSelf = true;
			m_AnimatorStateTransition419518.duration = 0.15f;
			m_AnimatorStateTransition419518.exitTime = 0.9158775f;
			m_AnimatorStateTransition419518.hasExitTime = false;
			m_AnimatorStateTransition419518.hasFixedDuration = true;
			m_AnimatorStateTransition419518.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419518.offset = 0f;
			m_AnimatorStateTransition419518.orderedInterruption = true;
			m_AnimatorStateTransition419518.isExit = true;
			m_AnimatorStateTransition419518.mute = false;
			m_AnimatorStateTransition419518.solo = false;
			m_AnimatorStateTransition419518.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419150 = m_RollWalkAnimatorState420096.AddExitTransition();
			m_AnimatorStateTransition419150.canTransitionToSelf = true;
			m_AnimatorStateTransition419150.duration = 0.15f;
			m_AnimatorStateTransition419150.exitTime = 0.9158775f;
			m_AnimatorStateTransition419150.hasExitTime = false;
			m_AnimatorStateTransition419150.hasFixedDuration = true;
			m_AnimatorStateTransition419150.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419150.offset = 0f;
			m_AnimatorStateTransition419150.orderedInterruption = true;
			m_AnimatorStateTransition419150.isExit = true;
			m_AnimatorStateTransition419150.mute = false;
			m_AnimatorStateTransition419150.solo = false;
			m_AnimatorStateTransition419150.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419354 = m_RollRunAnimatorState420158.AddExitTransition();
			m_AnimatorStateTransition419354.canTransitionToSelf = true;
			m_AnimatorStateTransition419354.duration = 0.15f;
			m_AnimatorStateTransition419354.exitTime = 0.9158775f;
			m_AnimatorStateTransition419354.hasExitTime = false;
			m_AnimatorStateTransition419354.hasFixedDuration = true;
			m_AnimatorStateTransition419354.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419354.offset = 0f;
			m_AnimatorStateTransition419354.orderedInterruption = true;
			m_AnimatorStateTransition419354.isExit = true;
			m_AnimatorStateTransition419354.mute = false;
			m_AnimatorStateTransition419354.solo = false;
			m_AnimatorStateTransition419354.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419554 = m_AimRollRightAnimatorState420240.AddExitTransition();
			m_AnimatorStateTransition419554.canTransitionToSelf = true;
			m_AnimatorStateTransition419554.duration = 0.15f;
			m_AnimatorStateTransition419554.exitTime = 0.9158775f;
			m_AnimatorStateTransition419554.hasExitTime = false;
			m_AnimatorStateTransition419554.hasFixedDuration = true;
			m_AnimatorStateTransition419554.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419554.offset = 0f;
			m_AnimatorStateTransition419554.orderedInterruption = true;
			m_AnimatorStateTransition419554.isExit = true;
			m_AnimatorStateTransition419554.mute = false;
			m_AnimatorStateTransition419554.solo = false;
			m_AnimatorStateTransition419554.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418642 = m_FallingRollAnimatorState420108.AddExitTransition();
			m_AnimatorStateTransition418642.canTransitionToSelf = true;
			m_AnimatorStateTransition418642.duration = 0.15f;
			m_AnimatorStateTransition418642.exitTime = 0.9158775f;
			m_AnimatorStateTransition418642.hasExitTime = false;
			m_AnimatorStateTransition418642.hasFixedDuration = true;
			m_AnimatorStateTransition418642.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418642.offset = 0f;
			m_AnimatorStateTransition418642.orderedInterruption = true;
			m_AnimatorStateTransition418642.isExit = true;
			m_AnimatorStateTransition418642.mute = false;
			m_AnimatorStateTransition418642.solo = false;
			m_AnimatorStateTransition418642.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			// State Machine Defaults.
			m_RollAnimatorStateMachine420336.anyStatePosition = new Vector3(50f, 20f, 0f);
			m_RollAnimatorStateMachine420336.defaultState = m_RollAnimatorState420040;
			m_RollAnimatorStateMachine420336.entryPosition = new Vector3(60f, -36f, 0f);
			m_RollAnimatorStateMachine420336.exitPosition = new Vector3(756f, 24f, 0f);
			m_RollAnimatorStateMachine420336.parentStateMachinePosition = new Vector3(732f, -48f, 0f);

			// State Machine Transitions.
			var m_AnimatorStateTransition419108 = baseStateMachine0.AddAnyStateTransition(m_AimRollLeftAnimatorState420042);
			m_AnimatorStateTransition419108.canTransitionToSelf = false;
			m_AnimatorStateTransition419108.duration = 0.05f;
			m_AnimatorStateTransition419108.exitTime = 0.75f;
			m_AnimatorStateTransition419108.hasExitTime = false;
			m_AnimatorStateTransition419108.hasFixedDuration = true;
			m_AnimatorStateTransition419108.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419108.offset = 0.1f;
			m_AnimatorStateTransition419108.orderedInterruption = true;
			m_AnimatorStateTransition419108.isExit = false;
			m_AnimatorStateTransition419108.mute = false;
			m_AnimatorStateTransition419108.solo = false;
			m_AnimatorStateTransition419108.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition419108.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419108.AddCondition(AnimatorConditionMode.Equals, 0f, "AbilityIntData");
			m_AnimatorStateTransition419108.AddCondition(AnimatorConditionMode.If, 0f, "Aiming");

			var m_AnimatorStateTransition418572 = baseStateMachine0.AddAnyStateTransition(m_RollLeftAnimatorState420224);
			m_AnimatorStateTransition418572.canTransitionToSelf = false;
			m_AnimatorStateTransition418572.duration = 0.05f;
			m_AnimatorStateTransition418572.exitTime = 0.75f;
			m_AnimatorStateTransition418572.hasExitTime = false;
			m_AnimatorStateTransition418572.hasFixedDuration = true;
			m_AnimatorStateTransition418572.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418572.offset = 0.1f;
			m_AnimatorStateTransition418572.orderedInterruption = true;
			m_AnimatorStateTransition418572.isExit = false;
			m_AnimatorStateTransition418572.mute = false;
			m_AnimatorStateTransition418572.solo = false;
			m_AnimatorStateTransition418572.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition418572.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418572.AddCondition(AnimatorConditionMode.Equals, 0f, "AbilityIntData");
			m_AnimatorStateTransition418572.AddCondition(AnimatorConditionMode.IfNot, 0f, "Aiming");

			var m_AnimatorStateTransition419416 = baseStateMachine0.AddAnyStateTransition(m_RollRightAnimatorState420054);
			m_AnimatorStateTransition419416.canTransitionToSelf = false;
			m_AnimatorStateTransition419416.duration = 0.05f;
			m_AnimatorStateTransition419416.exitTime = 0.75f;
			m_AnimatorStateTransition419416.hasExitTime = false;
			m_AnimatorStateTransition419416.hasFixedDuration = true;
			m_AnimatorStateTransition419416.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419416.offset = 0.1f;
			m_AnimatorStateTransition419416.orderedInterruption = true;
			m_AnimatorStateTransition419416.isExit = false;
			m_AnimatorStateTransition419416.mute = false;
			m_AnimatorStateTransition419416.solo = false;
			m_AnimatorStateTransition419416.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition419416.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419416.AddCondition(AnimatorConditionMode.Equals, 1f, "AbilityIntData");
			m_AnimatorStateTransition419416.AddCondition(AnimatorConditionMode.IfNot, 0f, "Aiming");

			var m_AnimatorStateTransition418630 = baseStateMachine0.AddAnyStateTransition(m_RollAnimatorState420040);
			m_AnimatorStateTransition418630.canTransitionToSelf = false;
			m_AnimatorStateTransition418630.duration = 0.05f;
			m_AnimatorStateTransition418630.exitTime = 0.75f;
			m_AnimatorStateTransition418630.hasExitTime = false;
			m_AnimatorStateTransition418630.hasFixedDuration = true;
			m_AnimatorStateTransition418630.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418630.offset = 0.1f;
			m_AnimatorStateTransition418630.orderedInterruption = true;
			m_AnimatorStateTransition418630.isExit = false;
			m_AnimatorStateTransition418630.mute = false;
			m_AnimatorStateTransition418630.solo = false;
			m_AnimatorStateTransition418630.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition418630.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418630.AddCondition(AnimatorConditionMode.Equals, 2f, "AbilityIntData");
			m_AnimatorStateTransition418630.AddCondition(AnimatorConditionMode.IfNot, 0f, "Moving");

			var m_AnimatorStateTransition419014 = baseStateMachine0.AddAnyStateTransition(m_RollWalkAnimatorState420096);
			m_AnimatorStateTransition419014.canTransitionToSelf = false;
			m_AnimatorStateTransition419014.duration = 0.05f;
			m_AnimatorStateTransition419014.exitTime = 0.75f;
			m_AnimatorStateTransition419014.hasExitTime = false;
			m_AnimatorStateTransition419014.hasFixedDuration = true;
			m_AnimatorStateTransition419014.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419014.offset = 0.1f;
			m_AnimatorStateTransition419014.orderedInterruption = true;
			m_AnimatorStateTransition419014.isExit = false;
			m_AnimatorStateTransition419014.mute = false;
			m_AnimatorStateTransition419014.solo = false;
			m_AnimatorStateTransition419014.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition419014.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419014.AddCondition(AnimatorConditionMode.Equals, 2f, "AbilityIntData");
			m_AnimatorStateTransition419014.AddCondition(AnimatorConditionMode.If, 0f, "Moving");
			m_AnimatorStateTransition419014.AddCondition(AnimatorConditionMode.Less, 0.5f, "Speed");

			var m_AnimatorStateTransition418950 = baseStateMachine0.AddAnyStateTransition(m_RollRunAnimatorState420158);
			m_AnimatorStateTransition418950.canTransitionToSelf = false;
			m_AnimatorStateTransition418950.duration = 0.05f;
			m_AnimatorStateTransition418950.exitTime = 0.75f;
			m_AnimatorStateTransition418950.hasExitTime = false;
			m_AnimatorStateTransition418950.hasFixedDuration = true;
			m_AnimatorStateTransition418950.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418950.offset = 0.1f;
			m_AnimatorStateTransition418950.orderedInterruption = true;
			m_AnimatorStateTransition418950.isExit = false;
			m_AnimatorStateTransition418950.mute = false;
			m_AnimatorStateTransition418950.solo = false;
			m_AnimatorStateTransition418950.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition418950.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418950.AddCondition(AnimatorConditionMode.Equals, 2f, "AbilityIntData");
			m_AnimatorStateTransition418950.AddCondition(AnimatorConditionMode.If, 0f, "Moving");
			m_AnimatorStateTransition418950.AddCondition(AnimatorConditionMode.Greater, 0.5f, "Speed");

			var m_AnimatorStateTransition419774 = baseStateMachine0.AddAnyStateTransition(m_AimRollRightAnimatorState420240);
			m_AnimatorStateTransition419774.canTransitionToSelf = false;
			m_AnimatorStateTransition419774.duration = 0.05f;
			m_AnimatorStateTransition419774.exitTime = 0.75f;
			m_AnimatorStateTransition419774.hasExitTime = false;
			m_AnimatorStateTransition419774.hasFixedDuration = true;
			m_AnimatorStateTransition419774.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419774.offset = 0.1f;
			m_AnimatorStateTransition419774.orderedInterruption = true;
			m_AnimatorStateTransition419774.isExit = false;
			m_AnimatorStateTransition419774.mute = false;
			m_AnimatorStateTransition419774.solo = false;
			m_AnimatorStateTransition419774.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition419774.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419774.AddCondition(AnimatorConditionMode.Equals, 1f, "AbilityIntData");
			m_AnimatorStateTransition419774.AddCondition(AnimatorConditionMode.If, 0f, "Aiming");

			var m_AnimatorStateTransition419700 = baseStateMachine0.AddAnyStateTransition(m_FallingRollAnimatorState420108);
			m_AnimatorStateTransition419700.canTransitionToSelf = false;
			m_AnimatorStateTransition419700.duration = 0.05f;
			m_AnimatorStateTransition419700.exitTime = 0.75f;
			m_AnimatorStateTransition419700.hasExitTime = false;
			m_AnimatorStateTransition419700.hasFixedDuration = true;
			m_AnimatorStateTransition419700.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419700.offset = 0.1f;
			m_AnimatorStateTransition419700.orderedInterruption = true;
			m_AnimatorStateTransition419700.isExit = false;
			m_AnimatorStateTransition419700.mute = false;
			m_AnimatorStateTransition419700.solo = false;
			m_AnimatorStateTransition419700.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition419700.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419700.AddCondition(AnimatorConditionMode.Equals, 3f, "AbilityIntData");

			var baseStateMachine5 = animatorController.layers[5].stateMachine;

			// The state machine should start fresh.
			for (int i = 0; i < animatorController.layers.Length; ++i) {
				for (int j = 0; j < baseStateMachine5.stateMachines.Length; ++j) {
					if (baseStateMachine5.stateMachines[j].stateMachine.name == "Roll") {
						baseStateMachine5.RemoveStateMachine(baseStateMachine5.stateMachines[j].stateMachine);
						break;
					}
				}
			}

			// AnimationClip references.
			var m_AssaultRifleRollIdleAnimationClip15330Path = AssetDatabase.GUIDToAssetPath("39544c3509240c542b07576a5af1c13c"); 
			var m_AssaultRifleRollIdleAnimationClip15330 = AnimatorBuilder.GetAnimationClip(m_AssaultRifleRollIdleAnimationClip15330Path, "AssaultRifleRollIdle");
			var m_PistolRollIdleAnimationClip15510Path = AssetDatabase.GUIDToAssetPath("09e38275d81602b489835c6374477408"); 
			var m_PistolRollIdleAnimationClip15510 = AnimatorBuilder.GetAnimationClip(m_PistolRollIdleAnimationClip15510Path, "PistolRollIdle");
			var m_DualPistolRollIdleAnimationClip22182Path = AssetDatabase.GUIDToAssetPath("d828fedde99b6024a8e7e26e17e45386"); 
			var m_DualPistolRollIdleAnimationClip22182 = AnimatorBuilder.GetAnimationClip(m_DualPistolRollIdleAnimationClip22182Path, "DualPistolRollIdle");
			var m_RocketLauncherRollIdleAnimationClip15104Path = AssetDatabase.GUIDToAssetPath("4b2e64e4268ee634a8f5ef226eea2fa7"); 
			var m_RocketLauncherRollIdleAnimationClip15104 = AnimatorBuilder.GetAnimationClip(m_RocketLauncherRollIdleAnimationClip15104Path, "RocketLauncherRollIdle");
			var m_ShotgunRollIdleAnimationClip16290Path = AssetDatabase.GUIDToAssetPath("ecea2486194b00448941b70a63f54011"); 
			var m_ShotgunRollIdleAnimationClip16290 = AnimatorBuilder.GetAnimationClip(m_ShotgunRollIdleAnimationClip16290Path, "ShotgunRollIdle");
			var m_SniperRifleRollIdleAnimationClip20776Path = AssetDatabase.GUIDToAssetPath("39b068db4042ab343a202f0407939fed"); 
			var m_SniperRifleRollIdleAnimationClip20776 = AnimatorBuilder.GetAnimationClip(m_SniperRifleRollIdleAnimationClip20776Path, "SniperRifleRollIdle");
			var m_ShieldRollIdleAnimationClip21758Path = AssetDatabase.GUIDToAssetPath("a0f52d3dc5cea6046b13848f8edd2f5c"); 
			var m_ShieldRollIdleAnimationClip21758 = AnimatorBuilder.GetAnimationClip(m_ShieldRollIdleAnimationClip21758Path, "ShieldRollIdle");
			var m_BowRollIdleAnimationClip22056Path = AssetDatabase.GUIDToAssetPath("a073f0ad43f21244aaca022aef7f88ee"); 
			var m_BowRollIdleAnimationClip22056 = AnimatorBuilder.GetAnimationClip(m_BowRollIdleAnimationClip22056Path, "BowRollIdle");

			// State Machine.
			var m_RollAnimatorStateMachine420334 = baseStateMachine5.AddStateMachine("Roll", new Vector3(852f, 108f, 0f));

			// States.
			var m_AssaultRifleAnimatorState420154 = m_RollAnimatorStateMachine420334.AddState("Assault Rifle", new Vector3(384f, -48f, 0f));
			m_AssaultRifleAnimatorState420154.motion = m_AssaultRifleRollIdleAnimationClip15330;
			m_AssaultRifleAnimatorState420154.cycleOffset = 0f;
			m_AssaultRifleAnimatorState420154.cycleOffsetParameterActive = false;
			m_AssaultRifleAnimatorState420154.iKOnFeet = false;
			m_AssaultRifleAnimatorState420154.mirror = false;
			m_AssaultRifleAnimatorState420154.mirrorParameterActive = false;
			m_AssaultRifleAnimatorState420154.speed = 1f;
			m_AssaultRifleAnimatorState420154.speedParameterActive = false;
			m_AssaultRifleAnimatorState420154.writeDefaultValues = true;

			var m_PistolAnimatorState420248 = m_RollAnimatorStateMachine420334.AddState("Pistol", new Vector3(384f, 132f, 0f));
			m_PistolAnimatorState420248.motion = m_PistolRollIdleAnimationClip15510;
			m_PistolAnimatorState420248.cycleOffset = 0f;
			m_PistolAnimatorState420248.cycleOffsetParameterActive = false;
			m_PistolAnimatorState420248.iKOnFeet = false;
			m_PistolAnimatorState420248.mirror = false;
			m_PistolAnimatorState420248.mirrorParameterActive = false;
			m_PistolAnimatorState420248.speed = 1f;
			m_PistolAnimatorState420248.speedParameterActive = false;
			m_PistolAnimatorState420248.writeDefaultValues = true;

			var m_DualPistolAnimatorState420208 = m_RollAnimatorStateMachine420334.AddState("Dual Pistol", new Vector3(384f, 72f, 0f));
			m_DualPistolAnimatorState420208.motion = m_DualPistolRollIdleAnimationClip22182;
			m_DualPistolAnimatorState420208.cycleOffset = 0f;
			m_DualPistolAnimatorState420208.cycleOffsetParameterActive = false;
			m_DualPistolAnimatorState420208.iKOnFeet = false;
			m_DualPistolAnimatorState420208.mirror = false;
			m_DualPistolAnimatorState420208.mirrorParameterActive = false;
			m_DualPistolAnimatorState420208.speed = 1f;
			m_DualPistolAnimatorState420208.speedParameterActive = false;
			m_DualPistolAnimatorState420208.writeDefaultValues = true;

			var m_RocketLauncherAnimatorState420032 = m_RollAnimatorStateMachine420334.AddState("Rocket Launcher", new Vector3(384f, 192f, 0f));
			m_RocketLauncherAnimatorState420032.motion = m_RocketLauncherRollIdleAnimationClip15104;
			m_RocketLauncherAnimatorState420032.cycleOffset = 0f;
			m_RocketLauncherAnimatorState420032.cycleOffsetParameterActive = false;
			m_RocketLauncherAnimatorState420032.iKOnFeet = false;
			m_RocketLauncherAnimatorState420032.mirror = false;
			m_RocketLauncherAnimatorState420032.mirrorParameterActive = false;
			m_RocketLauncherAnimatorState420032.speed = 1f;
			m_RocketLauncherAnimatorState420032.speedParameterActive = false;
			m_RocketLauncherAnimatorState420032.writeDefaultValues = true;

			var m_ShotgunAnimatorState420028 = m_RollAnimatorStateMachine420334.AddState("Shotgun", new Vector3(384f, 312f, 0f));
			m_ShotgunAnimatorState420028.motion = m_ShotgunRollIdleAnimationClip16290;
			m_ShotgunAnimatorState420028.cycleOffset = 0f;
			m_ShotgunAnimatorState420028.cycleOffsetParameterActive = false;
			m_ShotgunAnimatorState420028.iKOnFeet = false;
			m_ShotgunAnimatorState420028.mirror = false;
			m_ShotgunAnimatorState420028.mirrorParameterActive = false;
			m_ShotgunAnimatorState420028.speed = 1f;
			m_ShotgunAnimatorState420028.speedParameterActive = false;
			m_ShotgunAnimatorState420028.writeDefaultValues = true;

			var m_SniperRifleAnimatorState420200 = m_RollAnimatorStateMachine420334.AddState("Sniper Rifle", new Vector3(384f, 372f, 0f));
			m_SniperRifleAnimatorState420200.motion = m_SniperRifleRollIdleAnimationClip20776;
			m_SniperRifleAnimatorState420200.cycleOffset = 0f;
			m_SniperRifleAnimatorState420200.cycleOffsetParameterActive = false;
			m_SniperRifleAnimatorState420200.iKOnFeet = false;
			m_SniperRifleAnimatorState420200.mirror = false;
			m_SniperRifleAnimatorState420200.mirrorParameterActive = false;
			m_SniperRifleAnimatorState420200.speed = 1f;
			m_SniperRifleAnimatorState420200.speedParameterActive = false;
			m_SniperRifleAnimatorState420200.writeDefaultValues = true;

			var m_ShieldAnimatorState420160 = m_RollAnimatorStateMachine420334.AddState("Shield", new Vector3(384f, 252f, 0f));
			m_ShieldAnimatorState420160.motion = m_ShieldRollIdleAnimationClip21758;
			m_ShieldAnimatorState420160.cycleOffset = 0f;
			m_ShieldAnimatorState420160.cycleOffsetParameterActive = false;
			m_ShieldAnimatorState420160.iKOnFeet = false;
			m_ShieldAnimatorState420160.mirror = false;
			m_ShieldAnimatorState420160.mirrorParameterActive = false;
			m_ShieldAnimatorState420160.speed = 1f;
			m_ShieldAnimatorState420160.speedParameterActive = false;
			m_ShieldAnimatorState420160.writeDefaultValues = true;

			var m_BowAnimatorState420008 = m_RollAnimatorStateMachine420334.AddState("Bow", new Vector3(384f, 12f, 0f));
			m_BowAnimatorState420008.motion = m_BowRollIdleAnimationClip22056;
			m_BowAnimatorState420008.cycleOffset = 0f;
			m_BowAnimatorState420008.cycleOffsetParameterActive = false;
			m_BowAnimatorState420008.iKOnFeet = false;
			m_BowAnimatorState420008.mirror = false;
			m_BowAnimatorState420008.mirrorParameterActive = false;
			m_BowAnimatorState420008.speed = 1f;
			m_BowAnimatorState420008.speedParameterActive = false;
			m_BowAnimatorState420008.writeDefaultValues = true;

			// State Transitions.
			var m_AnimatorStateTransition419862 = m_AssaultRifleAnimatorState420154.AddExitTransition();
			m_AnimatorStateTransition419862.canTransitionToSelf = true;
			m_AnimatorStateTransition419862.duration = 0.15f;
			m_AnimatorStateTransition419862.exitTime = 0f;
			m_AnimatorStateTransition419862.hasExitTime = false;
			m_AnimatorStateTransition419862.hasFixedDuration = true;
			m_AnimatorStateTransition419862.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419862.offset = 0f;
			m_AnimatorStateTransition419862.orderedInterruption = true;
			m_AnimatorStateTransition419862.isExit = true;
			m_AnimatorStateTransition419862.mute = false;
			m_AnimatorStateTransition419862.solo = false;
			m_AnimatorStateTransition419862.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418852 = m_PistolAnimatorState420248.AddExitTransition();
			m_AnimatorStateTransition418852.canTransitionToSelf = true;
			m_AnimatorStateTransition418852.duration = 0.15f;
			m_AnimatorStateTransition418852.exitTime = 0f;
			m_AnimatorStateTransition418852.hasExitTime = false;
			m_AnimatorStateTransition418852.hasFixedDuration = true;
			m_AnimatorStateTransition418852.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418852.offset = 0f;
			m_AnimatorStateTransition418852.orderedInterruption = true;
			m_AnimatorStateTransition418852.isExit = true;
			m_AnimatorStateTransition418852.mute = false;
			m_AnimatorStateTransition418852.solo = false;
			m_AnimatorStateTransition418852.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419072 = m_DualPistolAnimatorState420208.AddExitTransition();
			m_AnimatorStateTransition419072.canTransitionToSelf = true;
			m_AnimatorStateTransition419072.duration = 0.15f;
			m_AnimatorStateTransition419072.exitTime = 0f;
			m_AnimatorStateTransition419072.hasExitTime = false;
			m_AnimatorStateTransition419072.hasFixedDuration = true;
			m_AnimatorStateTransition419072.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419072.offset = 0f;
			m_AnimatorStateTransition419072.orderedInterruption = true;
			m_AnimatorStateTransition419072.isExit = true;
			m_AnimatorStateTransition419072.mute = false;
			m_AnimatorStateTransition419072.solo = false;
			m_AnimatorStateTransition419072.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418910 = m_RocketLauncherAnimatorState420032.AddExitTransition();
			m_AnimatorStateTransition418910.canTransitionToSelf = true;
			m_AnimatorStateTransition418910.duration = 0.15f;
			m_AnimatorStateTransition418910.exitTime = 0f;
			m_AnimatorStateTransition418910.hasExitTime = false;
			m_AnimatorStateTransition418910.hasFixedDuration = true;
			m_AnimatorStateTransition418910.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418910.offset = 0f;
			m_AnimatorStateTransition418910.orderedInterruption = true;
			m_AnimatorStateTransition418910.isExit = true;
			m_AnimatorStateTransition418910.mute = false;
			m_AnimatorStateTransition418910.solo = false;
			m_AnimatorStateTransition418910.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418494 = m_ShotgunAnimatorState420028.AddExitTransition();
			m_AnimatorStateTransition418494.canTransitionToSelf = true;
			m_AnimatorStateTransition418494.duration = 0.15f;
			m_AnimatorStateTransition418494.exitTime = 0f;
			m_AnimatorStateTransition418494.hasExitTime = false;
			m_AnimatorStateTransition418494.hasFixedDuration = true;
			m_AnimatorStateTransition418494.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418494.offset = 0f;
			m_AnimatorStateTransition418494.orderedInterruption = true;
			m_AnimatorStateTransition418494.isExit = true;
			m_AnimatorStateTransition418494.mute = false;
			m_AnimatorStateTransition418494.solo = false;
			m_AnimatorStateTransition418494.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418796 = m_SniperRifleAnimatorState420200.AddExitTransition();
			m_AnimatorStateTransition418796.canTransitionToSelf = true;
			m_AnimatorStateTransition418796.duration = 0.15f;
			m_AnimatorStateTransition418796.exitTime = 0f;
			m_AnimatorStateTransition418796.hasExitTime = false;
			m_AnimatorStateTransition418796.hasFixedDuration = true;
			m_AnimatorStateTransition418796.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418796.offset = 0f;
			m_AnimatorStateTransition418796.orderedInterruption = true;
			m_AnimatorStateTransition418796.isExit = true;
			m_AnimatorStateTransition418796.mute = false;
			m_AnimatorStateTransition418796.solo = false;
			m_AnimatorStateTransition418796.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419580 = m_ShieldAnimatorState420160.AddExitTransition();
			m_AnimatorStateTransition419580.canTransitionToSelf = true;
			m_AnimatorStateTransition419580.duration = 0.15f;
			m_AnimatorStateTransition419580.exitTime = 0f;
			m_AnimatorStateTransition419580.hasExitTime = false;
			m_AnimatorStateTransition419580.hasFixedDuration = true;
			m_AnimatorStateTransition419580.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419580.offset = 0f;
			m_AnimatorStateTransition419580.orderedInterruption = true;
			m_AnimatorStateTransition419580.isExit = true;
			m_AnimatorStateTransition419580.mute = false;
			m_AnimatorStateTransition419580.solo = false;
			m_AnimatorStateTransition419580.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419044 = m_BowAnimatorState420008.AddExitTransition();
			m_AnimatorStateTransition419044.canTransitionToSelf = true;
			m_AnimatorStateTransition419044.duration = 0.15f;
			m_AnimatorStateTransition419044.exitTime = 0f;
			m_AnimatorStateTransition419044.hasExitTime = false;
			m_AnimatorStateTransition419044.hasFixedDuration = true;
			m_AnimatorStateTransition419044.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419044.offset = 0f;
			m_AnimatorStateTransition419044.orderedInterruption = true;
			m_AnimatorStateTransition419044.isExit = true;
			m_AnimatorStateTransition419044.mute = false;
			m_AnimatorStateTransition419044.solo = false;
			m_AnimatorStateTransition419044.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			// State Machine Defaults.
			m_RollAnimatorStateMachine420334.anyStatePosition = new Vector3(36f, 156f, 0f);
			m_RollAnimatorStateMachine420334.defaultState = m_AssaultRifleAnimatorState420154;
			m_RollAnimatorStateMachine420334.entryPosition = new Vector3(36f, 96f, 0f);
			m_RollAnimatorStateMachine420334.exitPosition = new Vector3(768f, 168f, 0f);
			m_RollAnimatorStateMachine420334.parentStateMachinePosition = new Vector3(744f, 84f, 0f);

			// State Machine Transitions.
			var m_AnimatorStateTransition418906 = baseStateMachine5.AddAnyStateTransition(m_ShieldAnimatorState420160);
			m_AnimatorStateTransition418906.canTransitionToSelf = false;
			m_AnimatorStateTransition418906.duration = 0.05f;
			m_AnimatorStateTransition418906.exitTime = 0.75f;
			m_AnimatorStateTransition418906.hasExitTime = false;
			m_AnimatorStateTransition418906.hasFixedDuration = true;
			m_AnimatorStateTransition418906.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418906.offset = 0f;
			m_AnimatorStateTransition418906.orderedInterruption = true;
			m_AnimatorStateTransition418906.isExit = false;
			m_AnimatorStateTransition418906.mute = false;
			m_AnimatorStateTransition418906.solo = false;
			m_AnimatorStateTransition418906.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418906.AddCondition(AnimatorConditionMode.Equals, 25f, "Slot1ItemID");

			var m_AnimatorStateTransition419336 = baseStateMachine5.AddAnyStateTransition(m_DualPistolAnimatorState420208);
			m_AnimatorStateTransition419336.canTransitionToSelf = false;
			m_AnimatorStateTransition419336.duration = 0.05f;
			m_AnimatorStateTransition419336.exitTime = 0.75f;
			m_AnimatorStateTransition419336.hasExitTime = false;
			m_AnimatorStateTransition419336.hasFixedDuration = true;
			m_AnimatorStateTransition419336.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419336.offset = 0f;
			m_AnimatorStateTransition419336.orderedInterruption = true;
			m_AnimatorStateTransition419336.isExit = false;
			m_AnimatorStateTransition419336.mute = false;
			m_AnimatorStateTransition419336.solo = false;
			m_AnimatorStateTransition419336.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419336.AddCondition(AnimatorConditionMode.Equals, 2f, "Slot0ItemID");
			m_AnimatorStateTransition419336.AddCondition(AnimatorConditionMode.Equals, 2f, "Slot1ItemID");

			var m_AnimatorStateTransition419848 = baseStateMachine5.AddAnyStateTransition(m_PistolAnimatorState420248);
			m_AnimatorStateTransition419848.canTransitionToSelf = false;
			m_AnimatorStateTransition419848.duration = 0.05f;
			m_AnimatorStateTransition419848.exitTime = 0.75f;
			m_AnimatorStateTransition419848.hasExitTime = false;
			m_AnimatorStateTransition419848.hasFixedDuration = true;
			m_AnimatorStateTransition419848.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419848.offset = 0f;
			m_AnimatorStateTransition419848.orderedInterruption = true;
			m_AnimatorStateTransition419848.isExit = false;
			m_AnimatorStateTransition419848.mute = false;
			m_AnimatorStateTransition419848.solo = false;
			m_AnimatorStateTransition419848.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419848.AddCondition(AnimatorConditionMode.Equals, 2f, "Slot0ItemID");
			m_AnimatorStateTransition419848.AddCondition(AnimatorConditionMode.NotEqual, 2f, "Slot1ItemID");

			var m_AnimatorStateTransition419298 = baseStateMachine5.AddAnyStateTransition(m_AssaultRifleAnimatorState420154);
			m_AnimatorStateTransition419298.canTransitionToSelf = false;
			m_AnimatorStateTransition419298.duration = 0.05f;
			m_AnimatorStateTransition419298.exitTime = 0.75f;
			m_AnimatorStateTransition419298.hasExitTime = false;
			m_AnimatorStateTransition419298.hasFixedDuration = true;
			m_AnimatorStateTransition419298.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419298.offset = 0f;
			m_AnimatorStateTransition419298.orderedInterruption = true;
			m_AnimatorStateTransition419298.isExit = false;
			m_AnimatorStateTransition419298.mute = false;
			m_AnimatorStateTransition419298.solo = false;
			m_AnimatorStateTransition419298.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419298.AddCondition(AnimatorConditionMode.Equals, 1f, "Slot0ItemID");

			var m_AnimatorStateTransition418720 = baseStateMachine5.AddAnyStateTransition(m_BowAnimatorState420008);
			m_AnimatorStateTransition418720.canTransitionToSelf = false;
			m_AnimatorStateTransition418720.duration = 0.05f;
			m_AnimatorStateTransition418720.exitTime = 0.75f;
			m_AnimatorStateTransition418720.hasExitTime = false;
			m_AnimatorStateTransition418720.hasFixedDuration = true;
			m_AnimatorStateTransition418720.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418720.offset = 0f;
			m_AnimatorStateTransition418720.orderedInterruption = true;
			m_AnimatorStateTransition418720.isExit = false;
			m_AnimatorStateTransition418720.mute = false;
			m_AnimatorStateTransition418720.solo = false;
			m_AnimatorStateTransition418720.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418720.AddCondition(AnimatorConditionMode.Equals, 4f, "Slot1ItemID");

			var m_AnimatorStateTransition419548 = baseStateMachine5.AddAnyStateTransition(m_SniperRifleAnimatorState420200);
			m_AnimatorStateTransition419548.canTransitionToSelf = false;
			m_AnimatorStateTransition419548.duration = 0.05f;
			m_AnimatorStateTransition419548.exitTime = 0.75f;
			m_AnimatorStateTransition419548.hasExitTime = false;
			m_AnimatorStateTransition419548.hasFixedDuration = true;
			m_AnimatorStateTransition419548.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419548.offset = 0f;
			m_AnimatorStateTransition419548.orderedInterruption = true;
			m_AnimatorStateTransition419548.isExit = false;
			m_AnimatorStateTransition419548.mute = false;
			m_AnimatorStateTransition419548.solo = false;
			m_AnimatorStateTransition419548.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419548.AddCondition(AnimatorConditionMode.Equals, 5f, "Slot0ItemID");

			var m_AnimatorStateTransition419596 = baseStateMachine5.AddAnyStateTransition(m_ShotgunAnimatorState420028);
			m_AnimatorStateTransition419596.canTransitionToSelf = false;
			m_AnimatorStateTransition419596.duration = 0.05f;
			m_AnimatorStateTransition419596.exitTime = 0.75f;
			m_AnimatorStateTransition419596.hasExitTime = false;
			m_AnimatorStateTransition419596.hasFixedDuration = true;
			m_AnimatorStateTransition419596.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419596.offset = 0f;
			m_AnimatorStateTransition419596.orderedInterruption = true;
			m_AnimatorStateTransition419596.isExit = false;
			m_AnimatorStateTransition419596.mute = false;
			m_AnimatorStateTransition419596.solo = false;
			m_AnimatorStateTransition419596.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419596.AddCondition(AnimatorConditionMode.Equals, 3f, "Slot0ItemID");

			var m_AnimatorStateTransition419252 = baseStateMachine5.AddAnyStateTransition(m_RocketLauncherAnimatorState420032);
			m_AnimatorStateTransition419252.canTransitionToSelf = false;
			m_AnimatorStateTransition419252.duration = 0.05f;
			m_AnimatorStateTransition419252.exitTime = 0.75f;
			m_AnimatorStateTransition419252.hasExitTime = false;
			m_AnimatorStateTransition419252.hasFixedDuration = true;
			m_AnimatorStateTransition419252.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419252.offset = 0f;
			m_AnimatorStateTransition419252.orderedInterruption = true;
			m_AnimatorStateTransition419252.isExit = false;
			m_AnimatorStateTransition419252.mute = false;
			m_AnimatorStateTransition419252.solo = false;
			m_AnimatorStateTransition419252.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419252.AddCondition(AnimatorConditionMode.Equals, 6f, "Slot0ItemID");

			var baseStateMachine6 = animatorController.layers[6].stateMachine;

			// The state machine should start fresh.
			for (int i = 0; i < animatorController.layers.Length; ++i) {
				for (int j = 0; j < baseStateMachine6.stateMachines.Length; ++j) {
					if (baseStateMachine6.stateMachines[j].stateMachine.name == "Roll") {
						baseStateMachine6.RemoveStateMachine(baseStateMachine6.stateMachines[j].stateMachine);
						break;
					}
				}
			}

			// AnimationClip references.
			var m_SwordRollIdleAnimationClip23502Path = AssetDatabase.GUIDToAssetPath("0e24b81f894df92458c8e8a5588622de"); 
			var m_SwordRollIdleAnimationClip23502 = AnimatorBuilder.GetAnimationClip(m_SwordRollIdleAnimationClip23502Path, "SwordRollIdle");
			var m_KatanaRollIdleAnimationClip16394Path = AssetDatabase.GUIDToAssetPath("e9734da6e3b579041b65e31fa14305f9"); 
			var m_KatanaRollIdleAnimationClip16394 = AnimatorBuilder.GetAnimationClip(m_KatanaRollIdleAnimationClip16394Path, "KatanaRollIdle");
			var m_KnifeRollIdleAnimationClip20472Path = AssetDatabase.GUIDToAssetPath("c7bccd5ba3087d24ab9b21627c71d781"); 
			var m_KnifeRollIdleAnimationClip20472 = AnimatorBuilder.GetAnimationClip(m_KnifeRollIdleAnimationClip20472Path, "KnifeRollIdle");

			// State Machine.
			var m_RollAnimatorStateMachine420366 = baseStateMachine6.AddStateMachine("Roll", new Vector3(852f, 60f, 0f));

			// States.
			var m_AssaultRifleAnimatorState420284 = m_RollAnimatorStateMachine420366.AddState("Assault Rifle", new Vector3(384f, -228f, 0f));
			m_AssaultRifleAnimatorState420284.motion = m_AssaultRifleRollIdleAnimationClip15330;
			m_AssaultRifleAnimatorState420284.cycleOffset = 0f;
			m_AssaultRifleAnimatorState420284.cycleOffsetParameterActive = false;
			m_AssaultRifleAnimatorState420284.iKOnFeet = false;
			m_AssaultRifleAnimatorState420284.mirror = false;
			m_AssaultRifleAnimatorState420284.mirrorParameterActive = false;
			m_AssaultRifleAnimatorState420284.speed = 1f;
			m_AssaultRifleAnimatorState420284.speedParameterActive = false;
			m_AssaultRifleAnimatorState420284.writeDefaultValues = true;

			var m_SniperRifleAnimatorState420204 = m_RollAnimatorStateMachine420366.AddState("Sniper Rifle", new Vector3(384f, 252f, 0f));
			m_SniperRifleAnimatorState420204.motion = m_SniperRifleRollIdleAnimationClip20776;
			m_SniperRifleAnimatorState420204.cycleOffset = 0f;
			m_SniperRifleAnimatorState420204.cycleOffsetParameterActive = false;
			m_SniperRifleAnimatorState420204.iKOnFeet = false;
			m_SniperRifleAnimatorState420204.mirror = false;
			m_SniperRifleAnimatorState420204.mirrorParameterActive = false;
			m_SniperRifleAnimatorState420204.speed = 1f;
			m_SniperRifleAnimatorState420204.speedParameterActive = false;
			m_SniperRifleAnimatorState420204.writeDefaultValues = true;

			var m_ShotgunAnimatorState420106 = m_RollAnimatorStateMachine420366.AddState("Shotgun", new Vector3(384f, 192f, 0f));
			m_ShotgunAnimatorState420106.motion = m_ShotgunRollIdleAnimationClip16290;
			m_ShotgunAnimatorState420106.cycleOffset = 0f;
			m_ShotgunAnimatorState420106.cycleOffsetParameterActive = false;
			m_ShotgunAnimatorState420106.iKOnFeet = false;
			m_ShotgunAnimatorState420106.mirror = false;
			m_ShotgunAnimatorState420106.mirrorParameterActive = false;
			m_ShotgunAnimatorState420106.speed = 1f;
			m_ShotgunAnimatorState420106.speedParameterActive = false;
			m_ShotgunAnimatorState420106.writeDefaultValues = true;

			var m_RocketLauncherAnimatorState420072 = m_RollAnimatorStateMachine420366.AddState("Rocket Launcher", new Vector3(384f, 132f, 0f));
			m_RocketLauncherAnimatorState420072.motion = m_RocketLauncherRollIdleAnimationClip15104;
			m_RocketLauncherAnimatorState420072.cycleOffset = 0f;
			m_RocketLauncherAnimatorState420072.cycleOffsetParameterActive = false;
			m_RocketLauncherAnimatorState420072.iKOnFeet = false;
			m_RocketLauncherAnimatorState420072.mirror = false;
			m_RocketLauncherAnimatorState420072.mirrorParameterActive = false;
			m_RocketLauncherAnimatorState420072.speed = 1f;
			m_RocketLauncherAnimatorState420072.speedParameterActive = false;
			m_RocketLauncherAnimatorState420072.writeDefaultValues = true;

			var m_DualPistolAnimatorState420318 = m_RollAnimatorStateMachine420366.AddState("Dual Pistol", new Vector3(384f, -108f, 0f));
			m_DualPistolAnimatorState420318.motion = m_DualPistolRollIdleAnimationClip22182;
			m_DualPistolAnimatorState420318.cycleOffset = 0f;
			m_DualPistolAnimatorState420318.cycleOffsetParameterActive = false;
			m_DualPistolAnimatorState420318.iKOnFeet = false;
			m_DualPistolAnimatorState420318.mirror = false;
			m_DualPistolAnimatorState420318.mirrorParameterActive = false;
			m_DualPistolAnimatorState420318.speed = 1f;
			m_DualPistolAnimatorState420318.speedParameterActive = false;
			m_DualPistolAnimatorState420318.writeDefaultValues = true;

			var m_PistolAnimatorState420172 = m_RollAnimatorStateMachine420366.AddState("Pistol", new Vector3(384f, 72f, 0f));
			m_PistolAnimatorState420172.motion = m_PistolRollIdleAnimationClip15510;
			m_PistolAnimatorState420172.cycleOffset = 0f;
			m_PistolAnimatorState420172.cycleOffsetParameterActive = false;
			m_PistolAnimatorState420172.iKOnFeet = false;
			m_PistolAnimatorState420172.mirror = false;
			m_PistolAnimatorState420172.mirrorParameterActive = false;
			m_PistolAnimatorState420172.speed = 1f;
			m_PistolAnimatorState420172.speedParameterActive = false;
			m_PistolAnimatorState420172.writeDefaultValues = true;

			var m_SwordAnimatorState420316 = m_RollAnimatorStateMachine420366.AddState("Sword", new Vector3(384f, 312f, 0f));
			m_SwordAnimatorState420316.motion = m_SwordRollIdleAnimationClip23502;
			m_SwordAnimatorState420316.cycleOffset = 0f;
			m_SwordAnimatorState420316.cycleOffsetParameterActive = false;
			m_SwordAnimatorState420316.iKOnFeet = false;
			m_SwordAnimatorState420316.mirror = false;
			m_SwordAnimatorState420316.mirrorParameterActive = false;
			m_SwordAnimatorState420316.speed = 1f;
			m_SwordAnimatorState420316.speedParameterActive = false;
			m_SwordAnimatorState420316.writeDefaultValues = true;

			var m_KatanaAnimatorState420052 = m_RollAnimatorStateMachine420366.AddState("Katana", new Vector3(384f, -48f, 0f));
			m_KatanaAnimatorState420052.motion = m_KatanaRollIdleAnimationClip16394;
			m_KatanaAnimatorState420052.cycleOffset = 0f;
			m_KatanaAnimatorState420052.cycleOffsetParameterActive = false;
			m_KatanaAnimatorState420052.iKOnFeet = false;
			m_KatanaAnimatorState420052.mirror = false;
			m_KatanaAnimatorState420052.mirrorParameterActive = false;
			m_KatanaAnimatorState420052.speed = 1f;
			m_KatanaAnimatorState420052.speedParameterActive = false;
			m_KatanaAnimatorState420052.writeDefaultValues = true;

			var m_BowAnimatorState420220 = m_RollAnimatorStateMachine420366.AddState("Bow", new Vector3(384f, -168f, 0f));
			m_BowAnimatorState420220.motion = m_BowRollIdleAnimationClip22056;
			m_BowAnimatorState420220.cycleOffset = 0f;
			m_BowAnimatorState420220.cycleOffsetParameterActive = false;
			m_BowAnimatorState420220.iKOnFeet = false;
			m_BowAnimatorState420220.mirror = false;
			m_BowAnimatorState420220.mirrorParameterActive = false;
			m_BowAnimatorState420220.speed = 1f;
			m_BowAnimatorState420220.speedParameterActive = false;
			m_BowAnimatorState420220.writeDefaultValues = true;

			var m_KnifeAnimatorState420216 = m_RollAnimatorStateMachine420366.AddState("Knife", new Vector3(384f, 12f, 0f));
			m_KnifeAnimatorState420216.motion = m_KnifeRollIdleAnimationClip20472;
			m_KnifeAnimatorState420216.cycleOffset = 0f;
			m_KnifeAnimatorState420216.cycleOffsetParameterActive = false;
			m_KnifeAnimatorState420216.iKOnFeet = false;
			m_KnifeAnimatorState420216.mirror = false;
			m_KnifeAnimatorState420216.mirrorParameterActive = false;
			m_KnifeAnimatorState420216.speed = 1f;
			m_KnifeAnimatorState420216.speedParameterActive = false;
			m_KnifeAnimatorState420216.writeDefaultValues = true;

			// State Transitions.
			var m_AnimatorStateTransition419310 = m_AssaultRifleAnimatorState420284.AddExitTransition();
			m_AnimatorStateTransition419310.canTransitionToSelf = true;
			m_AnimatorStateTransition419310.duration = 0.15f;
			m_AnimatorStateTransition419310.exitTime = 0f;
			m_AnimatorStateTransition419310.hasExitTime = false;
			m_AnimatorStateTransition419310.hasFixedDuration = true;
			m_AnimatorStateTransition419310.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419310.offset = 0f;
			m_AnimatorStateTransition419310.orderedInterruption = true;
			m_AnimatorStateTransition419310.isExit = true;
			m_AnimatorStateTransition419310.mute = false;
			m_AnimatorStateTransition419310.solo = false;
			m_AnimatorStateTransition419310.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418724 = m_SniperRifleAnimatorState420204.AddExitTransition();
			m_AnimatorStateTransition418724.canTransitionToSelf = true;
			m_AnimatorStateTransition418724.duration = 0.15f;
			m_AnimatorStateTransition418724.exitTime = 0f;
			m_AnimatorStateTransition418724.hasExitTime = false;
			m_AnimatorStateTransition418724.hasFixedDuration = true;
			m_AnimatorStateTransition418724.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418724.offset = 0f;
			m_AnimatorStateTransition418724.orderedInterruption = true;
			m_AnimatorStateTransition418724.isExit = true;
			m_AnimatorStateTransition418724.mute = false;
			m_AnimatorStateTransition418724.solo = false;
			m_AnimatorStateTransition418724.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419268 = m_ShotgunAnimatorState420106.AddExitTransition();
			m_AnimatorStateTransition419268.canTransitionToSelf = true;
			m_AnimatorStateTransition419268.duration = 0.15f;
			m_AnimatorStateTransition419268.exitTime = 0f;
			m_AnimatorStateTransition419268.hasExitTime = false;
			m_AnimatorStateTransition419268.hasFixedDuration = true;
			m_AnimatorStateTransition419268.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419268.offset = 0f;
			m_AnimatorStateTransition419268.orderedInterruption = true;
			m_AnimatorStateTransition419268.isExit = true;
			m_AnimatorStateTransition419268.mute = false;
			m_AnimatorStateTransition419268.solo = false;
			m_AnimatorStateTransition419268.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418866 = m_RocketLauncherAnimatorState420072.AddExitTransition();
			m_AnimatorStateTransition418866.canTransitionToSelf = true;
			m_AnimatorStateTransition418866.duration = 0.15f;
			m_AnimatorStateTransition418866.exitTime = 0f;
			m_AnimatorStateTransition418866.hasExitTime = false;
			m_AnimatorStateTransition418866.hasFixedDuration = true;
			m_AnimatorStateTransition418866.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418866.offset = 0f;
			m_AnimatorStateTransition418866.orderedInterruption = true;
			m_AnimatorStateTransition418866.isExit = true;
			m_AnimatorStateTransition418866.mute = false;
			m_AnimatorStateTransition418866.solo = false;
			m_AnimatorStateTransition418866.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419272 = m_DualPistolAnimatorState420318.AddExitTransition();
			m_AnimatorStateTransition419272.canTransitionToSelf = true;
			m_AnimatorStateTransition419272.duration = 0.15f;
			m_AnimatorStateTransition419272.exitTime = 0f;
			m_AnimatorStateTransition419272.hasExitTime = false;
			m_AnimatorStateTransition419272.hasFixedDuration = true;
			m_AnimatorStateTransition419272.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419272.offset = 0f;
			m_AnimatorStateTransition419272.orderedInterruption = true;
			m_AnimatorStateTransition419272.isExit = true;
			m_AnimatorStateTransition419272.mute = false;
			m_AnimatorStateTransition419272.solo = false;
			m_AnimatorStateTransition419272.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419328 = m_PistolAnimatorState420172.AddExitTransition();
			m_AnimatorStateTransition419328.canTransitionToSelf = true;
			m_AnimatorStateTransition419328.duration = 0.15f;
			m_AnimatorStateTransition419328.exitTime = 0f;
			m_AnimatorStateTransition419328.hasExitTime = false;
			m_AnimatorStateTransition419328.hasFixedDuration = true;
			m_AnimatorStateTransition419328.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419328.offset = 0f;
			m_AnimatorStateTransition419328.orderedInterruption = true;
			m_AnimatorStateTransition419328.isExit = true;
			m_AnimatorStateTransition419328.mute = false;
			m_AnimatorStateTransition419328.solo = false;
			m_AnimatorStateTransition419328.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419086 = m_SwordAnimatorState420316.AddExitTransition();
			m_AnimatorStateTransition419086.canTransitionToSelf = true;
			m_AnimatorStateTransition419086.duration = 0.15f;
			m_AnimatorStateTransition419086.exitTime = 0f;
			m_AnimatorStateTransition419086.hasExitTime = false;
			m_AnimatorStateTransition419086.hasFixedDuration = true;
			m_AnimatorStateTransition419086.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419086.offset = 0f;
			m_AnimatorStateTransition419086.orderedInterruption = false;
			m_AnimatorStateTransition419086.isExit = true;
			m_AnimatorStateTransition419086.mute = false;
			m_AnimatorStateTransition419086.solo = false;
			m_AnimatorStateTransition419086.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419754 = m_KatanaAnimatorState420052.AddExitTransition();
			m_AnimatorStateTransition419754.canTransitionToSelf = true;
			m_AnimatorStateTransition419754.duration = 15f;
			m_AnimatorStateTransition419754.exitTime = 0f;
			m_AnimatorStateTransition419754.hasExitTime = false;
			m_AnimatorStateTransition419754.hasFixedDuration = true;
			m_AnimatorStateTransition419754.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419754.offset = 0f;
			m_AnimatorStateTransition419754.orderedInterruption = true;
			m_AnimatorStateTransition419754.isExit = true;
			m_AnimatorStateTransition419754.mute = false;
			m_AnimatorStateTransition419754.solo = false;
			m_AnimatorStateTransition419754.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition419942 = m_BowAnimatorState420220.AddExitTransition();
			m_AnimatorStateTransition419942.canTransitionToSelf = true;
			m_AnimatorStateTransition419942.duration = 0.15f;
			m_AnimatorStateTransition419942.exitTime = 0f;
			m_AnimatorStateTransition419942.hasExitTime = false;
			m_AnimatorStateTransition419942.hasFixedDuration = true;
			m_AnimatorStateTransition419942.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419942.offset = 0f;
			m_AnimatorStateTransition419942.orderedInterruption = true;
			m_AnimatorStateTransition419942.isExit = true;
			m_AnimatorStateTransition419942.mute = false;
			m_AnimatorStateTransition419942.solo = false;
			m_AnimatorStateTransition419942.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			var m_AnimatorStateTransition418940 = m_KnifeAnimatorState420216.AddExitTransition();
			m_AnimatorStateTransition418940.canTransitionToSelf = true;
			m_AnimatorStateTransition418940.duration = 0.15f;
			m_AnimatorStateTransition418940.exitTime = 0f;
			m_AnimatorStateTransition418940.hasExitTime = false;
			m_AnimatorStateTransition418940.hasFixedDuration = true;
			m_AnimatorStateTransition418940.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418940.offset = 0f;
			m_AnimatorStateTransition418940.orderedInterruption = true;
			m_AnimatorStateTransition418940.isExit = true;
			m_AnimatorStateTransition418940.mute = false;
			m_AnimatorStateTransition418940.solo = false;
			m_AnimatorStateTransition418940.AddCondition(AnimatorConditionMode.NotEqual, 102f, "AbilityIndex");

			// State Machine Defaults.
			m_RollAnimatorStateMachine420366.anyStatePosition = new Vector3(48f, 48f, 0f);
			m_RollAnimatorStateMachine420366.defaultState = m_AssaultRifleAnimatorState420284;
			m_RollAnimatorStateMachine420366.entryPosition = new Vector3(48f, 0f, 0f);
			m_RollAnimatorStateMachine420366.exitPosition = new Vector3(768f, 36f, 0f);
			m_RollAnimatorStateMachine420366.parentStateMachinePosition = new Vector3(744f, -48f, 0f);

			// State Machine Transitions.
			var m_AnimatorStateTransition419206 = baseStateMachine6.AddAnyStateTransition(m_SwordAnimatorState420316);
			m_AnimatorStateTransition419206.canTransitionToSelf = false;
			m_AnimatorStateTransition419206.duration = 0.05f;
			m_AnimatorStateTransition419206.exitTime = 0.75f;
			m_AnimatorStateTransition419206.hasExitTime = false;
			m_AnimatorStateTransition419206.hasFixedDuration = true;
			m_AnimatorStateTransition419206.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419206.offset = 0f;
			m_AnimatorStateTransition419206.orderedInterruption = true;
			m_AnimatorStateTransition419206.isExit = false;
			m_AnimatorStateTransition419206.mute = false;
			m_AnimatorStateTransition419206.solo = false;
			m_AnimatorStateTransition419206.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419206.AddCondition(AnimatorConditionMode.Equals, 22f, "Slot0ItemID");

			var m_AnimatorStateTransition418498 = baseStateMachine6.AddAnyStateTransition(m_KnifeAnimatorState420216);
			m_AnimatorStateTransition418498.canTransitionToSelf = false;
			m_AnimatorStateTransition418498.duration = 0.05f;
			m_AnimatorStateTransition418498.exitTime = 0.75f;
			m_AnimatorStateTransition418498.hasExitTime = false;
			m_AnimatorStateTransition418498.hasFixedDuration = true;
			m_AnimatorStateTransition418498.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418498.offset = 0f;
			m_AnimatorStateTransition418498.orderedInterruption = true;
			m_AnimatorStateTransition418498.isExit = false;
			m_AnimatorStateTransition418498.mute = false;
			m_AnimatorStateTransition418498.solo = false;
			m_AnimatorStateTransition418498.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418498.AddCondition(AnimatorConditionMode.Equals, 23f, "Slot0ItemID");

			var m_AnimatorStateTransition419908 = baseStateMachine6.AddAnyStateTransition(m_KatanaAnimatorState420052);
			m_AnimatorStateTransition419908.canTransitionToSelf = false;
			m_AnimatorStateTransition419908.duration = 0.05f;
			m_AnimatorStateTransition419908.exitTime = 0.75f;
			m_AnimatorStateTransition419908.hasExitTime = false;
			m_AnimatorStateTransition419908.hasFixedDuration = true;
			m_AnimatorStateTransition419908.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419908.offset = 0f;
			m_AnimatorStateTransition419908.orderedInterruption = true;
			m_AnimatorStateTransition419908.isExit = false;
			m_AnimatorStateTransition419908.mute = false;
			m_AnimatorStateTransition419908.solo = false;
			m_AnimatorStateTransition419908.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419908.AddCondition(AnimatorConditionMode.Equals, 24f, "Slot0ItemID");

			var m_AnimatorStateTransition419046 = baseStateMachine6.AddAnyStateTransition(m_BowAnimatorState420220);
			m_AnimatorStateTransition419046.canTransitionToSelf = false;
			m_AnimatorStateTransition419046.duration = 0.05f;
			m_AnimatorStateTransition419046.exitTime = 0.75f;
			m_AnimatorStateTransition419046.hasExitTime = false;
			m_AnimatorStateTransition419046.hasFixedDuration = true;
			m_AnimatorStateTransition419046.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419046.offset = 0f;
			m_AnimatorStateTransition419046.orderedInterruption = true;
			m_AnimatorStateTransition419046.isExit = false;
			m_AnimatorStateTransition419046.mute = false;
			m_AnimatorStateTransition419046.solo = false;
			m_AnimatorStateTransition419046.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419046.AddCondition(AnimatorConditionMode.Equals, 4f, "Slot1ItemID");

			var m_AnimatorStateTransition419264 = baseStateMachine6.AddAnyStateTransition(m_DualPistolAnimatorState420318);
			m_AnimatorStateTransition419264.canTransitionToSelf = false;
			m_AnimatorStateTransition419264.duration = 0.05f;
			m_AnimatorStateTransition419264.exitTime = 0.75f;
			m_AnimatorStateTransition419264.hasExitTime = false;
			m_AnimatorStateTransition419264.hasFixedDuration = true;
			m_AnimatorStateTransition419264.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419264.offset = 0f;
			m_AnimatorStateTransition419264.orderedInterruption = true;
			m_AnimatorStateTransition419264.isExit = false;
			m_AnimatorStateTransition419264.mute = false;
			m_AnimatorStateTransition419264.solo = false;
			m_AnimatorStateTransition419264.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419264.AddCondition(AnimatorConditionMode.Equals, 2f, "Slot0ItemID");
			m_AnimatorStateTransition419264.AddCondition(AnimatorConditionMode.Equals, 2f, "Slot1ItemID");

			var m_AnimatorStateTransition419468 = baseStateMachine6.AddAnyStateTransition(m_ShotgunAnimatorState420106);
			m_AnimatorStateTransition419468.canTransitionToSelf = false;
			m_AnimatorStateTransition419468.duration = 0.05f;
			m_AnimatorStateTransition419468.exitTime = 0.75f;
			m_AnimatorStateTransition419468.hasExitTime = false;
			m_AnimatorStateTransition419468.hasFixedDuration = true;
			m_AnimatorStateTransition419468.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419468.offset = 0f;
			m_AnimatorStateTransition419468.orderedInterruption = true;
			m_AnimatorStateTransition419468.isExit = false;
			m_AnimatorStateTransition419468.mute = false;
			m_AnimatorStateTransition419468.solo = false;
			m_AnimatorStateTransition419468.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419468.AddCondition(AnimatorConditionMode.Equals, 3f, "Slot0ItemID");

			var m_AnimatorStateTransition419658 = baseStateMachine6.AddAnyStateTransition(m_PistolAnimatorState420172);
			m_AnimatorStateTransition419658.canTransitionToSelf = false;
			m_AnimatorStateTransition419658.duration = 0.05f;
			m_AnimatorStateTransition419658.exitTime = 0.75f;
			m_AnimatorStateTransition419658.hasExitTime = false;
			m_AnimatorStateTransition419658.hasFixedDuration = true;
			m_AnimatorStateTransition419658.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419658.offset = 0f;
			m_AnimatorStateTransition419658.orderedInterruption = true;
			m_AnimatorStateTransition419658.isExit = false;
			m_AnimatorStateTransition419658.mute = false;
			m_AnimatorStateTransition419658.solo = false;
			m_AnimatorStateTransition419658.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition419658.AddCondition(AnimatorConditionMode.Equals, 2f, "Slot0ItemID");
			m_AnimatorStateTransition419658.AddCondition(AnimatorConditionMode.NotEqual, 2f, "Slot1ItemID");

			var m_AnimatorStateTransition418488 = baseStateMachine6.AddAnyStateTransition(m_RocketLauncherAnimatorState420072);
			m_AnimatorStateTransition418488.canTransitionToSelf = false;
			m_AnimatorStateTransition418488.duration = 0.05f;
			m_AnimatorStateTransition418488.exitTime = 0.75f;
			m_AnimatorStateTransition418488.hasExitTime = false;
			m_AnimatorStateTransition418488.hasFixedDuration = true;
			m_AnimatorStateTransition418488.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418488.offset = 0f;
			m_AnimatorStateTransition418488.orderedInterruption = true;
			m_AnimatorStateTransition418488.isExit = false;
			m_AnimatorStateTransition418488.mute = false;
			m_AnimatorStateTransition418488.solo = false;
			m_AnimatorStateTransition418488.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418488.AddCondition(AnimatorConditionMode.Equals, 6f, "Slot0ItemID");

			var m_AnimatorStateTransition418638 = baseStateMachine6.AddAnyStateTransition(m_AssaultRifleAnimatorState420284);
			m_AnimatorStateTransition418638.canTransitionToSelf = false;
			m_AnimatorStateTransition418638.duration = 0.05f;
			m_AnimatorStateTransition418638.exitTime = 0.75f;
			m_AnimatorStateTransition418638.hasExitTime = false;
			m_AnimatorStateTransition418638.hasFixedDuration = true;
			m_AnimatorStateTransition418638.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418638.offset = 0f;
			m_AnimatorStateTransition418638.orderedInterruption = true;
			m_AnimatorStateTransition418638.isExit = false;
			m_AnimatorStateTransition418638.mute = false;
			m_AnimatorStateTransition418638.solo = false;
			m_AnimatorStateTransition418638.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418638.AddCondition(AnimatorConditionMode.Equals, 1f, "Slot0ItemID");

			var m_AnimatorStateTransition418620 = baseStateMachine6.AddAnyStateTransition(m_SniperRifleAnimatorState420204);
			m_AnimatorStateTransition418620.canTransitionToSelf = false;
			m_AnimatorStateTransition418620.duration = 0.05f;
			m_AnimatorStateTransition418620.exitTime = 0.75f;
			m_AnimatorStateTransition418620.hasExitTime = false;
			m_AnimatorStateTransition418620.hasFixedDuration = true;
			m_AnimatorStateTransition418620.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418620.offset = 0f;
			m_AnimatorStateTransition418620.orderedInterruption = true;
			m_AnimatorStateTransition418620.isExit = false;
			m_AnimatorStateTransition418620.mute = false;
			m_AnimatorStateTransition418620.solo = false;
			m_AnimatorStateTransition418620.AddCondition(AnimatorConditionMode.Equals, 102f, "AbilityIndex");
			m_AnimatorStateTransition418620.AddCondition(AnimatorConditionMode.Equals, 5f, "Slot0ItemID");
		}
	}
}
