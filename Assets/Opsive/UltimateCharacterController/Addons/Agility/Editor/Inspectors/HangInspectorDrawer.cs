using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using Opsive.UltimateCharacterController.Addons.Agility;
using Opsive.UltimateCharacterController.Editor.Inspectors.Character.Abilities;
using Opsive.UltimateCharacterController.Editor.Inspectors.Utility;
using Opsive.UltimateCharacterController.Editor.Utility;

namespace Opsive.UltimateCharacterController.Editor.Inspectors.Addons.Agility
{
	/// <summary>
	/// Draws a custom inspector for the Hang Ability.
	/// </summary>
	[InspectorDrawer(typeof(Hang))]
	public class HangInspectorDrawer : DetectObjectAbilityBaseInspectorDrawer
    {
        /// <summary>
        /// Draws the fields related to the inspector drawer.
        /// </summary>
        /// <param name="target">The object that is being drawn.</param>
        /// <param name="parent">The Unity Object that the object belongs to.</param>
        protected override void DrawInspectorDrawerFields(object target, Object parent)
        {
            // Draw AllowMovements manually so it'll use the MaskField.
            var allowedMovements = (int)InspectorUtility.GetFieldValue<Hang.AllowedMovement>(target, "m_AllowedMovements");
            var allowedMovementsString = System.Enum.GetNames(typeof(Hang.AllowedMovement));
            var value = EditorGUILayout.MaskField(new GUIContent("Allowed Movements", InspectorUtility.GetFieldTooltip(target, "m_AllowedMovements")), allowedMovements, allowedMovementsString);
            if (value != allowedMovements) {
                InspectorUtility.SetFieldValue(target, "m_AllowedMovements", value);
            }

            base.DrawInspectorDrawerFields(target, parent);
        }

		// ------------------------------------------- Start Generated Code -------------------------------------------
		// ------- Do NOT make any changes below. Changes will be removed when the animator is generated again. -------
		// ------------------------------------------------------------------------------------------------------------

		/// <summary>
		/// Returns true if the ability can build to the animator.
		/// </summary>
		public override bool CanBuildAnimator { get { return true; } }

		/// <summary>
		/// An editor only method which can add the abilities states/transitions to the animator.
		/// </summary>
		/// <param name="animatorController">The Animator Controller to add the states to.</param>
		/// <param name="firstPersonAnimatorController">The first person Animator Controller to add the states to.</param>
		public override void BuildAnimator(AnimatorController animatorController, AnimatorController firstPersonAnimatorController)
		{
			var baseStateMachine12 = animatorController.layers[12].stateMachine;

			// The state machine should start fresh.
			for (int i = 0; i < animatorController.layers.Length; ++i) {
				for (int j = 0; j < baseStateMachine12.stateMachines.Length; ++j) {
					if (baseStateMachine12.stateMachines[j].stateMachine.name == "Hang") {
						baseStateMachine12.RemoveStateMachine(baseStateMachine12.stateMachines[j].stateMachine);
						break;
					}
				}
			}

			// AnimationClip references.
			var m_HangMovementAnimationClip19834Path = AssetDatabase.GUIDToAssetPath("fd8bcd7a5867d3c489f6e8c561390f5c"); 
			var m_HangMovementAnimationClip19834 = AnimatorBuilder.GetAnimationClip(m_HangMovementAnimationClip19834Path, "HangMovement");
			var m_HangIdleAnimationClip19712Path = AssetDatabase.GUIDToAssetPath("bddb2b4a0e5f12f49b71618b412ff5aa"); 
			var m_HangIdleAnimationClip19712 = AnimatorBuilder.GetAnimationClip(m_HangIdleAnimationClip19712Path, "HangIdle");
			var m_HangStartAnimationClip20854Path = AssetDatabase.GUIDToAssetPath("053d29fbe247cbc4f98bd80634cdad46"); 
			var m_HangStartAnimationClip20854 = AnimatorBuilder.GetAnimationClip(m_HangStartAnimationClip20854Path, "HangStart");
			var m_TransferRightAnimationClip21160Path = AssetDatabase.GUIDToAssetPath("04c74e7c306a6cf48813b0ffc2e992db"); 
			var m_TransferRightAnimationClip21160 = AnimatorBuilder.GetAnimationClip(m_TransferRightAnimationClip21160Path, "TransferRight");
			var m_TransferLeftAnimationClip20624Path = AssetDatabase.GUIDToAssetPath("77339d9bf70da1441b2e6b4de0bbc3d8"); 
			var m_TransferLeftAnimationClip20624 = AnimatorBuilder.GetAnimationClip(m_TransferLeftAnimationClip20624Path, "TransferLeft");
			var m_TransferUpAnimationClip14472Path = AssetDatabase.GUIDToAssetPath("9409cd14629cef747a96eac1d5fa22f7"); 
			var m_TransferUpAnimationClip14472 = AnimatorBuilder.GetAnimationClip(m_TransferUpAnimationClip14472Path, "TransferUp");
			var m_DropStartAnimationClip13902Path = AssetDatabase.GUIDToAssetPath("0b38d3330a39f684ab19e45f90a6eead"); 
			var m_DropStartAnimationClip13902 = AnimatorBuilder.GetAnimationClip(m_DropStartAnimationClip13902Path, "DropStart");
			var m_PullUpAnimationClip13394Path = AssetDatabase.GUIDToAssetPath("c7d5ba824b936f649bbb2747a4a3fc4a"); 
			var m_PullUpAnimationClip13394 = AnimatorBuilder.GetAnimationClip(m_PullUpAnimationClip13394Path, "PullUp");
			var m_TransferDownAnimationClip20006Path = AssetDatabase.GUIDToAssetPath("2ee34eba64def1b44a6062121131cc13"); 
			var m_TransferDownAnimationClip20006 = AnimatorBuilder.GetAnimationClip(m_TransferDownAnimationClip20006Path, "TransferDown");
			var m_HangJumpStartAnimationClip20852Path = AssetDatabase.GUIDToAssetPath("053d29fbe247cbc4f98bd80634cdad46"); 
			var m_HangJumpStartAnimationClip20852 = AnimatorBuilder.GetAnimationClip(m_HangJumpStartAnimationClip20852Path, "HangJumpStart");
			var m_DropStartLedgeStrafeAnimationClip19542Path = AssetDatabase.GUIDToAssetPath("02e3ce0a2c624334d9af402815fa5f5f"); 
			var m_DropStartLedgeStrafeAnimationClip19542 = AnimatorBuilder.GetAnimationClip(m_DropStartLedgeStrafeAnimationClip19542Path, "DropStartLedgeStrafe");

			// State Machine.
			var m_HangAnimatorStateMachine420340 = baseStateMachine12.AddStateMachine("Hang", new Vector3(624f, 204f, 0f));

			// States.
			var m_HangAnimatorState419992 = m_HangAnimatorStateMachine420340.AddState("Hang", new Vector3(384f, -84f, 0f));
			var m_HangAnimatorState419992m_BlendTreeBlendTree417780 = new BlendTree();
			AssetDatabase.AddObjectToAsset(m_HangAnimatorState419992m_BlendTreeBlendTree417780, animatorController);
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.hideFlags = HideFlags.HideInHierarchy;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.blendParameter = "HorizontalMovement";
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.blendParameterY = "HorizontalMovement";
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.blendType = BlendTreeType.Simple1D;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.maxThreshold = 1f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.minThreshold = -1f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.name = "Blend Tree";
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.useAutomaticThresholds = false;
			var m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0=  new ChildMotion();
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0.motion = m_HangMovementAnimationClip19834;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0.cycleOffset = 0f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0.directBlendParameter = "HorizontalMovement";
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0.mirror = false;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0.position = new Vector2(0f, 0f);
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0.threshold = -1f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0.timeScale = 1.4f;
			var m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1=  new ChildMotion();
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1.motion = m_HangIdleAnimationClip19712;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1.cycleOffset = 0f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1.directBlendParameter = "HorizontalMovement";
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1.mirror = false;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1.position = new Vector2(0f, 0f);
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1.threshold = 0f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1.timeScale = 1f;
			var m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2=  new ChildMotion();
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2.motion = m_HangMovementAnimationClip19834;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2.cycleOffset = 0f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2.directBlendParameter = "HorizontalMovement";
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2.mirror = false;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2.position = new Vector2(0f, 0f);
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2.threshold = 1f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2.timeScale = -1.4f;
			m_HangAnimatorState419992m_BlendTreeBlendTree417780.children = new ChildMotion[] {
				m_HangAnimatorState419992m_BlendTreeBlendTree417780Child0,
				m_HangAnimatorState419992m_BlendTreeBlendTree417780Child1,
				m_HangAnimatorState419992m_BlendTreeBlendTree417780Child2
			};
			m_HangAnimatorState419992.motion = m_HangAnimatorState419992m_BlendTreeBlendTree417780;
			m_HangAnimatorState419992.cycleOffset = 0f;
			m_HangAnimatorState419992.cycleOffsetParameterActive = false;
			m_HangAnimatorState419992.iKOnFeet = false;
			m_HangAnimatorState419992.mirror = false;
			m_HangAnimatorState419992.mirrorParameterActive = false;
			m_HangAnimatorState419992.speed = 1f;
			m_HangAnimatorState419992.speedParameterActive = false;
			m_HangAnimatorState419992.writeDefaultValues = false;

			var m_HangStartAnimatorState420088 = m_HangAnimatorStateMachine420340.AddState("Hang Start", new Vector3(384f, -192f, 0f));
			m_HangStartAnimatorState420088.motion = m_HangStartAnimationClip20854;
			m_HangStartAnimatorState420088.cycleOffset = 0f;
			m_HangStartAnimatorState420088.cycleOffsetParameterActive = false;
			m_HangStartAnimatorState420088.iKOnFeet = false;
			m_HangStartAnimatorState420088.mirror = false;
			m_HangStartAnimatorState420088.mirrorParameterActive = false;
			m_HangStartAnimatorState420088.speed = 2f;
			m_HangStartAnimatorState420088.speedParameterActive = false;
			m_HangStartAnimatorState420088.writeDefaultValues = false;

			var m_TransferRightAnimatorState420298 = m_HangAnimatorStateMachine420340.AddState("Transfer Right", new Vector3(588f, 12f, 0f));
			m_TransferRightAnimatorState420298.motion = m_TransferRightAnimationClip21160;
			m_TransferRightAnimatorState420298.cycleOffset = 0f;
			m_TransferRightAnimatorState420298.cycleOffsetParameterActive = false;
			m_TransferRightAnimatorState420298.iKOnFeet = true;
			m_TransferRightAnimatorState420298.mirror = false;
			m_TransferRightAnimatorState420298.mirrorParameterActive = false;
			m_TransferRightAnimatorState420298.speed = 1.4f;
			m_TransferRightAnimatorState420298.speedParameterActive = false;
			m_TransferRightAnimatorState420298.writeDefaultValues = true;

			var m_TransferLeftAnimatorState420068 = m_HangAnimatorStateMachine420340.AddState("Transfer Left", new Vector3(180f, 12f, 0f));
			m_TransferLeftAnimatorState420068.motion = m_TransferLeftAnimationClip20624;
			m_TransferLeftAnimatorState420068.cycleOffset = 0f;
			m_TransferLeftAnimatorState420068.cycleOffsetParameterActive = false;
			m_TransferLeftAnimatorState420068.iKOnFeet = true;
			m_TransferLeftAnimatorState420068.mirror = false;
			m_TransferLeftAnimatorState420068.mirrorParameterActive = false;
			m_TransferLeftAnimatorState420068.speed = 1.4f;
			m_TransferLeftAnimatorState420068.speedParameterActive = false;
			m_TransferLeftAnimatorState420068.writeDefaultValues = true;

			var m_TransferUpAnimatorState420130 = m_HangAnimatorStateMachine420340.AddState("Transfer Up", new Vector3(264f, 84f, 0f));
			m_TransferUpAnimatorState420130.motion = m_TransferUpAnimationClip14472;
			m_TransferUpAnimatorState420130.cycleOffset = 0f;
			m_TransferUpAnimatorState420130.cycleOffsetParameterActive = false;
			m_TransferUpAnimatorState420130.iKOnFeet = true;
			m_TransferUpAnimatorState420130.mirror = false;
			m_TransferUpAnimatorState420130.mirrorParameterActive = false;
			m_TransferUpAnimatorState420130.speed = 1.4f;
			m_TransferUpAnimatorState420130.speedParameterActive = false;
			m_TransferUpAnimatorState420130.writeDefaultValues = true;

			var m_DropStartAnimatorState420278 = m_HangAnimatorStateMachine420340.AddState("Drop Start", new Vector3(132f, -192f, 0f));
			m_DropStartAnimatorState420278.motion = m_DropStartAnimationClip13902;
			m_DropStartAnimatorState420278.cycleOffset = 0f;
			m_DropStartAnimatorState420278.cycleOffsetParameterActive = false;
			m_DropStartAnimatorState420278.iKOnFeet = true;
			m_DropStartAnimatorState420278.mirror = false;
			m_DropStartAnimatorState420278.mirrorParameterActive = false;
			m_DropStartAnimatorState420278.speed = 1.3f;
			m_DropStartAnimatorState420278.speedParameterActive = false;
			m_DropStartAnimatorState420278.writeDefaultValues = true;

			var m_PullUpAnimatorState420074 = m_HangAnimatorStateMachine420340.AddState("Pull Up", new Vector3(384f, 168f, 0f));
			m_PullUpAnimatorState420074.motion = m_PullUpAnimationClip13394;
			m_PullUpAnimatorState420074.cycleOffset = 0f;
			m_PullUpAnimatorState420074.cycleOffsetParameter = "HorizontalMovement";
			m_PullUpAnimatorState420074.cycleOffsetParameterActive = false;
			m_PullUpAnimatorState420074.iKOnFeet = true;
			m_PullUpAnimatorState420074.mirror = false;
			m_PullUpAnimatorState420074.mirrorParameterActive = false;
			m_PullUpAnimatorState420074.speed = 1.4f;
			m_PullUpAnimatorState420074.speedParameterActive = false;
			m_PullUpAnimatorState420074.writeDefaultValues = true;

			var m_TransferDownAnimatorState420206 = m_HangAnimatorStateMachine420340.AddState("Transfer Down", new Vector3(504f, 84f, 0f));
			m_TransferDownAnimatorState420206.motion = m_TransferDownAnimationClip20006;
			m_TransferDownAnimatorState420206.cycleOffset = 0f;
			m_TransferDownAnimatorState420206.cycleOffsetParameterActive = false;
			m_TransferDownAnimatorState420206.iKOnFeet = true;
			m_TransferDownAnimatorState420206.mirror = false;
			m_TransferDownAnimatorState420206.mirrorParameterActive = false;
			m_TransferDownAnimatorState420206.speed = 1.4f;
			m_TransferDownAnimatorState420206.speedParameterActive = false;
			m_TransferDownAnimatorState420206.writeDefaultValues = true;

			var m_HangJumpStartAnimatorState420014 = m_HangAnimatorStateMachine420340.AddState("Hang Jump Start", new Vector3(624f, -192f, 0f));
			m_HangJumpStartAnimatorState420014.motion = m_HangJumpStartAnimationClip20852;
			m_HangJumpStartAnimatorState420014.cycleOffset = 0f;
			m_HangJumpStartAnimatorState420014.cycleOffsetParameterActive = false;
			m_HangJumpStartAnimatorState420014.iKOnFeet = false;
			m_HangJumpStartAnimatorState420014.mirror = false;
			m_HangJumpStartAnimatorState420014.mirrorParameterActive = false;
			m_HangJumpStartAnimatorState420014.speed = 1f;
			m_HangJumpStartAnimatorState420014.speedParameterActive = false;
			m_HangJumpStartAnimatorState420014.writeDefaultValues = false;

			var m_DropStartfromLedgeStrafeAnimatorState420234 = m_HangAnimatorStateMachine420340.AddState("Drop Start from Ledge Strafe", new Vector3(132f, -264f, 0f));
			m_DropStartfromLedgeStrafeAnimatorState420234.motion = m_DropStartLedgeStrafeAnimationClip19542;
			m_DropStartfromLedgeStrafeAnimatorState420234.cycleOffset = 0f;
			m_DropStartfromLedgeStrafeAnimatorState420234.cycleOffsetParameterActive = false;
			m_DropStartfromLedgeStrafeAnimatorState420234.iKOnFeet = true;
			m_DropStartfromLedgeStrafeAnimatorState420234.mirror = false;
			m_DropStartfromLedgeStrafeAnimatorState420234.mirrorParameterActive = false;
			m_DropStartfromLedgeStrafeAnimatorState420234.speed = 1.3f;
			m_DropStartfromLedgeStrafeAnimatorState420234.speedParameterActive = false;
			m_DropStartfromLedgeStrafeAnimatorState420234.writeDefaultValues = true;

			// State Transitions.
			var m_AnimatorStateTransition418840 = m_HangAnimatorState419992.AddExitTransition();
			m_AnimatorStateTransition418840.canTransitionToSelf = true;
			m_AnimatorStateTransition418840.duration = 0.15f;
			m_AnimatorStateTransition418840.exitTime = 0.91f;
			m_AnimatorStateTransition418840.hasExitTime = false;
			m_AnimatorStateTransition418840.hasFixedDuration = true;
			m_AnimatorStateTransition418840.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418840.offset = 0f;
			m_AnimatorStateTransition418840.orderedInterruption = true;
			m_AnimatorStateTransition418840.isExit = true;
			m_AnimatorStateTransition418840.mute = false;
			m_AnimatorStateTransition418840.solo = false;
			m_AnimatorStateTransition418840.AddCondition(AnimatorConditionMode.NotEqual, 104f, "AbilityIndex");

			var m_AnimatorStateTransition419130 = m_HangAnimatorState419992.AddTransition(m_TransferRightAnimatorState420298);
			m_AnimatorStateTransition419130.canTransitionToSelf = true;
			m_AnimatorStateTransition419130.duration = 0.15f;
			m_AnimatorStateTransition419130.exitTime = 0.91f;
			m_AnimatorStateTransition419130.hasExitTime = false;
			m_AnimatorStateTransition419130.hasFixedDuration = true;
			m_AnimatorStateTransition419130.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419130.offset = 0f;
			m_AnimatorStateTransition419130.orderedInterruption = true;
			m_AnimatorStateTransition419130.isExit = false;
			m_AnimatorStateTransition419130.mute = false;
			m_AnimatorStateTransition419130.solo = false;
			m_AnimatorStateTransition419130.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition419130.AddCondition(AnimatorConditionMode.Equals, 4f, "AbilityIntData");

			var m_AnimatorStateTransition419504 = m_HangAnimatorState419992.AddTransition(m_TransferUpAnimatorState420130);
			m_AnimatorStateTransition419504.canTransitionToSelf = true;
			m_AnimatorStateTransition419504.duration = 0.1f;
			m_AnimatorStateTransition419504.exitTime = 0.91f;
			m_AnimatorStateTransition419504.hasExitTime = false;
			m_AnimatorStateTransition419504.hasFixedDuration = true;
			m_AnimatorStateTransition419504.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419504.offset = 0f;
			m_AnimatorStateTransition419504.orderedInterruption = true;
			m_AnimatorStateTransition419504.isExit = false;
			m_AnimatorStateTransition419504.mute = false;
			m_AnimatorStateTransition419504.solo = false;
			m_AnimatorStateTransition419504.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition419504.AddCondition(AnimatorConditionMode.Equals, 3f, "AbilityIntData");

			var m_AnimatorStateTransition419246 = m_HangAnimatorState419992.AddTransition(m_TransferLeftAnimatorState420068);
			m_AnimatorStateTransition419246.canTransitionToSelf = true;
			m_AnimatorStateTransition419246.duration = 0.15f;
			m_AnimatorStateTransition419246.exitTime = 0.91f;
			m_AnimatorStateTransition419246.hasExitTime = false;
			m_AnimatorStateTransition419246.hasFixedDuration = true;
			m_AnimatorStateTransition419246.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419246.offset = 0f;
			m_AnimatorStateTransition419246.orderedInterruption = true;
			m_AnimatorStateTransition419246.isExit = false;
			m_AnimatorStateTransition419246.mute = false;
			m_AnimatorStateTransition419246.solo = false;
			m_AnimatorStateTransition419246.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition419246.AddCondition(AnimatorConditionMode.Equals, 5f, "AbilityIntData");

			var m_AnimatorStateTransition418760 = m_HangAnimatorState419992.AddTransition(m_PullUpAnimatorState420074);
			m_AnimatorStateTransition418760.canTransitionToSelf = true;
			m_AnimatorStateTransition418760.duration = 0f;
			m_AnimatorStateTransition418760.exitTime = 0.91f;
			m_AnimatorStateTransition418760.hasExitTime = false;
			m_AnimatorStateTransition418760.hasFixedDuration = false;
			m_AnimatorStateTransition418760.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418760.offset = 0f;
			m_AnimatorStateTransition418760.orderedInterruption = true;
			m_AnimatorStateTransition418760.isExit = false;
			m_AnimatorStateTransition418760.mute = false;
			m_AnimatorStateTransition418760.solo = false;
			m_AnimatorStateTransition418760.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition418760.AddCondition(AnimatorConditionMode.Equals, 7f, "AbilityIntData");

			var m_AnimatorStateTransition418878 = m_HangAnimatorState419992.AddTransition(m_TransferDownAnimatorState420206);
			m_AnimatorStateTransition418878.canTransitionToSelf = true;
			m_AnimatorStateTransition418878.duration = 0.15f;
			m_AnimatorStateTransition418878.exitTime = 0.91f;
			m_AnimatorStateTransition418878.hasExitTime = false;
			m_AnimatorStateTransition418878.hasFixedDuration = true;
			m_AnimatorStateTransition418878.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418878.offset = 0f;
			m_AnimatorStateTransition418878.orderedInterruption = true;
			m_AnimatorStateTransition418878.isExit = false;
			m_AnimatorStateTransition418878.mute = false;
			m_AnimatorStateTransition418878.solo = false;
			m_AnimatorStateTransition418878.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition418878.AddCondition(AnimatorConditionMode.Equals, 6f, "AbilityIntData");

			var m_AnimatorStateTransition418752 = m_HangStartAnimatorState420088.AddTransition(m_HangAnimatorState419992);
			m_AnimatorStateTransition418752.canTransitionToSelf = true;
			m_AnimatorStateTransition418752.duration = 0.05f;
			m_AnimatorStateTransition418752.exitTime = 0.95f;
			m_AnimatorStateTransition418752.hasExitTime = true;
			m_AnimatorStateTransition418752.hasFixedDuration = true;
			m_AnimatorStateTransition418752.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418752.offset = 0f;
			m_AnimatorStateTransition418752.orderedInterruption = true;
			m_AnimatorStateTransition418752.isExit = false;
			m_AnimatorStateTransition418752.mute = false;
			m_AnimatorStateTransition418752.solo = false;

			var m_AnimatorStateTransition419674 = m_TransferRightAnimatorState420298.AddTransition(m_HangAnimatorState419992);
			m_AnimatorStateTransition419674.canTransitionToSelf = true;
			m_AnimatorStateTransition419674.duration = 0.15f;
			m_AnimatorStateTransition419674.exitTime = 0.95f;
			m_AnimatorStateTransition419674.hasExitTime = true;
			m_AnimatorStateTransition419674.hasFixedDuration = true;
			m_AnimatorStateTransition419674.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419674.offset = 0f;
			m_AnimatorStateTransition419674.orderedInterruption = true;
			m_AnimatorStateTransition419674.isExit = false;
			m_AnimatorStateTransition419674.mute = false;
			m_AnimatorStateTransition419674.solo = false;

			var m_AnimatorStateTransition419466 = m_TransferLeftAnimatorState420068.AddTransition(m_HangAnimatorState419992);
			m_AnimatorStateTransition419466.canTransitionToSelf = true;
			m_AnimatorStateTransition419466.duration = 0.15f;
			m_AnimatorStateTransition419466.exitTime = 0.95f;
			m_AnimatorStateTransition419466.hasExitTime = true;
			m_AnimatorStateTransition419466.hasFixedDuration = true;
			m_AnimatorStateTransition419466.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419466.offset = 0f;
			m_AnimatorStateTransition419466.orderedInterruption = true;
			m_AnimatorStateTransition419466.isExit = false;
			m_AnimatorStateTransition419466.mute = false;
			m_AnimatorStateTransition419466.solo = false;

			var m_AnimatorStateTransition419590 = m_TransferUpAnimatorState420130.AddTransition(m_HangAnimatorState419992);
			m_AnimatorStateTransition419590.canTransitionToSelf = true;
			m_AnimatorStateTransition419590.duration = 0.15f;
			m_AnimatorStateTransition419590.exitTime = 0.95f;
			m_AnimatorStateTransition419590.hasExitTime = true;
			m_AnimatorStateTransition419590.hasFixedDuration = true;
			m_AnimatorStateTransition419590.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419590.offset = 0f;
			m_AnimatorStateTransition419590.orderedInterruption = true;
			m_AnimatorStateTransition419590.isExit = false;
			m_AnimatorStateTransition419590.mute = false;
			m_AnimatorStateTransition419590.solo = false;

			var m_AnimatorStateTransition419018 = m_DropStartAnimatorState420278.AddTransition(m_HangAnimatorState419992);
			m_AnimatorStateTransition419018.canTransitionToSelf = true;
			m_AnimatorStateTransition419018.duration = 0.15f;
			m_AnimatorStateTransition419018.exitTime = 0.99f;
			m_AnimatorStateTransition419018.hasExitTime = true;
			m_AnimatorStateTransition419018.hasFixedDuration = true;
			m_AnimatorStateTransition419018.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419018.offset = 0f;
			m_AnimatorStateTransition419018.orderedInterruption = true;
			m_AnimatorStateTransition419018.isExit = false;
			m_AnimatorStateTransition419018.mute = false;
			m_AnimatorStateTransition419018.solo = false;

			var m_AnimatorStateTransition418882 = m_PullUpAnimatorState420074.AddExitTransition();
			m_AnimatorStateTransition418882.canTransitionToSelf = true;
			m_AnimatorStateTransition418882.duration = 0.25f;
			m_AnimatorStateTransition418882.exitTime = 0.95f;
			m_AnimatorStateTransition418882.hasExitTime = false;
			m_AnimatorStateTransition418882.hasFixedDuration = true;
			m_AnimatorStateTransition418882.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418882.offset = 0f;
			m_AnimatorStateTransition418882.orderedInterruption = true;
			m_AnimatorStateTransition418882.isExit = true;
			m_AnimatorStateTransition418882.mute = false;
			m_AnimatorStateTransition418882.solo = false;
			m_AnimatorStateTransition418882.AddCondition(AnimatorConditionMode.NotEqual, 104f, "AbilityIndex");

			var m_AnimatorStateTransition419744 = m_TransferDownAnimatorState420206.AddTransition(m_HangAnimatorState419992);
			m_AnimatorStateTransition419744.canTransitionToSelf = true;
			m_AnimatorStateTransition419744.duration = 0.15f;
			m_AnimatorStateTransition419744.exitTime = 0.95f;
			m_AnimatorStateTransition419744.hasExitTime = true;
			m_AnimatorStateTransition419744.hasFixedDuration = true;
			m_AnimatorStateTransition419744.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419744.offset = 0f;
			m_AnimatorStateTransition419744.orderedInterruption = true;
			m_AnimatorStateTransition419744.isExit = false;
			m_AnimatorStateTransition419744.mute = false;
			m_AnimatorStateTransition419744.solo = false;

			var m_AnimatorStateTransition418556 = m_HangJumpStartAnimatorState420014.AddTransition(m_HangAnimatorState419992);
			m_AnimatorStateTransition418556.canTransitionToSelf = true;
			m_AnimatorStateTransition418556.duration = 0.05f;
			m_AnimatorStateTransition418556.exitTime = 0.95f;
			m_AnimatorStateTransition418556.hasExitTime = true;
			m_AnimatorStateTransition418556.hasFixedDuration = true;
			m_AnimatorStateTransition418556.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418556.offset = 0f;
			m_AnimatorStateTransition418556.orderedInterruption = true;
			m_AnimatorStateTransition418556.isExit = false;
			m_AnimatorStateTransition418556.mute = false;
			m_AnimatorStateTransition418556.solo = false;

			var m_AnimatorStateTransition419806 = m_DropStartfromLedgeStrafeAnimatorState420234.AddTransition(m_HangAnimatorState419992);
			m_AnimatorStateTransition419806.canTransitionToSelf = true;
			m_AnimatorStateTransition419806.duration = 0.15f;
			m_AnimatorStateTransition419806.exitTime = 0.99f;
			m_AnimatorStateTransition419806.hasExitTime = true;
			m_AnimatorStateTransition419806.hasFixedDuration = true;
			m_AnimatorStateTransition419806.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419806.offset = 0f;
			m_AnimatorStateTransition419806.orderedInterruption = true;
			m_AnimatorStateTransition419806.isExit = false;
			m_AnimatorStateTransition419806.mute = false;
			m_AnimatorStateTransition419806.solo = false;

			// State Machine Defaults.
			m_HangAnimatorStateMachine420340.anyStatePosition = new Vector3(-84f, -72f, 0f);
			m_HangAnimatorStateMachine420340.defaultState = m_HangAnimatorState419992;
			m_HangAnimatorStateMachine420340.entryPosition = new Vector3(-84f, -156f, 0f);
			m_HangAnimatorStateMachine420340.exitPosition = new Vector3(876f, -84f, 0f);
			m_HangAnimatorStateMachine420340.parentStateMachinePosition = new Vector3(852f, -168f, 0f);

			// State Machine Transitions.
			var m_AnimatorStateTransition419342 = baseStateMachine12.AddAnyStateTransition(m_DropStartAnimatorState420278);
			m_AnimatorStateTransition419342.canTransitionToSelf = false;
			m_AnimatorStateTransition419342.duration = 0.15f;
			m_AnimatorStateTransition419342.exitTime = 0.75f;
			m_AnimatorStateTransition419342.hasExitTime = false;
			m_AnimatorStateTransition419342.hasFixedDuration = true;
			m_AnimatorStateTransition419342.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419342.offset = 0f;
			m_AnimatorStateTransition419342.orderedInterruption = true;
			m_AnimatorStateTransition419342.isExit = false;
			m_AnimatorStateTransition419342.mute = false;
			m_AnimatorStateTransition419342.solo = false;
			m_AnimatorStateTransition419342.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition419342.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition419342.AddCondition(AnimatorConditionMode.Equals, 1f, "AbilityIntData");
			m_AnimatorStateTransition419342.AddCondition(AnimatorConditionMode.Less, 0.1f, "AbilityFloatData");

			var m_AnimatorStateTransition419358 = baseStateMachine12.AddAnyStateTransition(m_HangJumpStartAnimatorState420014);
			m_AnimatorStateTransition419358.canTransitionToSelf = false;
			m_AnimatorStateTransition419358.duration = 0.05f;
			m_AnimatorStateTransition419358.exitTime = 0.75f;
			m_AnimatorStateTransition419358.hasExitTime = false;
			m_AnimatorStateTransition419358.hasFixedDuration = true;
			m_AnimatorStateTransition419358.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419358.offset = 0f;
			m_AnimatorStateTransition419358.orderedInterruption = true;
			m_AnimatorStateTransition419358.isExit = false;
			m_AnimatorStateTransition419358.mute = false;
			m_AnimatorStateTransition419358.solo = false;
			m_AnimatorStateTransition419358.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition419358.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition419358.AddCondition(AnimatorConditionMode.Equals, 2f, "AbilityIntData");

			var m_AnimatorStateTransition418904 = baseStateMachine12.AddAnyStateTransition(m_DropStartfromLedgeStrafeAnimatorState420234);
			m_AnimatorStateTransition418904.canTransitionToSelf = false;
			m_AnimatorStateTransition418904.duration = 0.15f;
			m_AnimatorStateTransition418904.exitTime = 0.75f;
			m_AnimatorStateTransition418904.hasExitTime = false;
			m_AnimatorStateTransition418904.hasFixedDuration = true;
			m_AnimatorStateTransition418904.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition418904.offset = 0f;
			m_AnimatorStateTransition418904.orderedInterruption = true;
			m_AnimatorStateTransition418904.isExit = false;
			m_AnimatorStateTransition418904.mute = false;
			m_AnimatorStateTransition418904.solo = false;
			m_AnimatorStateTransition418904.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition418904.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition418904.AddCondition(AnimatorConditionMode.Equals, 1f, "AbilityIntData");
			m_AnimatorStateTransition418904.AddCondition(AnimatorConditionMode.Greater, 0.9f, "AbilityFloatData");
			m_AnimatorStateTransition418904.AddCondition(AnimatorConditionMode.Less, 1.1f, "AbilityFloatData");

			var m_AnimatorStateTransition419374 = baseStateMachine12.AddAnyStateTransition(m_HangStartAnimatorState420088);
			m_AnimatorStateTransition419374.canTransitionToSelf = false;
			m_AnimatorStateTransition419374.duration = 0.2f;
			m_AnimatorStateTransition419374.exitTime = 0.75f;
			m_AnimatorStateTransition419374.hasExitTime = false;
			m_AnimatorStateTransition419374.hasFixedDuration = true;
			m_AnimatorStateTransition419374.interruptionSource = TransitionInterruptionSource.None;
			m_AnimatorStateTransition419374.offset = 0f;
			m_AnimatorStateTransition419374.orderedInterruption = true;
			m_AnimatorStateTransition419374.isExit = false;
			m_AnimatorStateTransition419374.mute = false;
			m_AnimatorStateTransition419374.solo = false;
			m_AnimatorStateTransition419374.AddCondition(AnimatorConditionMode.If, 0f, "AbilityChange");
			m_AnimatorStateTransition419374.AddCondition(AnimatorConditionMode.Equals, 104f, "AbilityIndex");
			m_AnimatorStateTransition419374.AddCondition(AnimatorConditionMode.Equals, 0f, "AbilityIntData");
		}
	}
}
