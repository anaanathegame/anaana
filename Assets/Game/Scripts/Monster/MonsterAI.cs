﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using Opsive.UltimateCharacterController.Events;

/*
 * Monster AI interface class, this class works as an interface with the behavior in order to provide an easier tool to customize for designers
 * Created by: Gustavo Sanchez
*/
public class MonsterAI : MonoBehaviour
{
    // Start is called before the first frame update

    [Tooltip("Target object")]
    public List<GameObject> Targets;

    [Tooltip("Waypoints for wander")]
    public List<GameObject> wayPoints;

    [Tooltip("WayPoint duration")]
    public float wayPointDuration = 0.0f;

    [Tooltip("is Waypont selection random?")]
    public bool wayPointRandom = false;


    [Tooltip("Random Max Time for Wander")]
    public float RandomMaxWaitWander = 25.0f;

    [Tooltip("Random Min Time for Wander")]
    public float RandomMinWaitWander = 15.0f;

    [Tooltip("Damage")]
    public float Damage = 100;

    [Tooltip("View Distance")]
    public float viewDistance = 40;
    
    [Tooltip("Field of view Angle")]
    public float fieldOfViewAngle = 90;

    [Tooltip("Patrol material")]
    public Material patrolMaterialHead;

    [Tooltip("Alert Material")]
    public Material alertMaterialHead;

    [Tooltip("Patrol material")]
    public Material patrolMaterialBody;

    [Tooltip("Alert Material")]
    public Material alertMaterialBody;

    [Tooltip("Max duration in seconds for monster go for the player")]
    public float maxTimeBehindPlayer = 2.0f;

    [Tooltip("Patrol motion (Speed while the monster is patroling")]
    public Vector3 patrolMotion = new Vector3(0.10f, 0, 0.10f);

    [Tooltip("Chase motion (Speed while the monster is behind the player)")]
    public Vector3 chaseMotion = new Vector3(0.25f, 0, 0.25f);

    [Tooltip("Going to Chase Sound")]
    public AudioClip chaseSound;

    [Tooltip("Go back to patrol sound")]
    public AudioClip patrolSound;


    private BehaviorTree behaviorTree;

    private GameObject Head;

    void Start()
    {
        behaviorTree = GetComponent<BehaviorTree>();
        Head = gameObject.transform.Find("Head").gameObject;
        EventHandler.RegisterEvent(gameObject, "OnBehaviorRestart", OnBehaviorRestart);
        LazyVariables();

    }

    private void LazyVariables()
    {
        behaviorTree.SetVariable("Targets", (SharedGameObjectList)Targets);

        behaviorTree.SetVariable("WayPoints", (SharedGameObjectList)wayPoints);

        behaviorTree.SetVariable("RandomWaitWanderMax", (SharedFloat)RandomMaxWaitWander);

        behaviorTree.SetVariable("RandomWaitWanderMin", (SharedFloat)RandomMinWaitWander);

        behaviorTree.SetVariable("MaxTimeBehindPlayer", (SharedFloat)maxTimeBehindPlayer);

        behaviorTree.SetVariable("Damage", (SharedFloat)Damage);

        behaviorTree.SetVariable("ViewDistance", (SharedFloat)viewDistance);

        behaviorTree.SetVariable("FieldOfViewAngle", (SharedFloat)fieldOfViewAngle);

        behaviorTree.SetVariable("WayPointDuration", (SharedFloat)wayPointDuration);

        behaviorTree.SetVariable("WayPointRandom", (SharedBool)wayPointRandom);

        behaviorTree.SetVariable("PatrolMaterialHead", (SharedMaterial)patrolMaterialHead);

        behaviorTree.SetVariable("AlertMaterialHead", (SharedMaterial)alertMaterialHead);

        behaviorTree.SetVariable("PatrolMaterialBody", (SharedMaterial)patrolMaterialBody);

        behaviorTree.SetVariable("AlertMaterialBody", (SharedMaterial)alertMaterialBody);

        behaviorTree.SetVariable("PatrolMotion", (SharedVector3)patrolMotion);

        behaviorTree.SetVariable("ChaseMotion", (SharedVector3)chaseMotion);

        behaviorTree.SetVariable("Head", (SharedGameObject)Head);

        behaviorTree.SetVariable("ChaseSound", (SharedObject)chaseSound);

        behaviorTree.SetVariable("PatrolSound", (SharedObject)patrolSound);

    }
    // Update is called once per frame
    void Update()
    {
        
    }

    void OnBehaviorRestart()
    {
        // Stop the behavior tree
        behaviorTree.DisableBehavior();
        // Start the behavior tree back up
        behaviorTree.EnableBehavior();

        LazyVariables();
    }
}
