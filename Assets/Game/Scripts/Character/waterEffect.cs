﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waterEffect : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.gameObject.GetComponentInParent<PlayerController>();

        if (player)
        {
            GameObject water = player.gameObject.transform.Find("WaterInteractor").gameObject;
            ParticleSystem particle = water.GetComponentInChildren<ParticleSystem>();
            particle.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerController player = other.gameObject.GetComponentInParent<PlayerController>();

        if (player)
        {
            GameObject water = player.gameObject.transform.Find("WaterInteractor").gameObject;
            ParticleSystem particle = water.GetComponentInChildren<ParticleSystem>();
            particle.Stop();
        }
    }
}
