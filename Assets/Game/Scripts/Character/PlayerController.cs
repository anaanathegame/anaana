﻿using Opsive.UltimateCharacterController.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;


/*
 * Simple class for classify player
 * Created by: Gustavo Sanchez
*/
public class PlayerController : MonoBehaviour, IAnimationCompleted
{

    public GameObject flameThrower;

    public Animator animator;

    public List<PlayerController> players;

    private float forceStrenght = 10 * 80f;


    [HideInInspector]
    public GameObject Hinge;
    private void Awake()
    {
        DesactivateFlame();
        players = new List<PlayerController>();

        foreach(PlayerController player in Object.FindObjectsOfType<PlayerController>())
        {
            if (player.gameObject.GetComponent<MonsterAI>() == null)
            {
                players.Add(player);
            }
        }
    }

    public void ActivateFlame()
    {
        if (flameThrower != null && !flameThrower.activeSelf)
        {
            animator.SetTrigger("Activate");
            flameThrower.SetActive(true);
        }
    }

    public void DesactivateFlame()
    {
        if (flameThrower != null && flameThrower.activeSelf)
        {
            animator.SetTrigger("Desactivate");
            flameThrower.SetActive(false);
        }
    }
    public enum PLAYER_TYPE
    {
        PRINCIPAL,
        COMPANION,
        BOTH,
        PUSHABLE_OBJECT,
        BOTH_AT_THE_SAME_TIME
    }

    public PLAYER_TYPE playerType = PLAYER_TYPE.PRINCIPAL;

   
    public PlayerController getOtherPlayer()
    {
        foreach(PlayerController player in players)
        {
            if (player != gameObject.GetComponent<PlayerController>())
            {
                return player;
            }
        }
        return null;
    }
    public void OnRespawn()
    {
        PlayerController otherPlayer = getOtherPlayer();

        if (otherPlayer != null)
        {
            UltimateCharacterLocomotion ultimateCharacterLocomotion = otherPlayer.gameObject.GetComponent<UltimateCharacterLocomotion>();

            if (ultimateCharacterLocomotion && otherPlayer.gameObject.GetComponent<MonsterAI>() == null)
            {
                ultimateCharacterLocomotion.SetPositionAndRotation(gameObject.transform.position, Quaternion.identity);
            }
        }
        DesactivateFlame();
    }

    public void AnimationCompleted(int shortHashName)
    {
        Rigidbody rb = Hinge.GetComponent<Rigidbody>();
        if (rb)
        {
            var heading = Mathf.Atan2(transform.right.z, transform.right.x) * Mathf.Rad2Deg;
            rb.constraints = RigidbodyConstraints.None;

            if (heading > 0)
            {
                forceStrenght = -Mathf.Abs(forceStrenght);
            }
            else
            {
                forceStrenght = Mathf.Abs(forceStrenght);
            }

            rb.AddForce(new Vector3(1, 1, 1) * forceStrenght);
        }
        Animator animator = gameObject.GetComponent<Animator>();
        if (animator != null)
        {
            animator.SetInteger("AbilityIndex", 0);
        }
        UltimateCharacterLocomotionHandler ultimateCharacterLocomotionHandler = gameObject.GetComponent<UltimateCharacterLocomotionHandler>();
        if (ultimateCharacterLocomotionHandler != null)
        {
            ultimateCharacterLocomotionHandler.enabled = true;
        }
    }
}

