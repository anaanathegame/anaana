﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionableVol : MonoBehaviour
{
    // Start is called before the first frame update
    public UnityEvent OnAction;

    public GameObject PushAbleAnimator;

    public void OnActivate()
    {
        OnAction.Invoke();
    }
}
