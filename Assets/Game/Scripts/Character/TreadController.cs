﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Input;

public class TreadController : MonoBehaviour
{

    private GameObject player;
    private Animator playerAnimator;
    private Animator treadAnimator;
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInParent<UnityInput>().gameObject;
        playerAnimator = player.GetComponent<Animator>();
        treadAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (treadAnimator && playerAnimator)
        {
            float horizontalMovement = playerAnimator.GetFloat("HorizontalMovement");
            float forwardMovement = playerAnimator.GetFloat("ForwardMovement");

            treadAnimator.SetFloat("HorizontalMovement", horizontalMovement);
            treadAnimator.SetFloat("ForwardMovement", forwardMovement);

            if (forwardMovement >= 0.2)
            {
                gameObject.GetComponent<AudioSource>().enabled = true;
            }

            if (forwardMovement <= 0.1)
            {
                gameObject.GetComponent<AudioSource>().enabled = false;
            }

        }
    }
}
