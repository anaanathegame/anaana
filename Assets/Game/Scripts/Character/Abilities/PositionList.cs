﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;

public class PositionList : MonoBehaviour
{
    public List<Transform> positionList;

    public Transform GetClosestPoint(Vector3 playerPosition)
    {
        // Check the list has objeects
        if (positionList.Count <= 0)
        {
            return null;
        }

        // Get the first elements distance and assign it as the current closest point
        Transform closestPosition = positionList[0].transform;
        float distance = Vector3.Distance(positionList[0].transform.position, playerPosition);

        foreach (var member in positionList)
        {
            // Check if current member is closer
            if (Vector3.Distance(member.transform.position, playerPosition) < distance)
            {
                distance = Vector3.Distance(member.transform.position, playerPosition);
                closestPosition = member;
            }
        }

        // Check if the closest point is active
        if (closestPosition.gameObject.activeSelf == true) 
        {
            return closestPosition;
        }
        else
        {
            return null;
        }
    }
}
