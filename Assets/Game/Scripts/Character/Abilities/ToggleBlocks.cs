using Opsive.UltimateCharacterController.Character.Abilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Objects;
using Opsive.UltimateCharacterController.Input;
using Opsive.UltimateCharacterController.Character;
/*
 * Toggle block ability, using Opsive Ability API
 * Created by: Gustavo Sanchez
*/

public class ToggleBlocks : BaseAbility
{
    private UltimateCharacterLocomotionHandler ultimateCharacterLocomotionHandler;
    // Start is called before the first frame update
    public bool AlreadyAnimated = false;
    public override void Awake()
    {
        base.Awake();
    }

    public override bool CanStartAbility()
    {
        return base.CanStartAbility();

    }

    public override void Update()
    {
        base.Update();

        if (m_DetectedObject != null)
        {
            PlayerController player = m_GameObject.GetComponent<PlayerController>();
            if (player.Hinge != m_DetectedObject)
            {
                player.Hinge = m_DetectedObject;
            }
           
            ultimateCharacterLocomotionHandler = m_GameObject.GetComponent<UltimateCharacterLocomotionHandler>();

            if (ultimateCharacterLocomotionHandler != null && !AlreadyAnimated)
            {
                ultimateCharacterLocomotionHandler.enabled = false;
                AlreadyAnimated = true;
            }



        }
    }

    public override bool CanStopAbility()
    {
        AlreadyAnimated = false;
        return base.CanStopAbility();
    }

}