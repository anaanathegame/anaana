﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Events;


/*
 * Push ability SB using Opsive Ability API
 * Created by: Ricardo Pintado
*/

public class IdlePushSB : StateMachineBehaviour
{
    public string m_PushLayer = "Block";
    public float castDistance = 1.0f;
    private Vector3 raycastOffset = new Vector3(0, 0.5f, 0);

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger("PushState", 0);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RaycastHit hit;
        if (!Physics.Raycast(animator.transform.position + raycastOffset, animator.transform.forward, out hit, castDistance, 1 << LayerMask.NameToLayer(m_PushLayer), QueryTriggerInteraction.Ignore))
        {
            animator.SetInteger("PushState", -1);
        }
    }
}
