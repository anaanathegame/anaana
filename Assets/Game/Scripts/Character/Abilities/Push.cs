﻿using Opsive.UltimateCharacterController.Character.Abilities;
using Opsive.UltimateCharacterController.Input;
using Opsive.UltimateCharacterController.Audio;
using UnityEngine;


/*
 * Push ability using Opsive Ability API
 * Created by: Gustavo Sanchez
 * Modified by: Ricardo Pintado
*/

public class Push : DetectObjectAbilityBase
{
    [Tooltip("The distance the object is pushed")]
    [SerializeField] protected float m_PushedDistance = 1f;

    [Tooltip("The speed the object is pushed")]
    [SerializeField] protected float m_Speed = 1f;

    [Tooltip("Name of the layer the objet to push is in")]
    [SerializeField] protected string m_PushLayer = "Block";

    [Tooltip("Name of the animator state for the ability")]
    [SerializeField] protected string m_AnimatorState = "PushState";

    [Tooltip("A set of AudioClips that can be played when the ability is started.")]
    [SerializeField] protected AudioClipSet m_PushAudioClipSet = new AudioClipSet();


    private PlayerController m_PlayerController;
    private Animator m_Animator;
    private PushableObject m_PushableObject;
    private PositionList m_PositionList;
    
    private Vector3 startPosition;
    private Vector3 targetPosition;
    private bool isFinished = true;
    private bool isValidDirection;
    private bool isStateEnter = true;

    public override void Awake()
    {
        base.Awake();

        startPosition = new Vector3();
        targetPosition = new Vector3();

        m_PlayerController = m_GameObject.GetComponent<PlayerController>();
        if(!m_PlayerController)
        {
            Debug.LogWarning(m_DetectedObject.name + " does not have PlayerController script");
        }

        m_Animator = m_GameObject.GetComponent<Animator>();
        if (!m_Animator)
        {
            Debug.LogWarning(m_DetectedObject.name + " does not have an Animator");
        }
    }

    public override bool CanStartAbility()
    {
        base.CanStartAbility();


        // Check if theres an object to Pull/Push
        if (m_DetectedObject == null)
        {
            return false;
        }

        // Check if the Object is pushable
        m_PushableObject = m_DetectedObject.GetComponent<PushableObject>();
        if (m_PushableObject == null)
        {
            Debug.LogWarning(m_DetectedObject.name + " does not have PushableObject script");
            return false;
        }

        // Check that the object has a list of valid positions from where to push
        m_PositionList = m_DetectedObject.GetComponent<PositionList>();
        if (m_PositionList == null)
        {
            Debug.LogWarning(m_DetectedObject.name + " does not have PositionList script");
            return false;
        }

        // Get the closest position from where to Push/Pull the object
        Transform closestPosition = m_PositionList.GetClosestPoint(m_GameObject.transform.position);
        if (closestPosition == null)
        {
            return false;
        }

        // Set the character position and rotation to the closest Push/Pull position
        Vector3 position = closestPosition.position;
        position.y = m_GameObject.transform.position.y;
        m_CharacterLocomotion.SetPositionAndRotation(position, closestPosition.rotation);

        if (m_PlayerController == null) 
        {
            return false;
        }

        // Check if the player type can push the specific game object
        switch (m_PlayerController.playerType)
        {
            case PlayerController.PLAYER_TYPE.COMPANION:
                if (m_PushableObject.type == PushableObject.OBJECT_TYPE.COMPANION_OBJECT || m_PushableObject.type == PushableObject.OBJECT_TYPE.BOTH)
                {
                    return true;
                }
                break;
            case PlayerController.PLAYER_TYPE.PRINCIPAL:
                if (m_PushableObject.type == PushableObject.OBJECT_TYPE.PLAYER_OBJECT || m_PushableObject.type == PushableObject.OBJECT_TYPE.BOTH)
                {
                    return true;
                }
                break;
        }
        return false;
    }


    /// <summary>
    /// Move the pushable object in the biggest force of the forward vector
    /// </summary>
    public override void Update()
    {
        base.Update();

        int state = m_Animator.GetInteger(m_AnimatorState);

        if (state == 0)
        {
            // Ability state Idle
            isStateEnter = true;
            isFinished = true;
        }
        else if (state == 1)
        {
            // Ability state Push/Pull

            // Check if its the first time its entered
            if (isStateEnter)
            {
                OnPushStateEnter();
            }

            // Check if the Push/Pull finished or is not a valid direction
            if (m_PushableObject.finished || !isValidDirection)
            {
                isStateEnter = true;
                m_Animator.SetInteger(m_AnimatorState, 0);
                m_PushableObject.finished = false;
            }
            else 
            {
                // Move object
                isFinished = false;
                if (!m_PushableObject.move)
                {
                    m_PushableObject.MoveBlock(m_Speed, m_PushedDistance, startPosition, targetPosition);
                }
            }
        }
    }



    /// <summary>
    /// Function called only once during Push/Pull state
    /// Used for setting the target and current position, if the desired direction is valid.
    /// </summary>
    private void OnPushStateEnter()
    {
        isValidDirection = true;

        // Get Forward Or Backward Movement
        float moveDirection = GetMoveDirection();

        // Get the forward vector
        Vector3 forwardMovement = m_PositionList.GetClosestPoint(m_GameObject.transform.position).forward * moveDirection;

        // Check collision in push direction
        if (m_PushableObject.CanPushInDirection(forwardMovement, moveDirection == 1))
        {
            // Set the current and target position
            startPosition = m_DetectedObject.transform.position;
            targetPosition = m_DetectedObject.transform.position + forwardMovement;
            m_PushAudioClipSet.PlayAudioClip(m_GameObject);
            isStateEnter = false;
        }
        else
        {
            isValidDirection = false;
        }
    }

    /// <summary>
    /// Gets wether the movement is forward or backwards
    /// </summary>
    private float GetMoveDirection()
    {
        if (m_Animator.GetFloat("ForwardMovement") > 0)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    /// <summary>
    /// Can the input stop the ability?
    /// </summary>
    /// <param name="playerInput">A reference to the input component.</param>
    /// <returns>True if the input can stop the ability, first checking if the animation is completed.</returns>
    public override bool CanInputStopAbility(PlayerInput playerInput)
    {
        if (isFinished)
        {
            return base.CanInputStopAbility(playerInput);
        }

        return false;
    }

    /// <summary>
    /// The character has exited a trigger.
    /// </summary>
    /// <param name="other">The trigger collider that the character exited.</param>
    public override void OnTriggerExit(Collider other)
    {
        // The detected object will be set when the ability starts and contains a reference to the object that allowed the ability to start.
        if (other.gameObject == m_DetectedObject)
        {
            StopAbility();
        }

        base.OnTriggerExit(other);
    }

    /// <summary>
    /// Can the ability be stopped?
    /// </summary>
    /// <returns>True if the ability can be stopped.</returns>
    public override bool CanStopAbility()
    {
        if (isFinished)
        {
            return true;
        }
        return false;
    }
}
