 using Opsive.UltimateCharacterController.Character.Abilities;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionableToggle : BaseAbility
{
    // Start is called before the first frame update

    private ActionableVol Actionable;
    private Animator detectedAnim;
    private PlayerController playerController;
    public override void Awake()
    {
        base.Awake();
        playerController = m_GameObject.GetComponent<PlayerController>();
    }

    public override bool CanStartAbility()
    {
        return base.CanStartAbility();
    }

    public override void Update()
    {
        base.Update();
        if (m_DetectedObject != null && m_DetectedObject.GetComponent<ActionableVol>() as ActionableVol != null)
        {
            Actionable = m_DetectedObject.GetComponent<ActionableVol>();
            if (m_DetectedObject.GetComponent<Animator>() == null)
            {
                detectedAnim = m_DetectedObject.transform.parent.GetComponentInChildren<Animator>();
            }
            else
            {
                detectedAnim = m_DetectedObject.GetComponent<Animator>();
            }

            if (detectedAnim == null)
            {
                detectedAnim = Actionable.PushAbleAnimator.GetComponent<Animator>();
            }

            detectedAnim.enabled = true;
            Actionable.OnActivate();

            if (playerController != null && m_DetectedObject.transform.parent.gameObject.tag == "Door")
            {
                playerController.ActivateFlame();
            }
        }
    }

    public override bool CanStopAbility()
    {
        if (m_DetectedObject && detectedAnim != null && detectedAnim.enabled)
        {
            detectedAnim.enabled = false;
        }
        if (playerController != null)
        {
            playerController.DesactivateFlame();
        }
        return base.CanStopAbility();
    }
}
