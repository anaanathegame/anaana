﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Push ability pushing SB using Opsive Ability API
 * Created by: Ricardo Pintado
*/


public class PushSB : StateMachineBehaviour
{
    public string m_PushLayer = "Block";
    public float castDistance = 1.0f;
    private Vector3 raycastOffset = new Vector3(0, 0.5f, 0);

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger("PushState", 1);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RaycastHit hit;
        if (!Physics.Raycast(animator.transform.position + raycastOffset, animator.transform.forward, out hit, castDistance, 1 << LayerMask.NameToLayer(m_PushLayer), QueryTriggerInteraction.Ignore))
        {
            animator.SetInteger("PushState", -1);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetInteger("PushState", 0);
    }
}
