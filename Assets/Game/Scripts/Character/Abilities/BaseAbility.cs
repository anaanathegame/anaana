﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character.Abilities;


public class BaseAbility : DetectObjectAbilityBase
{
    // Start is called before the first frame update
    public override bool CanStartAbility()
    {
        bool canStartLocal = base.CanStartAbility();
        if (canStartLocal && m_DetectedObject != null && m_DetectedObject.transform.parent.GetComponent<PositionList>() != null)
        {
            PositionList positionList = m_DetectedObject.transform.parent.GetComponent<PositionList>();
            Transform closestPosition = positionList.GetClosestPoint(m_GameObject.transform.position);
            Vector3 position = closestPosition.position;
            position.y = m_GameObject.transform.position.y;
            m_CharacterLocomotion.SetPositionAndRotation(position, closestPosition.rotation);
        }
        return canStartLocal;
    }
}
