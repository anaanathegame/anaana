﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;

public class NewBehaviourScript : MonoBehaviour
{
    private UltimateCharacterLocomotion m_Locomotion;

    private void Start()
    {
        m_Locomotion = GetComponent<UltimateCharacterLocomotion>();
    }

    public void ToggleDisableMovement(bool toggle)
    {
        m_Locomotion.SetActive(toggle);
    }
}
