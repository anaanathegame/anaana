﻿using Opsive.UltimateCharacterController.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRotation : MonoBehaviour
{
    private UltimateCharacterLocomotion m_Locomotion;

    public void RotateCharacter()
    {
        m_Locomotion = this.GetComponent<TriggerVol>().otherObj.GetComponent<UltimateCharacterLocomotion>();
        if(m_Locomotion)
        {
            m_Locomotion.SetRotation(this.transform.rotation);
        }         
    }
}
