﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.ThirdPersonController.Character.MovementTypes;

/*
 * Script for changing the range the characters can separate on trigger
 * Created by: Ricardo Pintado
*/

public class MultiplayerZoom : MonoBehaviour
{
    public bool enableRenderer;
    [Tooltip("New player movement Range inside camera")]
    public Vector2 playerMovementRange;
    public List<GameObject> players;

    private List<TopDownMultiplayer> playerMovements = new List<TopDownMultiplayer>();

    private Vector2 defaultMovement;
    private int playerCount;

    private void Start()
    {
        this.gameObject.GetComponent<MeshRenderer>().enabled = enableRenderer;
        playerCount = 0;

        if (players == null)
        {
            Debug.LogWarning("Player list not assigned to Multiplayer Zoom in " + this.gameObject.name);
        }

        foreach (var player in players)
        {
            var Controller = player.GetComponent<Opsive.UltimateCharacterController.Character.UltimateCharacterLocomotion>();
            var movementTypes = Controller.GetSerializedMovementTypes();
            var topDownMultiplayer = (TopDownMultiplayer)movementTypes[0];

            defaultMovement = topDownMultiplayer.MaxSeparation;

            playerMovements.Add(topDownMultiplayer);
        }
    }

    public void CameraRestrictMovement()
    {
        playerCount++;

        if (playerCount < players.Count)
        {
            return;
        }
        
        foreach(var playerMovement in playerMovements)
        {
            // Assign new movement range
            playerMovement.MaxSeparation = playerMovementRange;
        }
    }

    public void ResetCameraDefault()
    {
        playerCount--;

        if (playerCount > 0) 
        {
            return;
        }

        foreach (var playerMovement in playerMovements)
        {
            // Assign new movement range
            playerMovement.MaxSeparation = defaultMovement;
        }
    }
}
