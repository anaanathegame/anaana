﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Cinemachine.CinemachineTargetGroup;

/*
 * Lerp Weight
 * Used for lerping the weight in the target group for focusing the camera on specific objects
 * Created by: Ricardo Pintado
*/
public class LerpWeight : MonoBehaviour
{
    private CinemachineTargetGroup targetGroup;

    private void Awake()
    {
        targetGroup = this.GetComponent<CinemachineTargetGroup>();
    }

    // Updates the targets weight to a desired floating value
    public void UpdateWeights(Transform target, float weight)
    {
        // Find object to switch
        int index = targetGroup.FindMember(target);
        if (index >= 0)
        {
            // Update weight to new weight value
            targetGroup.m_Targets[index].weight = weight;
        }
        else
        {
            Debug.LogWarning(target.name + " not in TargetGroup targets");
        }
    }

    // Lerps the targets weight over a certain amount of time till it reaches the maximum value of weight (1)
    public IEnumerator LerpTargetWeight(Transform target, float time)
    {
        float elapsedTime = 0;
        while (elapsedTime / time < 1)
        {
            targetGroup.m_Targets[targetGroup.FindMember(target)].weight = Mathf.Lerp(0, 1, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForSeconds(.1f);
        }
    }
}
