﻿using Opsive.UltimateCharacterController.Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Weight Radius
 * Determines if a character is with
 * Created by: Ricardo Pintado
*/
public class WeightRadius : MonoBehaviour
{
    [Tooltip("Target to track")]
    public Transform target;
    [Tooltip("Radius of the zoom area")]
    [Range(1.0f, 500.0f)]
    public float radius = 1.5f;
    [Tooltip("Gain used for zooming faster")]
    [Range(0.01f, 10.0f)]
    public float gain = 1.0f;
    public bool showRadius = true;
    [Tooltip("Weight change will only be used once")]
    public bool singleUse = false;

    private float distance;
    private bool isLerping = false;
    private List<UltimateCharacterLocomotion> characterControllerList = new List<UltimateCharacterLocomotion>();
    private LerpWeight lerpWeight;


    private void Awake()
    {
        characterControllerList = GameManager.Instance.characterControllerList;
        if (characterControllerList == null) 
        {
            Debug.LogWarning("No players found in GameManager");
        }

        lerpWeight = GameObject.FindObjectOfType<LerpWeight>();
        if (lerpWeight == null)
        {
            Debug.LogError("No LerpWeight script is attached to the TargetGroup camera");
        }
    }

    private void Update()
    {
        GameObject closestCharacter = GameManager.Instance.GetClosestCharacter(this.transform);
        distance = Vector3.Distance(closestCharacter.transform.position, this.transform.position);

        if (singleUse)
        {
            if (distance < radius && !isLerping)
            {
                StartCoroutine(lerpWeight.LerpTargetWeight(target, gain));
                isLerping = true;
            }
        }
        else
        {
            float percentage = Mathf.Lerp(0, 1, (1 - (distance / radius))) * gain;
            if (percentage > 1)
            {
                percentage = 1.0f;
            }

            lerpWeight.UpdateWeights(target, percentage);
        }
    }

    // Draw the weight radius
    void OnDrawGizmosSelected()
    {
        if(showRadius)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.position, radius);
        }
    }
}
