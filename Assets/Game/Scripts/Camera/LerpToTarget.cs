﻿using System.Collections;
using System.Collections.Generic;
using Fungus;
using UnityEngine;

public class LerpToTarget : MonoBehaviour
{
    private bool isTriggered = false; // Is GO moving towards desired Point
    public GameObject referenceStart; // Start Position Reference

    private float awaitTime; // Time to wait at Point
    private float lerpSpeed; // Speed to move towards Point
    public List<GameObject> targetGroup; // List of target Group Objects to LookAt

    public float AwaitTime
    {
        get { return awaitTime; }
        set { awaitTime = value; }
    }

    public float LerpSpeed
    {
        get { return lerpSpeed; }
        set { lerpSpeed = value; }
    }

    public List<GameObject> TargetGroup
    {
        get { return targetGroup; }
        set
        {
            if (!isTriggered)
            {
                targetGroup = value;
            }
        }
    }

    public void StartLerp()
    {
        if (!isTriggered)
        {
            StartCoroutine(MoveTowardsTarget(referenceStart.transform.position));
        }
    }

    private void Update()
    {
        if (!isTriggered)
        {
            this.gameObject.transform.position = referenceStart.transform.position;
        }
    }

    IEnumerator MoveTowardsTarget(Vector3 initialPoint)
    {
        isTriggered = true;
        float elapsedTime = 0;
        Vector3 targetGroupMidpoint = GetListMidpoint(targetGroup);

        while (elapsedTime / lerpSpeed < 1)
        {
            transform.position = Vector3.Lerp(initialPoint, targetGroupMidpoint, (elapsedTime / lerpSpeed));
            elapsedTime += Time.deltaTime;
            yield return new WaitForSeconds(.1f);
        }

        yield return new WaitForSeconds(awaitTime);

        elapsedTime = 0;
        while (elapsedTime / lerpSpeed < 1)
        {
            transform.position = Vector3.Lerp(targetGroupMidpoint, referenceStart.transform.position, (elapsedTime / lerpSpeed));
            elapsedTime += Time.deltaTime;
            yield return new WaitForSeconds(.1f);
        }

        isTriggered = false;
    }

    private Vector3 GetListMidpoint(List<GameObject> GOList)
    {
        Vector3 midPoint = new Vector3();
        if (GOList.Count > 0)
        {
            foreach (var GO in GOList)
            {
                midPoint += GO.gameObject.transform.position;
            }
            return (midPoint / targetGroup.Count);
        }

        return midPoint;
    }
}
