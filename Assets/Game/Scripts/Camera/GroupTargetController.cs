﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupTargetController : MonoBehaviour
{
    public float awaitTime = 5.0f;
    public float lerpSpeed = 0.2f;
    public List<GameObject> targetGroup;

    private LerpToTarget lerpToTarget;

    private void Awake()
    {
        lerpToTarget = FindObjectOfType<LerpToTarget>();
    }

    public void TriggerTargetGroupFocus()
    {
        if (!lerpToTarget)
        {
           return;
        }

        lerpToTarget.AwaitTime = awaitTime;
        lerpToTarget.LerpSpeed = lerpSpeed;
        lerpToTarget.TargetGroup = targetGroup;

        lerpToTarget.StartLerp();
    }
}
