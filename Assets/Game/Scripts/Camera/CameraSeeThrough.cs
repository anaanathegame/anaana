﻿using Opsive.UltimateCharacterController.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSeeThrough : MonoBehaviour
{
    public GameObject player;
    [Tooltip("Offset for which the camera should target the player")]
    public Vector3 offset;
    public float fadeOpacity = 0.2f;
    public float fadeTime = 0.2f;
    public float distanceOffset = 0.2f;
    [Tooltip("Layer mask that specifies any layers that represent a object to make transparent")]
    [SerializeField] protected LayerMask m_Seethrough = ((1 << LayerManager.Default) | (1 << LayerManager.TransparentFX));

    public LayerMask SeeThrough { get { return m_Seethrough; } }

    private IEnumerator coroutineXRay;
    private float characterDistance;
    private Vector3 fwd;

    private GameObject hitColider;
    private Renderer hitRenderer;

    void FixedUpdate()
    {
        coroutineXRay = XRay();
        StartCoroutine(coroutineXRay);
    }

    private void GetColliderRenderer(GameObject go)
    {
        hitRenderer = go.GetComponent<Renderer>();
        if(hitRenderer && hitRenderer.enabled)
        {
            return;
        }

        var hitChildren = go.GetComponentsInChildren<Renderer>();
        foreach(var children in hitChildren)
        {
            if (children.gameObject != hitColider.gameObject)
            {
                hitRenderer = children;
                return;
            }
        }
      
        hitRenderer = go.GetComponentInParent<Renderer>();
        if (hitRenderer)
        {
            return;
        }
    }

    private void SetMaterialTransparent()
    {
        foreach (Material m in hitRenderer.materials)
        {
            m.SetFloat("_Mode", 2);
            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            m.SetInt("_ZWrite", 0);
            m.DisableKeyword("_ALPHATEST_ON");
            m.EnableKeyword("_ALPHABLEND_ON");
            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            m.renderQueue = 3000;
        }
    }

    private void TargetFadeTo(float alpha)
    {
        GetColliderRenderer(hitColider);
        if (hitRenderer)
        {
            if(hitRenderer.material.HasProperty("_Color"))
            {
                if (alpha < 1.0f)
                {
                    SetMaterialTransparent();
                }
                iTween.FadeTo(hitRenderer.gameObject, alpha, fadeTime);
            }
        }
    }

    private IEnumerator XRay()
    {
        yield return (new WaitForSeconds(0.2f));

        RaycastHit hit;
        characterDistance = Vector3.Distance(player.transform.position, transform.position) - distanceOffset;
        fwd = player.transform.position - transform.position - offset;
        fwd.Normalize();
        Debug.DrawRay(transform.position, fwd * characterDistance, Color.green);
        if (Physics.Raycast(transform.position, fwd, out hit, characterDistance, m_Seethrough, QueryTriggerInteraction.Ignore))
        {
            if (hitColider != hit.collider.gameObject && hitColider)
            {
                TargetFadeTo(1.0f);
            }

            if (hitColider != hit.collider.gameObject || !hitColider)
            {
                hitColider = hit.collider.gameObject;
                TargetFadeTo(fadeOpacity);
            }

        }
        else
        {
            if (hitColider)
            {
                TargetFadeTo(1.0f);
                hitColider = null;
            }
        }
        StartCoroutine(coroutineXRay);
    }
}
