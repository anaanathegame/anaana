﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;

/*
 * Positions a GO in the middle between the players which is used as reference for the distance the players can separate (Originaly used for camera)
 * Created by: Ricardo Pintado
*/

namespace MultiplayerCamera
{
    public class CenterCameraLookAt : MonoBehaviour
    {
        [HideInInspector]
        public List<UltimateCharacterLocomotion> characterControllerList = new List<UltimateCharacterLocomotion>();
        public string playerTag = "Player";

        private void Awake()
        {
            characterControllerList = GameManager.Instance.characterControllerList;
        }

        void Update()
        {
            TranslateToMidpoint();
        }

        public void TranslateToMidpoint()
        {
            LateGetControllers();
            Vector3 playerMidpoint = new Vector3();
            if(characterControllerList.Count>0)
            {
                foreach (var character in characterControllerList)
                {
                    playerMidpoint += character.gameObject.transform.position;
                }
                this.transform.position = playerMidpoint / characterControllerList.Count;
            }
        }

        public void LateGetControllers()
        {
            if (characterControllerList == null)
            {
                var players = GameObject.FindGameObjectsWithTag(playerTag);
                foreach (var player in players)
                {
                    characterControllerList.Add(player.GetComponent<UltimateCharacterLocomotion>());
                }
            }
        }
    }
}
