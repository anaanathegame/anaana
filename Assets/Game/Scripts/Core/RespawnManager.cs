﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;
using Opsive.UltimateCharacterController.Traits;
using Opsive.UltimateCharacterController.Events;

/*
 * Respawn Manager, Singleton class, takes care of change the spawnpoint to the last checkpoint touched
 * Created by: Gustavo Sanchez
*/
public class RespawnManager : Singleton<RespawnManager>
{

    private List<CharacterHealth> characters;
    // Start is called before the first frame update
    void Start()
    {

        characters = new List<CharacterHealth>(FindObjectsOfType<CharacterHealth>());
        if (characters.Count > 0)
        {
            foreach (CharacterHealth character in characters)
            {
                EventHandler.RegisterEvent<Vector3, Vector3, GameObject>(character.gameObject, "OnDeath", OnDeath);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDeath(Vector3 position, Vector3 force, GameObject attacker)
    {

        if (characters.Count > 0)
        {
            foreach(CharacterHealth character in characters)
            {
                if (!character.GetComponent<MonsterAI>())
                {
                    if (character.HealthValue > 0)
                    {
                        character.ImmediateDeath();
                    }
                }
            }
        }
    }
}
