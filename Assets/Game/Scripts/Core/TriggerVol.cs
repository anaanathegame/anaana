﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerVol : MonoBehaviour {

    public UnityEvent OnTriggerEnterCB;
    public UnityEvent OnTriggerStayCB;
    public UnityEvent OnTriggerExitCB;

    public PlayerController.PLAYER_TYPE playerType = PlayerController.PLAYER_TYPE.BOTH;

    [HideInInspector]
    public GameObject otherObj;


    private List<PlayerController> players;

    private void Start()
    {
        players = new List<PlayerController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
        {
            return;
        }

        if (IsTriggeable(other) && OnTriggerEnterCB != null) 
        {
            OnTriggerEnterCB.Invoke();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.attachedRigidbody == null)
        {
            return;
        }

        if (IsTriggeable(other) && OnTriggerStayCB != null)
        {
           
            OnTriggerStayCB.Invoke();
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody == null)
        {
            return;
        }

        if (IsTriggeable(other) && OnTriggerExitCB != null)
        {
            OnTriggerExitCB.Invoke();
        }

    }

    public bool IsTriggeable(Collider other)
    {

        PlayerController playerController = other.attachedRigidbody.gameObject.GetComponent<PlayerController>();

        if (playerController != null)
        {
            // is a player


            if (playerController.playerType == playerType || playerType == PlayerController.PLAYER_TYPE.BOTH)
            {
                otherObj = other.attachedRigidbody.gameObject;
                return true;
            }

            if (playerType == PlayerController.PLAYER_TYPE.BOTH_AT_THE_SAME_TIME)
            {
                if (!players.Contains(playerController))
                {
                    players.Add(playerController);
                }

                if (players.Count >= 2)
                {
                    players = new List<PlayerController>();
                    otherObj = other.attachedRigidbody.gameObject;
                    return true;

                }
            }
        }
        else
        {
            // is an object
            PushableObject obj = other.gameObject.GetComponent<PushableObject>();
            if (playerType == PlayerController.PLAYER_TYPE.PUSHABLE_OBJECT && obj)
            {
                otherObj = other.attachedRigidbody.gameObject;
                return true;

            }
        }
        return false;


    }
}
