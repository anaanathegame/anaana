﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Canvas manager classs, Singleton class
 * Created by: Gustavo Sanchez
*/

public class CanvasManager : Singleton<CanvasManager>
{
    public string buttonPressTag = "ButtonPressUI";
    public List<Canvas> canvasInScene = new List<Canvas>();
    public GameObject buttonPressUI;

    private void Awake()
    {
        var foundObjects = FindObjectsOfType<Canvas>();
        foreach(var foundObject in foundObjects)
        {
            canvasInScene.Add(foundObject);
        }

        buttonPressUI = GameObject.FindGameObjectWithTag(buttonPressTag);
        if (buttonPressUI)
        {
            toggleButtonPressUI(false);
        }
    }

    public void toggleButtonPressUI(bool toggle)
    {
        buttonPressUI.gameObject.SetActive(toggle);
    }
}
