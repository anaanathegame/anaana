﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveBehaviourState : StateMachineBehaviour
{
    public string deactivateTrigger = "TriggerDeactivate";

    private OnTriggerAction onTriggerAction;
    private float stopwatch;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        onTriggerAction = animator.gameObject.GetComponent<OnTriggerAction>();
        animator.speed = onTriggerAction.activationSpeed;
        stopwatch = onTriggerAction.timer;
        if (onTriggerAction.activeSFX)
        {
            onTriggerAction.audioSource.clip = onTriggerAction.activeSFX;
            onTriggerAction.audioSource.Play();
        }

        if (onTriggerAction.isPlatform)
        {
            onTriggerAction.movingPlatform.enabled = true;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Is timer and has reached the time limit deactivate
        if (onTriggerAction.isTimer && stopwatch < 0)
        {
            animator.SetTrigger(deactivateTrigger);
        }
        else
        {
            // If the trigger is Dual and only one player is on switch
            if (onTriggerAction.isDual && onTriggerAction.playersOnTrigger < 2)
            {
                animator.SetTrigger(deactivateTrigger);
            }
        }

        // Start timmer when player exits trigger
        if (onTriggerAction.isTimer && onTriggerAction.startTimer)
        {
            stopwatch -= Time.deltaTime;
        }
    }
}
