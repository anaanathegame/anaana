﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactiveBehaviourState : StateMachineBehaviour
{
    private OnTriggerAction onTriggerAction;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        onTriggerAction = animator.gameObject.GetComponent<OnTriggerAction>();
        animator.speed = onTriggerAction.deactivationSpeed;
        if (onTriggerAction.deactivateSFX)
        {
            onTriggerAction.audioSource.clip = onTriggerAction.deactivateSFX;
            onTriggerAction.audioSource.Play();
        }
        if (onTriggerAction.isPlatform)
        {
            onTriggerAction.movingPlatform.enabled = false;
        }
    }
}
