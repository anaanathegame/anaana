﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Objects;

public class OnTriggerAction : MonoBehaviour
{
    public bool onStart = false;
    [Tooltip("Speed at which animation in activation clip is played")]
    public float activationSpeed = 1.0f;
    [Tooltip("Speed at which animation in deactivation clip is played")]
    public float deactivationSpeed = 1.0f;
    public bool isTimer = false;
    [Tooltip("Time the activation state reamins active")]
    public float timer = 0.0f;
    public bool isDual = false;
    public bool isRobot = false;
    public bool isPlatform = false;
    public AudioClip activeSFX;
    public AudioClip deactivateSFX;

    [HideInInspector]
    public AudioSource audioSource;
    [HideInInspector]
    public int playersOnTrigger = 0;
    [HideInInspector]
    public bool startTimer;
    [HideInInspector]
    public MovingPlatform movingPlatform;

    private Animator goAnimator;
    private string activateTrigger = "TriggerActivate";

    private void Start()
    {
        goAnimator = this.GetComponent<Animator>();
        if(!goAnimator)
        {
            Debug.LogWarning(this.gameObject.name + " does not have an Animator component");
        }
        audioSource = GetComponent<AudioSource>();
        if(!audioSource)
        {
            Debug.LogWarning(this.gameObject.name + " does not have an Audio Source component");
        }
        if(isPlatform)
        {
            movingPlatform = this.GetComponentInChildren<MovingPlatform>();
            movingPlatform.enabled = false;
        }
        if(onStart)
        {
            Activate();
            startTimer = true;
        }
    }

    public void Activate()
    {
        playersOnTrigger++;
        // If the trigger is Dual and only one player is on switch
        if(isDual && playersOnTrigger < 2)
        {
            return;
        }

        startTimer = false;

        goAnimator.SetTrigger(activateTrigger);
    }

    public void Deactivate()
    {
        playersOnTrigger--;
        startTimer = true;
    }
}
