﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Portal Manager, Monster portal manager, this way get a random portal to show
 * Created by: Gustavo Sanchez
*/

public class PortalManager : Singleton<PortalManager>
{
    [Tooltip("The list of teleport destination")]
    public List<GameObject> teleportDestinations;

    public GameObject AIPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool IsPortalActive()
    {

        return GameObject.FindGameObjectWithTag("Portal") != null;
    }

    public GameObject GetTeleportDestination()
    {
        Random.seed = System.DateTime.Now.Millisecond;
        int number = Random.Range(0, teleportDestinations.Count);
        GameObject destination = teleportDestinations[number];
        return destination;
    }
}
