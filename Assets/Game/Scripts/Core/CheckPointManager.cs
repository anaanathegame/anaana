using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Game;
using Opsive.UltimateCharacterController.Traits;
using UnityEngine.SceneManagement;
/*
* CheckPoint manager class, singleton class
* Created by: Gustavo Sanchez
*/

public class CheckPointManager : Singleton<CheckPointManager>
{
    private bool created = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ((FindObjectOfType<SpawnPoint>() == null || System.Math.Abs(FindObjectOfType<SpawnPoint>().Size) < 0.0f) && GameManager.Instance.IsSceneActive("level_7"))
        {
            //iT MEANS THERE IS NO SPAWN POINT IN THE GMAE
            CreateSpawnPoint(this.gameObject);
            CreatePlayerConfig();

        }
    }

    public void AddCheckPoint(GameObject gameObject, Checkpoint checkpoint)
    {
        SpawnPoint spawnPoint = FindObjectOfType<SpawnPoint>();
        CreateSpawnPoint(gameObject, checkpoint.respawnDistance);
        if (spawnPoint != null)
        {

            Destroy(spawnPoint);
        }

    }

    private void CreateSpawnPoint(GameObject spawn, float Size = 2.0f)
    {
        SpawnPoint newPoint = spawn.AddComponent(typeof(SpawnPoint)) as SpawnPoint;
        newPoint.Size = Size;
    }

    private void CreatePlayerConfig()
    {
        if (!created)
        {
            List<PlayerController> players = new List<PlayerController>(FindObjectsOfType<PlayerController>());

            if (players.Count > 0)
            {
                foreach (PlayerController player in players)
                {
                    CharacterRespawner characterRespawner = player.gameObject.GetComponent<CharacterRespawner>();
                    characterRespawner.PositioningMode = Respawner.SpawnPositioningMode.SpawnPoint;
                }
            }
            created = true;
        }
    }
}
