﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Marker manager class, this class handle the marker over the player
 * Created by: Gustavo Sanchez
*/
public class MarkerManager : Singleton<MarkerManager>
{

    public GameObject MarkerPlayer;
    public GameObject MarkerCompanion;
    public float Offset = 2.5f;
    private GameObject floaty;
    // Start is called before the first frame update
    void Start()
    {
        foreach (PlayerController player in FindObjectsOfType<PlayerController>())
        {
            switch (player.playerType)
            {
                case PlayerController.PLAYER_TYPE.PRINCIPAL:
                    floaty = Instantiate(MarkerPlayer, player.gameObject.transform.position, Quaternion.identity) as GameObject;
                    break;
                case PlayerController.PLAYER_TYPE.COMPANION:
                    floaty = Instantiate(MarkerCompanion, player.gameObject.transform.position, Quaternion.identity) as GameObject;
                    break;
            }
            floaty.name = "Mark";
            floaty.SetActive(false);
            floaty.transform.parent = player.gameObject.transform;
            floaty.transform.localPosition = new Vector3(0, Offset, 0);
        }


    }

   

    // Update is called once per frame
    void Update()
    {
        foreach (PlayerController player in FindObjectsOfType<PlayerController>())
        {
            if (player.GetComponent<Animator>().GetBool("Moving"))
            {
                player.gameObject.transform.Find("Mark").gameObject.SetActive(true);
            }
            else
            {
                player.gameObject.transform.Find("Mark").gameObject.SetActive(false);
            }
        }
    }
}
