﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeringLight : MonoBehaviour
{

    Light currentLight;

    public float minFlickerTime;
    public float maxFlickerTime;

    void Start()
    {
        currentLight = GetComponent<Light>();
        StartCoroutine(Flashing());
    }

   
    IEnumerator Flashing ()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minFlickerTime, maxFlickerTime));
            currentLight.enabled = !currentLight.enabled;
        }
    }
}
