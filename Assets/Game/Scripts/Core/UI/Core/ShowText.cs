﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;

public class ShowText : MonoBehaviour
{
    // by Chris DeChamplain
    public GameObject textPopup;

    void Start()
    {
        textPopup.GetComponent<Renderer>().enabled = false;
        Renderer[] rs = GetComponentsInChildren<Renderer>();//hide all children
        foreach (Renderer r in rs)
            r.enabled = false;
    }

    public void Callback()
    {
        GameObject other = this.gameObject.GetComponent<TriggerVol>().otherObj;
        var characterLocomotion = other.GetComponent<UltimateCharacterLocomotion>();

        textPopup.GetComponent<Renderer>().enabled = true;
        Renderer[] rs = GetComponentsInChildren<Renderer>();//show all children
        foreach (Renderer r in rs)
            r.enabled = true;
    }

    public void Exit()
    {
        textPopup.GetComponent<Renderer>().enabled = false;
        Renderer[] rs = GetComponentsInChildren<Renderer>();//hide all children
        foreach (Renderer r in rs)
            r.enabled = false;
    }
}
