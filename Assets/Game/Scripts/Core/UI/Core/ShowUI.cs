﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Opsive.UltimateCharacterController.Character;

public class ShowUI : MonoBehaviour
{
    // by Chris DeChamplain
    public Canvas textPopup;

    void Start()
    {
        textPopup.GetComponent<Canvas>().enabled = false;
        Canvas[] rs = GetComponentsInChildren<Canvas>();//hide all children
        foreach (Canvas r in rs)
            r.enabled = false;
    }

    public void Callback()
    {
        GameObject other = this.gameObject.GetComponent<TriggerVol>().otherObj;
        var characterLocomotion = other.GetComponent<UltimateCharacterLocomotion>();

        textPopup.GetComponent<Canvas>().enabled = true;
        Canvas[] rs = GetComponentsInChildren<Canvas>();//show all children
        foreach (Canvas r in rs)
            r.enabled = true;
    }

    public void Exit()
    {
        textPopup.GetComponent<Canvas>().enabled = false;
        Canvas[] rs = GetComponentsInChildren<Canvas>();//hide all children
        foreach (Canvas r in rs)
            r.enabled = false;
    }
}
