﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Opsive.UltimateCharacterController.Character;
using UnityEngine.SceneManagement;

/*
 * Pause Menu controller
 * Created by: Ricardo Pintado
*/

public class HideCanvasAction : MonoBehaviour
{
    public ControllerInterface.button button = ControllerInterface.button.AButton;
    private string buttonString;

    void OnEnable()
    {
        buttonString = ControllerInterface.Instance.buttonDictionary[button];
    }

    private void Update()
    {
        // Check for input for the pause menu
        if (Input.GetButtonDown(buttonString))
        {
            if ((SceneManager.sceneCount - GameManager.Instance.sceneOffset) > 0)
            {
                this.gameObject.SetActive(false);
            }
        }
    }
}
