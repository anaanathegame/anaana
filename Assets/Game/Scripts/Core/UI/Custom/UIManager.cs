﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * UI manager in charge of hiding and showing the corresponding Menus
 * Created by: Ricardo Pintado
 * Modified by: Gustavo Sanchez
*/

public class UIManager : Singleton<UIManager>
{
    public MenuClassifier startMenu;
    public MenuClassifier pauseMenu;
    public MenuClassifier settingsMenu;
    public MenuClassifier missionMenu;
    public MenuClassifier levelStart;
    public MenuClassifier creditsMenu;
    public MenuClassifier confirmMenu;
    public MenuClassifier deathScreen;

    private MenuClassifier lastSelected;

    private void Start()
    {
        lastSelected = startMenu;
    }

    public void ShowStartMenu()
    {
        MenuManager.Instance.hideMenu(lastSelected);
        lastSelected = startMenu;
        MenuManager.Instance.showMenu(startMenu);
    }

    public void ShowSettingsMenu()
    {
        MenuManager.Instance.hideMenu(lastSelected);
        lastSelected = settingsMenu;
        MenuManager.Instance.showMenu(settingsMenu);
    }

    public void ShowCreditsMenu()
    {
        MenuManager.Instance.hideMenu(lastSelected);
        lastSelected = creditsMenu;
        MenuManager.Instance.showMenu(creditsMenu);
    }

    public void ShowPauseMenu()
    {
        MenuManager.Instance.hideMenu(lastSelected);
        lastSelected = pauseMenu;
        MenuManager.Instance.showMenu(pauseMenu);
    }

    public void ShowConfirmMenu()
    {
        MenuManager.Instance.hideMenu(lastSelected);
        lastSelected = confirmMenu;
        MenuManager.Instance.showMenu(confirmMenu);
    }


    public void HidePauseMenu()
    {
        MenuManager.Instance.hideMenu(pauseMenu);
    }

    public void ShowMissionComplete()
    {
        MenuManager.Instance.showMenu(missionMenu);
    }

    public void HideMissionComplete()
    {
        MenuManager.Instance.hideMenu(missionMenu);
    }

    public void ShowLevelStart()
    {
        MenuManager.Instance.showMenu(levelStart);
    }

    public void HideLevelStart()
    {
        MenuManager.Instance.hideMenu(levelStart);
    }

    public void ShowDeathScreen()
    {
        MenuManager.Instance.showMenu(deathScreen);
    }

    public void HideAll()
    {
        MenuManager.Instance.hideMenu(settingsMenu);
        MenuManager.Instance.hideMenu(startMenu);
        MenuManager.Instance.hideMenu(pauseMenu);
        MenuManager.Instance.hideMenu(missionMenu);
    }

    public void ShowPrevious()
    {
        HideAll();
        MenuManager.Instance.showMenu(lastSelected);
    }

    public void QuitToMenu()
    {
        ShowStartMenu();
        SceneLoader.Instance.UnLoadCurrentScene();
    }


    public void ExitGame()
    {
        Application.Quit();
    }

    public void Restart()
    {
        HidePauseMenu();
        var numScenes = SceneManager.sceneCount;
        List<string> sceneNames = new List<string>(numScenes);
        Scene lastScene = SceneManager.GetSceneAt(numScenes - 1);
        SceneLoader.Instance.UnLoadCurrentScene();
        SceneLoader.Instance.LoadLevel(lastScene.name);
    }

    public void ToggleCanvas(MenuClassifier target, bool show)
    {
        if (show)
        {
            MenuManager.Instance.showMenu(target);
        }
        else
        {
            MenuManager.Instance.hideMenu(target);
        }
    }

    public void PlayAudio(MenuClassifier target, bool isActive)
    {
        AudioSource audio = GameObject.Find(target.menuName).GetComponent<AudioSource>();
        if (audio)
        {
            if(isActive)
            {
                audio.Play();
            }
            else
            {
                audio.Stop();
            } 
        }
    }

    public void PlaySelectedAudio()
    {
        AudioSource audio = this.GetComponent<AudioSource>();
        if (audio)
        {
            audio.PlayOneShot(audio.clip);
        }
    }

    public void EnableInput(MenuClassifier target, bool isActive)
    {
        MoveSelector moveSelector = GameObject.Find(target.menuName).GetComponentInChildren<MoveSelector>();
        if (moveSelector)
        {
            moveSelector.enabled = isActive;
        }
    }

    public void ReplacePlaceHolderInMenu(LevelStartInfo current, MenuClassifier menu)
    {
        if(menu != null)
        {
            MenuManager.Instance.showMenu(levelStart);
            PlaceHolder placeHolder = GameObject.Find(menu.menuName).GetComponentInChildren<PlaceHolder>();
            placeHolder.ReplacePlaceHolders(current.levelName, current.levelSubtitle, null);
        }
    }
}


