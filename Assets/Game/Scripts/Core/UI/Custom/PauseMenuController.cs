﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Opsive.UltimateCharacterController.Character;
using UnityEngine.SceneManagement;

/*
 * Pause Menu controller
 * Created by: Ricardo Pintado
*/

public class PauseMenuController : Singleton<PauseMenuController>
{
    public ControllerInterface.button button = ControllerInterface.button.Start;
    public Animator animator;

    private bool pauseToggle = true;
    [HideInInspector]
    public UltimateCharacterLocomotion[] characterControllers;
    private string buttonString;

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        buttonString = ControllerInterface.Instance.buttonDictionary[button];
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Reset on each level load
        pauseToggle = true;
    }

    private void Update()
    {
        // Check for input for the pause menu
        if (Input.GetButtonDown(buttonString))
        {
            if ((SceneManager.sceneCount- GameManager.Instance.sceneOffset) > 0)
            {
                TogglePauseMenu();
            }
        }
    }

    public void TogglePauseMenu()
    {
        if (pauseToggle)
        {
            UIManager.Instance.ShowPauseMenu();
        }
        else
        {
            UIManager.Instance.HidePauseMenu();
        }

        pauseToggle = !pauseToggle;

        GameManager.Instance.ToggleActiveCharacterControllers(pauseToggle);
    }
}
