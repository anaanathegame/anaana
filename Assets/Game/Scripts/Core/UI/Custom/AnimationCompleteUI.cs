﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationCompleteUI : MonoBehaviour, IAnimationCompleted
{
    [Tooltip("Scriptable object of the next UI element to show/hide")]
    public MenuClassifier targetCanvas;
    public bool isActive;

    public void AnimationCompleted(int shortHashName)
    {
        UIManager.Instance.ToggleCanvas(targetCanvas, isActive);
        UIManager.Instance.PlayAudio(targetCanvas, isActive);
        UIManager.Instance.EnableInput(targetCanvas, isActive);

        this.gameObject.SetActive(false);
    }
}
