﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelStartController : MonoBehaviour
{
    public List<LevelStartInfo> levelStartList;
    private MenuClassifier menuClassifier;

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        menuClassifier = this.GetComponent<Menu>().menuClassifier;
    }

    // Find the Characters in the level each time a scene is loaded
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if ((SceneManager.sceneCount - GameManager.Instance.sceneOffset) > 0)
        {
            LevelStartInfo current = levelStartList[(scene.buildIndex - GameManager.Instance.sceneOffset)];
            UIManager.Instance.ReplacePlaceHolderInMenu(current, menuClassifier);
        }
    }
}
