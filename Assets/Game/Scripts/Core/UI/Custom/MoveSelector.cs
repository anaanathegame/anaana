﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/*
 * Move Selector, takes care to use the joystick in the main menu
 * Created by: Ricardo Pintado
 * Modified by: Gustavo Sanchez
*/

public class MoveSelector : MonoBehaviour
{
    public ControllerInterface.button button = ControllerInterface.button.AButton;

    private GameObject currentSelected;
    private EventSystem eventSystem;
    private string buttonString;

    private void Start()
    {
        eventSystem = FindObjectOfType<EventSystem>();
        buttonString = ControllerInterface.Instance.buttonDictionary[button];
    }

    // Update is called once per frame
    void Update()
    {
        if(eventSystem)
        {
            // Check if a new one was selected
            if (currentSelected != eventSystem.currentSelectedGameObject && eventSystem.currentSelectedGameObject != null)
            {
                this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, eventSystem.currentSelectedGameObject.transform.position.y, this.gameObject.transform.position.z);
                currentSelected = eventSystem.currentSelectedGameObject;
            }

            // Invoke Click method
            if (Input.GetButtonDown(buttonString))
            {
                currentSelected.GetComponent<Button>().onClick.Invoke();
                eventSystem.SetSelectedGameObject(null);
            }
        }
    }
}
