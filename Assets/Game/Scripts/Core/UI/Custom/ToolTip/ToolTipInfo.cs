﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToolTipInfo : MonoBehaviour
{
    public ToolTipController.toolTipTypes type;
    public string title;
    [TextArea]
    public string description;
    public Sprite image;
    public bool isCenter = true;
    

    public void EnableToolTip()
    {
        ToolTipController.Instance.ShowTooltip(type, (isCenter ? null : this.gameObject), title, description, image);
    }

    public void DisableToolTip()
    {
        ToolTipController.Instance.HideTooltip();
    }
}
