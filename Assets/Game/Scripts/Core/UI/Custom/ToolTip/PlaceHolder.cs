﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlaceHolder : MonoBehaviour
{
    // UI Place Holders
    public ToolTipController.toolTipTypes type;
    public TextMeshProUGUI title;
    public TextMeshProUGUI description;
    public Image image;

    public void ReplacePlaceHolders(string title, string description, Sprite sprite)
    {
        this.title.text = title;
        this.description.text = description;
        if (sprite != null)
        {
            this.image.sprite = sprite;
        }
    }
}
