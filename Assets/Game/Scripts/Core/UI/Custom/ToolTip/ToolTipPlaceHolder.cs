﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToolTipPlaceHolder : MonoBehaviour
{
    // UI Place Holders
    public ToolTipController.toolTipTypes type;
    public TextMeshProUGUI title;
    public TextMeshProUGUI description;
    public Image image;

    private GameObject go;
    private Vector3 originalPosition;

    private void Start()
    {
        originalPosition = this.transform.position;
    }

    private void Update()
    {
        if(go)
        {
            Vector3 worldPos = Camera.main.WorldToScreenPoint(go.transform.position);
            this.transform.position = worldPos;
        }
        else
        {
            this.transform.position = originalPosition;
        }
    }

    public void ReplacePlaceHolders(GameObject go, string title, string description, Sprite sprite)
    {
        this.title.text = title;
        this.description.text = description;
        if(sprite!=null)
        {
            this.image.sprite = sprite;
        }
        this.go = go;
    }
}
