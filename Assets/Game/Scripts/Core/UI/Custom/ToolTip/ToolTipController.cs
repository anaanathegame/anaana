﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/*
 * Pause Menu controller
 * Created by: Ricardo Pintado
*/

public class ToolTipController : Singleton<ToolTipController>
{
    public ControllerInterface.button button = ControllerInterface.button.AButton;
    public string animatorTransition = "close";
    public enum toolTipTypes { simple, simpleImage};

    [HideInInspector]
    public Dictionary<toolTipTypes, ToolTipPlaceHolder> toolTipList = new Dictionary<toolTipTypes, ToolTipPlaceHolder>();
    [HideInInspector]
    public GameObject activeTooltip;
    private string buttonString;

    private void Awake()
    {
        var toolTips = GetComponentsInChildren<ToolTipPlaceHolder>();
        if (toolTips.Length > 0) 
        {
            foreach (var toolTip in toolTips)
            {
                toolTipList.Add(toolTip.type, toolTip);
                toolTip.gameObject.SetActive(false);
            }
        }
       
        buttonString = ControllerInterface.Instance.buttonDictionary[button];
    }

    private void Update()
    {
        // Invoke Click method
        if (Input.GetButtonDown(buttonString))
        {
            HideTooltip();
        }
    }

    public void ShowTooltip(toolTipTypes type, GameObject go, string title, string description, Sprite sprite)
    {
        toolTipList[type].ReplacePlaceHolders(go, title, description, sprite);
        toolTipList[type].gameObject.SetActive(true);
        activeTooltip = toolTipList[type].gameObject;
    }

    public void HideTooltip()
    {
        if(activeTooltip != null)
        {
            activeTooltip.gameObject.GetComponent<Animator>().SetBool(animatorTransition, true);
        }
    }
}
