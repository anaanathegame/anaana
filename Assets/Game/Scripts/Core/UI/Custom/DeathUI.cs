﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Opsive.UltimateCharacterController.Character;
using UnityEngine.SceneManagement;

/*
 * Pause Menu controller
 * Created by: Ricardo Pintado
*/

public class DeathUI : MonoBehaviour
{
    public void ShowDeathUI()
    {
        UIManager.Instance.ShowDeathScreen();
    }
}
