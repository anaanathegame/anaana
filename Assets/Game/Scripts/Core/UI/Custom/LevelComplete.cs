﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelComplete : MonoBehaviour
{
    public ControllerInterface.button button = ControllerInterface.button.AButton;
    private bool isComplete = false;
    private string buttonString;
    private SceneToLoad sceneToLoad;

    public float loadingTime = 2.0f;

    void OnEnable()
    {
        buttonString = ControllerInterface.Instance.buttonDictionary[button];
        sceneToLoad = this.gameObject.GetComponent<SceneToLoad>();
        if (!sceneToLoad)
        {
            Debug.Log(this.gameObject.name + " Does not have a SceneToLoad Script attached");
        }
    }

    private void Update()
    {
        // Check for input for the pause menu
        if (isComplete)
        {
            var sayDialog = Fungus.SayDialog.GetSayDialog();
            if (!sayDialog.isActiveAndEnabled && sceneToLoad)
            {
                UIManager.Instance.ShowMissionComplete();
                if (Input.GetButtonDown(buttonString))
                {
                    sceneToLoad.UnLoadCurrentScene();
                    sceneToLoad.LoadScene();
                }
            }
        }
    }

    public void MissionComplete()
    {
        StartCoroutine(ChangeLevel());
    }

    private IEnumerator ChangeLevel()
    {
        yield return new WaitForSeconds(loadingTime);
        isComplete = true;
    }
}
