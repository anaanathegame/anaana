﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Script for Loading a desired scene and unloading the current scene
 * Created by: Ricardo Pintado
*/

public class SceneToLoad : MonoBehaviour
{
    public SceneReference sceneToLoad;
    public bool EnableMultiTouch = false;

    public void LoadScene()
    {
        Input.multiTouchEnabled = EnableMultiTouch;

        Scene _scene = SceneManager.GetSceneByName(sceneToLoad);
        if (_scene.isLoaded == false)
        {
            Debug.Log("Loading Scene: " + sceneToLoad);
            SceneLoader.Instance.LoadLevel(sceneToLoad, true);
        }
    }

    public void UnLoadCurrentScene()
    {
        SceneLoader.Instance.UnLoadCurrentScene();
    }
}
