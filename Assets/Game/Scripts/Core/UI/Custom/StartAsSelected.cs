﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 * Script for assigning to the EventSystem the button it is attachted to as the default selected
 * Created by: Ricardo Pintado
*/

public class StartAsSelected : MonoBehaviour
{
    private EventSystem eventSystem;
    private Button selectedButton;

    private void Start()
    {
        eventSystem = FindObjectOfType<EventSystem>();
    }

    public void Update()
    {
        if(eventSystem)
        {
            if (!eventSystem.currentSelectedGameObject)
            {
                selectedButton = this.gameObject.GetComponent<Button>();
                selectedButton.Select();
            }
        }
    }
}
