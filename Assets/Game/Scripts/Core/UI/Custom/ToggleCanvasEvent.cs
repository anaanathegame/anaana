﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleCanvasEvent : MonoBehaviour
{
    [Tooltip("Scriptable object of the next UI element to show/hide")]
    public MenuClassifier toggleCanvas;
    public bool isActive;

    public void ToggleCanvas()
    {
        UIManager.Instance.ToggleCanvas(toggleCanvas, isActive);
    }
}
