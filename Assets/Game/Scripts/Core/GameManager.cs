using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Opsive.UltimateCharacterController.Character;
using Opsive.UltimateCharacterController.Events;
using BehaviorDesigner.Runtime;

/*
 * Game Manager
 * Used for having a list of the active character controllers, and enabling them or disabling them respectively
 * Created by: Ricardo Pintado
*/
public class GameManager : Singleton<GameManager>
{
    [Tooltip("Offset number of scenes loaded before first level")]
    public int sceneOffset = 2;
    
    public List<UltimateCharacterLocomotion> characterControllerList = new List<UltimateCharacterLocomotion>();
    public List<BehaviorTree> enemyTrees = new List<BehaviorTree>();
    private bool inDialogue = false;

    void OnEnable()
    {
        getPlayers();
        getEnemies();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void Update()
    {
        if (enemyTrees.Count <= 0 && (SceneManager.sceneCount - sceneOffset) > 0)
        {
            getEnemies();
        }

        var sayDialog = Fungus.SayDialog.GetSayDialog();
        if (sayDialog.isActiveAndEnabled && !inDialogue)
        {
            inDialogue = true;
            ToggleInputCharacterControllers(false);
        }
        else if (!sayDialog.isActiveAndEnabled && inDialogue)
        {
            inDialogue = false;
            ToggleInputCharacterControllers(true);
        }
    }

    // Find the Characters in the level each time a scene is loaded
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        getPlayers();
        getEnemies();
    }

    // Get the players and add them to the list
    private void getPlayers()
    {
        characterControllerList.Clear();
        UltimateCharacterLocomotion[] players = GameObject.FindObjectsOfType<UltimateCharacterLocomotion>();
        foreach (var player in players)
        {
            if(!player.GetComponent<MonsterAI>())
            {
                characterControllerList.Add(player);
            }
        }
    }

    // Get the enemies and add them to the list
    private void getEnemies()
    {
        enemyTrees.Clear();
        MonsterAI[] enemies = GameObject.FindObjectsOfType<MonsterAI>();
        foreach (var enemy in enemies)
        {
            enemyTrees.Add(enemy.gameObject.GetComponent<BehaviorTree>());
        }
    }

    private void toggleEnemies(bool toggle)
    {
        foreach (var enemy in enemyTrees)
        {
            if (toggle)
            {
                enemy.EnableBehavior();
            }
            else
            {
                enemy.DisableBehavior(toggle);
            }
        }
    }

    // Disable GO with Ultimate Character Controller
    public void ToggleActiveCharacterControllers(bool toggle)
    {
        if (characterControllerList != null)
        {
            foreach (var controller in characterControllerList)
            {
                controller.SetActive(toggle);
            }
        }
        toggleEnemies(toggle);
    }

    // Disable Input on Ultimate Character Controller
    public void ToggleInputCharacterControllers(bool toggle)
    {
        if(characterControllerList != null)
        {
            foreach (var controller in characterControllerList)
            {
                EventHandler.ExecuteEvent(controller.gameObject, "OnEnableGameplayInput", toggle);
            }
        }

        toggleEnemies(toggle);
    }

    // Get the gameobject of the closest character to the target
    public GameObject GetClosestCharacter(Transform target)
    {
        UltimateCharacterLocomotion closestCharacter = characterControllerList[0];
        float distance = Vector3.Distance(this.gameObject.transform.position, characterControllerList[0].transform.position);
        foreach (var character in characterControllerList)
        {
            float newdistance = Vector3.Distance(this.gameObject.transform.position, character.gameObject.transform.position);
            if (newdistance < distance)
            {
                distance = newdistance;
                closestCharacter = character;
            }
        }

        return closestCharacter.gameObject;
    }

    public List<Scene> getScenes()
    {
        List<Scene> Scenes = new List<Scene>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scenes.Add(SceneManager.GetSceneAt(i));
        }
        return Scenes;
    }

    public bool IsSceneActive(string sceneName)
    {
        Scene scene = SceneManager.GetSceneByName(sceneName);
        return getScenes().Contains(scene);
    }
}
