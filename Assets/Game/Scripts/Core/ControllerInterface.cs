﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerInterface : Singleton<ControllerInterface>
{   
    public enum button { LB, RB, Start, AButton};
    public Dictionary<button, string> buttonDictionary = new Dictionary<button, string>();

    private void Awake()
    {
        buttonDictionary.Add(button.LB, "Action");
        buttonDictionary.Add(button.RB, "Action2");
        buttonDictionary.Add(button.Start, "Start");
        buttonDictionary.Add(button.AButton, "Select");
    }
}
