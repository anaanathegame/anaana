using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

/*
 * Simple teleport creation monster
 * Created by: Gustavo Sanchez
*/
public class MonsterTeleport : Action
{
	public override void OnStart()
	{
		
	}

	public override TaskStatus OnUpdate()
	{
        Object.Instantiate(PortalManager.Instance.AIPrefab, PortalManager.Instance.GetTeleportDestination().transform);
        return TaskStatus.Success;
    }
}
