using UnityEngine;
using UnityEngine.AI;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Opsive.UltimateCharacterController.Character;

/*
 * Simple Monster base action class
 * Created by: Gustavo Sanchez
*/
[TaskCategory("Monster")]
public class MonsterAction : Action
{
    protected NavMeshAgent navMeshAgent;

    protected MonsterAI monsterAI;

    protected UltimateCharacterLocomotion characterController;
	public override void OnStart()
	{
        navMeshAgent = GetComponent<NavMeshAgent>();
        monsterAI = GetComponent<MonsterAI>();
        characterController = GetComponent<UltimateCharacterLocomotion>();
	}
    
}