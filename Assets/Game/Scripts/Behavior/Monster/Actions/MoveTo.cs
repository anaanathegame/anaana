using UnityEngine;
using UnityEngine.AI;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;



/*
 * Simple monster move action
 * Created by: Gustavo Sanchez
*/
[TaskCategory("Monster/Movement")]
[TaskDescription("Make the enemy move to a target point")]
public class MonsterMove : MonsterAction
{
    public SharedGameObject target;

    public bool onWayToTarget = false;
	public override void OnStart()
	{
        onWayToTarget = false;
        base.OnStart();
	}

	public override TaskStatus OnUpdate()
	{
        if (target != null)
        {

            if (navMeshAgent.isOnNavMesh && !onWayToTarget)
            {
                navMeshAgent.isStopped = true;
                navMeshAgent.ResetPath();
                onWayToTarget = true;
            }

            navMeshAgent.SetDestination(target.Value.transform.position);

            return Vector3.Distance(gameObject.transform.position, target.Value.transform.position) <= 1.0 ? TaskStatus.Success : TaskStatus.Running;
        }
        return TaskStatus.Failure;
	}
}
