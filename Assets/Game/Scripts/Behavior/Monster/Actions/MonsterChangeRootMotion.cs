using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class MonsterChangeRootMotion : MonsterAction
{
    public SharedVector3 motion;
	public override void OnStart()
	{
        base.OnStart();
	}

	public override TaskStatus OnUpdate()
	{
        characterController.MotorAcceleration = motion.Value;
		return TaskStatus.Success;
	}
}