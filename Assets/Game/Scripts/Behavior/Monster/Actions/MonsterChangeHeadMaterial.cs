using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;

public class MonsterChangeHeadMaterial : MonsterAction
{
    public SharedMaterial materialHead;
    public SharedMaterial materialBody;
    public SharedGameObject Head;

    public SharedColor color;

    public List<Material> materials;

    private Light spotLight;

    private VLight light;

	public override void OnStart()
	{
        base.OnStart();
        spotLight = gameObject.GetComponentInChildren<Light>();
        light = gameObject.GetComponentInChildren<VLight>();
    }

	public override TaskStatus OnUpdate()
	{
        materials = new List<Material>();
        SkinnedMeshRenderer skinnedMeshRenderer = Head.Value.GetComponent<SkinnedMeshRenderer>();
        if (skinnedMeshRenderer.materials.Length >= 1 && materialHead != null && materialBody != null)
        {
            materials.Add(materialBody.Value);
            materials.Add(materialHead.Value);
        }

        skinnedMeshRenderer.materials = materials.ToArray();

        if (spotLight != null && light != null)
        {
            spotLight.color = color.Value;
            light.colorTint = color.Value;
        }

        return TaskStatus.Success;
	}
}