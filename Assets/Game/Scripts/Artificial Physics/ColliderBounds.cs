﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Push ability using Opsive Ability API
 * Created by: Ricardo Pintado
*/

public class ColliderBounds : MonoBehaviour
{
    [HideInInspector]
    public Collider m_Collider;
    [HideInInspector]
    public Vector3 m_Center;
    [HideInInspector]
    public Vector3 m_Size, m_Min, m_Max;

    private void Start()
    {
        //Fetch the Collider from the GameObject
        m_Collider = GetComponent<Collider>();
        //Fetch the center of the Collider volume
        if(m_Collider)
        {
            m_Center = m_Collider.bounds.center;
            //Fetch the size of the Collider volume
            m_Size = m_Collider.bounds.size;
            //Fetch the minimum and maximum bounds of the Collider volume
            m_Min = m_Collider.bounds.min;
            m_Max = m_Collider.bounds.max;
        }
    }
}
