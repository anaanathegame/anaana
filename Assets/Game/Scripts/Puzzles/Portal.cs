﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;

/*
 * Portal, when the monster touch it will appear in other portal
 * Created by: Gustavo Sanchez
*/
public class Portal : MonoBehaviour
{ 
    // Start is called before the first frame update

    public GameObject AIPrefab;
    private void Awake()
    {
        StartCoroutine(Die());
    }
    void Start()
    {
        
    }

    private IEnumerator Die()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject AI = other.transform.parent.gameObject.transform.parent.gameObject;
        if (AI.GetComponent<BehaviorTree>() != null)
        {
            //It means if is an AI
            GameObject destination = PortalManager.Instance.GetTeleportDestination();
            Destroy(AI);
            Instantiate(AIPrefab, destination.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }
}
