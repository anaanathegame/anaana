﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Traits;

/*
 * Killable platform, any player that touch this will die
 * Created by: Gustavo Sanchez
*/
public class KillAblePlataform : MonoBehaviour
{
    // Start is called before the first frame update

    public PlayerController.PLAYER_TYPE playerType;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        GameObject target = other.gameObject.transform.parent.transform.parent.gameObject;
        if (target.GetComponent<PlayerController>().playerType == playerType)
        {
            CharacterHealth objectHealth = target.GetComponent<CharacterHealth>();

            if (objectHealth)
            {
                objectHealth.ImmediateDeath();
            }
        }
    }

}
