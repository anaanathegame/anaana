﻿using UnityEngine;
using System.Collections;

namespace ChrisScript
{
    // by Chris DeChamplain
    public class MovingPlatform : MonoBehaviour
    {
        public GameObject Player;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                Player.transform.parent = transform;
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                Player.transform.parent = null;
            }
        }

    }
}
