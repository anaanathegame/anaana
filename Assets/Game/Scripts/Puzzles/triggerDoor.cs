﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerDoor : MonoBehaviour {

    //Initial code by Chris DeChamplain
    //Updated by Gustavo

    public Transform target;
    public float Speed;//how fast does it move?
    public float maxDistance;//how far does it move up/down when active
    public bool isDualSwitch;//requires both players present to activate it
    public bool isBlockOnly;//requires both players present to activate it
    public bool isReversible;//goes back up if the switch is not held down
    private bool isOpening;
    public static int players = 0;

    public void OnTriggerEnter(Collider other)
    {
        if (isBlockOnly == true)
        {
            if (other.gameObject.tag == "Pushable")
            {
                OpenDoor();
            }
        }
        else
        {
            if (other.gameObject.tag == "Player")
            {
                if (!isBlockOnly)
                {
                    OpenDoor();
                    players++;
                }
            }
            if (other.gameObject.tag == "Pushable")
            {
                OpenDoor();
            }
        }
    }

    public void OnTriggerStay(Collider other)
    {
        //is the block set to ONLY open to blocks/objects being pushed on it?
        if (isBlockOnly == true)
        {
            if (other.gameObject.tag == "Pushable")
            {
                OpenDoor();
            }
        }
        else
        {
            if (other.gameObject.tag == "Player")
            {
            //Debug.Log(players);
            if (isDualSwitch == true)//if it is a Dual Switch, it requires both characters to press it to activate it
                {
                    if (players == 2)
                    {
                        OpenDoor();
                    }
                }
                else
                {
                    OpenDoor();
                }
            }
            if (other.gameObject.tag == "Pushable")
            {
                OpenDoor();
            }
        }


    }

    public void OnTriggerExit(Collider other)
    {
        if (isReversible == true)
        {
                CloseDoor();
        }
        players--;
    }


    public void OpenDoor()
    {
        isOpening = true;
        if (target.transform.position.y > -maxDistance)
        {
            target.transform.Translate(Vector3.down * Speed * Time.deltaTime); //open the door by moving the object down (temporary)
        }
        
    }
    public void CloseDoor()
    {
        isOpening = true;
        if (target.transform.position.y < maxDistance)
        {
            target.transform.Translate(Vector3.up * Speed * Time.deltaTime); //open the door by moving the object down (temporary)
        }

    }

}
