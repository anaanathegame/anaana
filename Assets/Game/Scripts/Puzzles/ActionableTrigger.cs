﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionableTrigger : MonoBehaviour
{
    public UnityEvent OnAction;
    public PlayerController.PLAYER_TYPE playerType;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        PlayerController player = other.gameObject.transform.parent.GetComponentInParent<PlayerController>();
        if (player)
        {
            switch (player.playerType)
            {
                case PlayerController.PLAYER_TYPE.PRINCIPAL:
                    if (Input.GetButton("Action") && playerType == PlayerController.PLAYER_TYPE.PRINCIPAL)
                        OnAction.Invoke();
                    break;
                case PlayerController.PLAYER_TYPE.COMPANION:
                    if (Input.GetButton("Action2") && playerType == PlayerController.PLAYER_TYPE.COMPANION)
                        OnAction.Invoke();
                    break;
            }
        }

    }
}
