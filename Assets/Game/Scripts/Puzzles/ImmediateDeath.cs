﻿using UnityEngine;
using Opsive.UltimateCharacterController.Character;
using Opsive.UltimateCharacterController.Traits;


//add this to your "level goal" trigger
//[RequireComponent(typeof(CapsuleCollider))]
public class ImmediateDeath : MonoBehaviour 
{
    public PlayerController.PLAYER_TYPE targetType;

    // Initial code by Chris DeChamplain
    // Adjusted by Gustavo and Ricardo
    public void IsActive()
    {
        GameObject other = this.gameObject.GetComponent<TriggerVol>().otherObj;
        var characterLocomotion = other.GetComponent<UltimateCharacterLocomotion>();

        var playerController = other.GetComponent<PlayerController>();

        // Is player Type
        if (playerController != null)
        {
            CharacterHealth characterHealth = other.GetComponent<CharacterHealth>();

            // Character has no health
            if (!characterHealth)
            {
                return;
            }

            if(targetType == PlayerController.PLAYER_TYPE.BOTH)
            {
                characterHealth.ImmediateDeath();
            }
            else if(playerController.playerType == targetType)
            {
                characterHealth.ImmediateDeath();
            }
        }
    }

    public void OnExit()
    {
        
    }

}