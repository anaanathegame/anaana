﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;

// Initial code by Chris DeChamplain
// Modified by Gustavo

[RequireComponent(typeof(AudioSource))]
public class Checkpoint : MonoBehaviour {

    //public Health healthManager;
    public Renderer theRenderer;
    public Material cpOff;
    public Material cpOn;

    private AudioSource aSource;
    public AudioClip checkpointSound;

    public float respawnDistance = 2.0f;
    private bool activated = false;
    private CheckPointManager checkPointManager;

    private void Awake()
    {
        checkPointManager = FindObjectOfType<CheckPointManager>();
    }
    public void activateCheckpoint()
    {
        aSource = GetComponent<AudioSource>();
        accessData();
    }

    public void CheckpointActivated()
    {
        if (checkpointSound && !activated)
        {
            aSource.volume = 0.5f;
            aSource.PlayOneShot(checkpointSound, 1.0f);
            activated = true;
        }
        Checkpoint[] checkpoints = FindObjectsOfType<Checkpoint>();
        foreach (Checkpoint cp in checkpoints)
        {
            cp.CheckpointDeactivated();
        }
        theRenderer.material = cpOn;

        checkPointManager.AddCheckPoint(gameObject, this);

    }

    public void CheckpointDeactivated()
    {
        if (theRenderer != null)
        {
            theRenderer.material = cpOff;
        }
    }

    public void accessData()
    {
        GameObject other = this.gameObject.GetComponent<TriggerVol>().otherObj;
        var characterLocomotion = other.GetComponent<UltimateCharacterLocomotion>();

        if (other.tag == "Player")
        {
          //  healthManager.SetSpawn(transform.position);
            CheckpointActivated();
        }

    }
}
