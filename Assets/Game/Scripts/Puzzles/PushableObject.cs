﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Game;
/*
 * Push ability using Opsive Ability API
 * Created by: Ricardo Pintado
*/

public class PushableObject : MonoBehaviour
{
    public enum OBJECT_TYPE
    {
        PLAYER_OBJECT,
        COMPANION_OBJECT,
        BOTH
    }

    [Tooltip("Specifies which Player can Push/Pull the Object.")]
    public OBJECT_TYPE type = OBJECT_TYPE.PLAYER_OBJECT;
    [Tooltip("Offset for the raycast when detecting collision")]
    [Range(0,0.2f)]
    public float offsetBounds = 0.1f;
    [Tooltip("Layer mask that specifies any layers that represent a solid object (such as the ground or a moving platform).")]
    [SerializeField] protected LayerMask m_SolidObjectLayers = (1 << LayerManager.Default) | (1 << LayerManager.TransparentFX) | (1 << LayerManager.MovingPlatform) | (1 << LayerManager.Character) | (1 << LayerManager.Character);
    public LayerMask SolidLayers { get { return m_SolidObjectLayers; } }

    [HideInInspector]
    public bool move;
    [HideInInspector]
    public bool finished;
    [HideInInspector]
    public bool isFinished = true;

    private float m_RaycastPushDistance = 1.25f;
    private float m_RaycastPullDistance = 2.0f;
    private float startTime = 0.0f;
    private float m_PushedDistance;
    private float m_Speed;

    private int axisDivision = 5;


    private List<Vector3> raycastBounds = new List<Vector3>();
    private Vector3 m_StartPosition = new Vector3();
    private Vector3 m_TargetPosition = new Vector3();
    

    /// <summary>
    /// Setups the character layer.
    /// </summary>
    public void Awake()
    { 
        LayerManager.Initialize();
    }


    /// <summary>
    /// Lerps the block in the desired move direction
    /// </summary>
    private void Update()
    {
        if (move)
        {
            if (((startTime * m_Speed) / m_PushedDistance) < 1)
            {
                startTime += Time.deltaTime;
                float distCovered = startTime * m_Speed;

                float fracJourney = distCovered / m_PushedDistance;
                this.transform.position = Vector3.Lerp(m_StartPosition, m_TargetPosition, fracJourney);
            }
            else
            {
                startTime = 0.0f;
                move = false;
                finished = true;
            }
        }
    }

    public void MoveBlock(float speed, float pushedDistance, Vector3 startPosition, Vector3 targetPosition)
    {
        move = true;
        finished = false;
        m_Speed = speed;
        m_PushedDistance = pushedDistance;
        m_StartPosition = startPosition;
        m_TargetPosition = targetPosition;
    }


    public void GenerateRaycastBounds(Vector3 direction)
    {
        raycastBounds.Clear();
        ColliderBounds colliderBounds = GetComponentInChildren<ColliderBounds>();
        raycastBounds.Add(transform.position);

        // Y BOUNDS
        float topBounds = transform.position.y + (colliderBounds.m_Size.y / 2);
        for (int i=0; i< axisDivision; i++)
        {
            Vector3 yOffsetPosition = new Vector3(transform.position.x,
                                                  topBounds - (colliderBounds.m_Size.y / axisDivision) * i,
                                                  transform.position.z);

            raycastBounds.Add(yOffsetPosition);
        }

        float sideBounds; 
        if (Mathf.Abs(direction.z) == 1)
        {
            // X BOUNDS
            sideBounds = transform.position.x + (colliderBounds.m_Size.x / 2) - 0.1f;
            for (int i = 0; i < axisDivision; i++)
            {
                Vector3 xOffsetPosition = new Vector3(sideBounds - (colliderBounds.m_Size.x / axisDivision) * i,
                                                    transform.position.y,
                                                    transform.position.z);

                raycastBounds.Add(xOffsetPosition);
            }
        }
        else
        {
            // Z BOUNDS
            sideBounds = transform.position.z + (colliderBounds.m_Size.z / 2) - 0.1f;
            for (int i = 0; i < axisDivision; i++)
            {
                Vector3 zOffsetPosition = new Vector3(transform.position.x,
                                                     transform.position.y,
                                                     sideBounds - (colliderBounds.m_Size.z / axisDivision) * i);

                raycastBounds.Add(zOffsetPosition);
            }
        }
    }

    public bool CanPushInDirection(Vector3 direction, bool push)
    {
        float castDistance = (push) ? m_RaycastPushDistance : m_RaycastPullDistance;
        GenerateRaycastBounds(direction);
        foreach (var bound in raycastBounds)
        {
            RaycastHit hit;
            Debug.DrawRay(bound, direction + (direction / 2), Color.blue);
            if (Physics.Raycast(bound, direction + (direction / 2), out hit, 3.0f,SolidLayers, QueryTriggerInteraction.Ignore))
            {
                if (hit.distance < castDistance)
                {
                    return false;
                }
            }
        }
        return true;
    }
}
