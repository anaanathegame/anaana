﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Opsive.UltimateCharacterController.Character;

public class TeleportPad : MonoBehaviour
{
    //Coded by Chris DeChamplain

    public int teleportCode;
    float disableTimer;
    public bool enable = true;
    public Vector3 offset;

    public float waitTime = 1.0f;

    private static TeleportPad[] teleportPads;
    private TeleportPad targetTeleportPad;
    private UltimateCharacterLocomotionHandler ultimateCharacterLocomotionHandler;
    private void Start()
    {
        teleportPads = FindObjectsOfType<TeleportPad>();
        FindTeleportTarget();
    }

    void Update()
    {
        if (disableTimer > 0)
        {
            disableTimer -= Time.deltaTime;
        }
    }

    public void Callback() {
        if(!enable || !targetTeleportPad)
        {
            return;
        }

        GameObject collider = this.gameObject.GetComponent<TriggerVol>().otherObj;
        var characterLocomotion = collider.GetComponent<UltimateCharacterLocomotion>();
        var playerController = collider.GetComponent<PlayerController>();
        ultimateCharacterLocomotionHandler = collider.GetComponent<UltimateCharacterLocomotionHandler>();

        if (playerController.playerType == PlayerController.PLAYER_TYPE.COMPANION && disableTimer <=0)
        {
            targetTeleportPad.enable = false;
            targetTeleportPad.disableTimer = 2f;
            Vector3 position = targetTeleportPad.gameObject.transform.position;
            if (ultimateCharacterLocomotionHandler != null)
            {
                characterLocomotion.enabled = false;
                ultimateCharacterLocomotionHandler.enabled = false;
            }
            StartCoroutine(MovePlayer(position, characterLocomotion));
        }
        enable = false;
    }

    private void FindTeleportTarget()
    {
        foreach (TeleportPad teleportpad in teleportPads)
        {
            if (teleportpad.teleportCode == teleportCode && teleportpad != this)
            {
                targetTeleportPad = teleportpad;
            }
        }
    }

    public void DisableSwitch()
    {
        enable = true;
    }

    public IEnumerator MovePlayer(Vector3 position, UltimateCharacterLocomotion characterLocomotion)
    {
        yield return new WaitForSeconds(waitTime);

        if (ultimateCharacterLocomotionHandler != null)
        {
            characterLocomotion.enabled = true;
            ultimateCharacterLocomotionHandler.enabled = true;
        }

        characterLocomotion.SetPositionAndRotation(position + offset, Quaternion.identity);

    }
}
