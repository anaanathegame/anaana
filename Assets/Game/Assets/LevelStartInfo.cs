﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Create/Level Start Info", order = 1)]
public class LevelStartInfo : ScriptableObject
{
    public string levelName;
    [TextArea]
    public string levelSubtitle;
}
