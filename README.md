# DUO

`DUO` The game

In DUO, you take on the roles of Yuka and TR34D as they struggle to survive and escape a hostile alien world full of danger and mystery. Use both characters in tandem to effectively pair their unique abilities and tackle ever more challenging puzzles.


# Technical Details:

  *  Unity version: `2019.1.1`
  *  Platforms: Windows desktop



# Credits:

  * Christopher Dechamplain - `Level Designer & Project Managment` 
  * Darren Briggs - `Art & Video` 
  * Ricardo Pintado - `Programming & Mechanics` 
  * Gustavo Sanchez - `Programming & AI`

    ## Music
    
    Credit for music used on the title screen and levels 1 through 6 are provided by https://ektoplazm.com/ , providers of copyright-free, royalty-free, creative commons music for non-commercial purposes.
    
    `Artists`
    
    * `Dohm & Schizoid Bears` - Modulation Manipulation
    * `Psyonysus` - New Beginnings
    * `Obri` - Beyond
    * `Faradize` - Darklight Castle
    * `Dibold` - Somewhere In Space
    * `Ghaap` - Om Namah Shivaya
    
    
    All other sounds are from freesounds.org, or the Sheridan Sound Library.